#pragma once
#pragma disable(warning:4996)
#include "LibEnums.h"
#include <ipps.h>
#include <mkl_cblas.h>
#include <algorithm>

class STException
{

    enum STVerbosityLevel_enum verb;

public:
    STException() :verb(ST_SHOW_ERRORS) {}
    virtual ~STException() {}

protected:
    template<typename T>
    void STReturnNullPointerError(T* x, enum STError_enum* ierr)
    {
        *ierr = ST_SUCCESS;
        if(x == nullptr)
        {
            STPrintError("invalid null pointer");
            *ierr = ST_NULL_PTR;
        }
    }

    template<typename T>
    void STReturnArrayTooSmallError(T n, T nmin, enum STError_enum* ierr)
    {
        if (n < nmin)
        {
            *ierr = ST_ARRAY_TOO_SMALL;
        }
    }

    static void STPrintError(char* msg)
    {
        //const char* ANSI_COLOR_RED = "\x1b[31m";
        //const char* ANSI_COLOR_GREEN = "\x1b[32m";
        //const char* ANSI_COLOR_YELLOW = "\x1b[33m";
        //const char* ANSI_COLOR_BLUE = "\x1b[34m";
        //const char* ANSI_COLOR_MAGENTA = "\x1b[35m";
        //const char* ANSI_COLOR_CYAN = "\x1b[36m";
        //const char* ANSI_COLOR_RESET = "\x1b[0m";

        wchar_t wmsg[128];
        //mbstowcs(wmsg, msg, strlen(msg) + 1);//Plus null
        //LPWSTR ptr = wmsg;

        //OutputDebugString(ptr);
        //OutputDebugString(L"\n");

    }

    static void STPrintWarning(char* msg)
    {
        //const char* ANSI_COLOR_RED = "\x1b[31m";
        //const char* ANSI_COLOR_GREEN = "\x1b[32m";
        //const char* ANSI_COLOR_YELLOW = "\x1b[33m";
        //const char* ANSI_COLOR_BLUE = "\x1b[34m";
        //const char* ANSI_COLOR_MAGENTA = "\x1b[35m";
        //const char* ANSI_COLOR_CYAN = "\x1b[36m";
        //const char* ANSI_COLOR_RESET = "\x1b[0m";

        wchar_t wmsg[128];
        //mbstowcs(wmsg, msg, strlen(msg) + 1);//Plus null
        //LPWSTR ptr = wmsg;

        //OutputDebugString(ptr);
        //OutputDebugString(L"\n");

    }

    template<typename T>
    void STSetArrayTooSmallErrorAndReturnNULL(T n, T nmin, enum STError_enum* ierr)
    {
        if (n < nmin)
        {
            //STLogArrayTooSmallError(n, nmin);
            STPrintError("first input is too small");
            *ierr = ST_ARRAY_TOO_SMALL;       
        }
    }

    template <typename T>
    bool STReturnArrayTooSmallError(T n, T nmin)
    {
        bool ret = true;
        if (n < nmin)
        {
            ret = false;
        }

        return ret;
    }


};

