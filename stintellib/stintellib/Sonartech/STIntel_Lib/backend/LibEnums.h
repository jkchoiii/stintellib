#pragma once

enum STError_enum
{
    ST_UNKNOWN_FAILURE = -1000, /*!< Unknown failure. */
    ST_MKL_FAILURE = -950,      /*!< Failure in MKL. */
    ST_MKL_ALLOC_FAILURE = -949, /*!< Failure in MKL to allocate memory. */
    ST_IPP_FAILURE = -945,      /*!< Failure in IPP. */
    ST_GMTIME_FAILURE = -940,  /*!< Error in gmtime. */
    ST_GEOLIB_FAILURE = -935,  /*!< GeographicLib failure. */
    ST_SYS_FAILURE = -930,     /*!< Error in sys functions like mkdir. */
    ST_LAPACK_FAILURE = -925,   /*!< Error in LAPACK(e). */
    ST_BLAS_FAILURE = -924,     /*!< Error in (c)BLAS. */
    ST_CONVERGENCE_ERROR = -50, /*!< Algorithm failed to converge. */
    ST_NULL_PATH = -41,         /*!< Path (directory or file) is NULL. */
    ST_EMPTY_PATH = -40,        /*!< Path (directory or file) is blank. */
    ST_ARRAY_TOO_SMALL = -3, /*!< Input array dimensions are too small. */
    ST_NULL_PTR = -2,        /*!< A required input/output pointer was NULL. */
    ST_INVALID_INPUT = -1,   /*!< A parameter to the function was incorrect. */
    ST_SUCCESS = 1,         /*!< No error.  Function was successful. */
    ST_ALLOC_FAILURE = 101, /*!< Memory allocation was unsuccesful. */
    ST_ALGORITHM_FAILURE = 102, /*!< Algorithmic failure. */
    ST_DIVISION_BY_ZERO = 103,  /*!< Division by zero. */
    ST_ARRAY_ALL_NANS = 104,    /*!< Array is all NaNs */
    ST_LSQR_EXCEEDED_CONLIM = 503, /*!< LSQR exceeded condition limit. */
    ST_LSQR_ITERLIM = 504          /*!< LSQR hit the iteration limit. */
};

enum STVerbosityLevel_enum
{
    ST_SHOW_NONE = 0,                /*!< Do not show errors. */
    ST_SHOW_ERRORS = 1,              /*!< Show errors only (default). */
    ST_SHOW_ERRORS_AND_WARNINGS = 2, /*!< Show errors and warnings. */
    ST_SHOW_ALL = 3                  /*!< Show everything - errors, warnings,
                                       and debug information. */
};

enum CACHE_LINE_SIZE_enum
{
    ST_MEMORY_ALIGN = 64
};