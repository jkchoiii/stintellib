#pragma once
#include "LibEnums.h"
#include "ErrorCheck.h"
#include <mkl.h>
#include <ipps.h>
#include <mkl_cblas.h>
#include <algorithm>

class STMemoryManager : protected STException
{

public:
    STMemoryManager(){}
    ~STMemoryManager(){}

protected:
    template<typename T>
    void CallocMemory(T** x, const int n, enum STError_enum *ierr)
    {
        *x = nullptr;

        if (n < 1)
        {
            STPrintError("Error invalid size");
            *ierr = ST_INVALID_INPUT;
        }

        if(*ierr == ST_SUCCESS)
        {
            SetArrayZeros(x, n, ierr);
            if ((*ierr) != ST_SUCCESS || *x == nullptr)
            {
                STPrintError("Error creating array");
                FreeMemory(&x);
            }
        }
        
    }

    template<typename T>
    void FreeMemory(T **p)
    {
        if (*p != nullptr)
        {
            MKL_free(*p);
            *p = nullptr;
        }
    }

    template<typename T>
    void SetArrayZeros(T** x, const int n, enum STError_enum *ierr)
    {
        *ierr = ST_SUCCESS;

        STSetArrayTooSmallErrorAndReturnNULL(n, 1, ierr);

        AllocateMemory(x, n, ierr);
        ArrayZeros(n, x, ierr);

        if (*ierr != ST_SUCCESS)
        {
            STPrintError("Error setting double zeros");
            FreeMemory(x);
        }
    }

    template<typename T>
    void ArrayZeros(const int n, T** __restrict zeros, enum STError_enum *ierr)
    {
        size_t nz;

        STReturnArrayTooSmallError(n, 1, ierr);
        STReturnNullPointerError(zeros, ierr);

        nz = (size_t)n * sizeof(T);
        memset(*zeros, 0, nz);

    }

    template<typename T>
    void AllocateMemory(T** x, const int n, enum STError_enum *ierr)
    {
        int nnew;
        size_t nbytes;
        *ierr = ST_SUCCESS;
        *x = nullptr;
        if (n < 1)
        {
            STPrintError("Invalid size");
            *ierr = ST_INVALID_INPUT;
            return;
        }
        nnew = n + ComputePadding<T>(n);
        nbytes = (size_t)nnew * sizeof(T);

        *x = (T*)MKL_malloc(nbytes, ST_MEMORY_ALIGN); // allocation

        if (*x == nullptr)
        {
            STPrintError("Error allocating array");
            *ierr = ST_MKL_ALLOC_FAILURE;
        }
        if (*x == nullptr)
        {
            STPrintError("Error allocating array");
            *ierr = ST_MKL_ALLOC_FAILURE;
        }
    }

    template<typename T>
    int ComputePadding(const int n)
    {
        size_t mod, pad;
        int ipad;
        pad = 0;
        mod = ((size_t)n * sizeof(T)) % ST_MEMORY_ALIGN;
        if (mod != 0)
        {
            pad = (ST_MEMORY_ALIGN - mod) / sizeof(T);
        }
        ipad = (int)pad;

        return ipad;
    }

};

