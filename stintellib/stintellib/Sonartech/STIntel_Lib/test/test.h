#pragma once
#include <iostream>
#include <vector>
#include "../Interface.h"

struct TestStatusStruct
{
    bool status;
    int num_case;
    std::string text;
    TestStatusStruct()
    {
        status = true;
        num_case = -1;
    }
};

class Test
{
public:

    template <typename T>
    static void PrintResult(std::string text, T data, size_t data_size)
    {
        std::cout << text << std::endl;
        for (size_t i = 0; i < data_size; i++)
        {
            std::cout << data[i] << " ";
        }
        std::cout << std::endl << std::endl;
    }

    static void PrintResult(std::string text, complex* data, size_t data_size)
    {
        std::cout << text << std::endl;
        for (size_t i = 0; i < data_size; i++)
        {
            std::cout << data[i].r << "  " << data[i].i << std::endl;
        }
        std::cout << std::endl << std::endl;
    }

    Test() {}
    virtual ~Test(){}

    virtual void StartTest() {}


};


