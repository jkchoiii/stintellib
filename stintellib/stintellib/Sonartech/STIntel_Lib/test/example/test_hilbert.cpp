#include "test_hilbert.h"

#include <numeric>

std::vector<double> HILBERT_TEST::GenerateFilterSignal()
{
    double fs = 1e4;
    double ts = 1 / fs;
    int L = 1001;
    std::vector<double> t(L);
    std::iota(t.begin(), t.end(), 0);
    for (int i = 0; i < L; i++)
    {
        t[i] = t[i] * ts;
    }
    std::vector<double> s(t.size());
    for (int i = 0; i < L; i++)
    {
        s[i] = 2.5 + cos(2.0*M_PI*203.0*t[i]) + sin(2.0*M_PI * 721.0 * t[i]) +  cos(2.0*M_PI * 1001.0 * t[i]);

    }

    return s;
}

void HILBERT_TEST::StartTest()
{
    std::shared_ptr<STInterface> creator = std::make_shared<StandardCreator<Hilbert>>();
    auto hilbert = creator->Create();

    std::vector<double> xr = GenerateFilterSignal();

    {
        std::unique_ptr<Hilbert> bb = std::make_unique<Hilbert>();
        auto dd = bb->GetDestinationStruct<complex>();
        auto pp = bb->GetParameterStruct<double>();
        pp.xr = xr;
        bb->SetDestination(&dd);
        bb->SetParameters(&pp, STIntelLib::REAL);
        bb->Process();
    }
    {
        std::unique_ptr<Envelope> bb = std::make_unique<Envelope>();
        auto dd = bb->GetDestinationStruct<double>();
        auto pp = bb->GetParameterStruct<double>();
        pp.xr = xr;
        bb->SetDestination(&dd);
        bb->SetParameters(&pp, STIntelLib::REAL);
        bb->Process();
    }
    


    {
        std::vector<double> xr = { 1, 2, 3, 4 };
        auto dst = hilbert->GetDestinationStruct<Hilbert>(hilbert);
        auto param = hilbert->GetParameterStruct<Hilbert>(hilbert);
        param.xr = xr;
        hilbert->SetParameterStruct<Hilbert>(hilbert, &param);
        hilbert->SetDestinationStruct<Hilbert>(hilbert, &dst);

        hilbert->Process();

        std::vector<double>r, i;
        r.resize(xr.size());
        i.resize(xr.size());
        for (int ii = 0; ii < i.size(); ii++)
        {
            r[ii] = dst.H[ii].r;
            i[ii] = dst.H[ii].i;
        }
    }

    creator.reset();
    creator = std::make_shared<StandardCreator<Envelope>>();
    auto envelope = creator->Create();
    auto env_param = envelope->GetParameterStruct<Envelope>(envelope);
    env_param.xr = xr;
    auto env_dst = envelope->GetDestinationStruct<Envelope>(envelope);
    envelope->SetParameterStruct<Envelope>(envelope, &env_param);
    envelope->SetDestinationStruct<Envelope>(envelope, &env_dst);

    envelope->Process();

    

}

