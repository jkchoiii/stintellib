#pragma once
#include <numeric>
#include <random>

#include "../test.h"


class FFT_TEST : public Test
{
    struct ComplexTestSetStruct
    {
        std::vector<std::pair<std::vector<complex>, std::vector<complex>>> test_set_complex;
        std::vector<int> fft_len;
    };

    struct RealTestSetStruct
    {
        std::vector<std::pair<std::vector<double>, std::vector<complex>>> test_set_double;
        std::vector<int> fft_len;
    };

    struct RealTestSetStruct2
    {
        std::vector<std::pair<std::vector<complex>, std::vector<double>>> test_set_complex;
        std::vector<int> fft_len;
    };
    
    

    struct RandomStruct
    {
        std::random_device rd;
        std::mt19937 *gen;
        std::normal_distribution<double>* dist;
        RandomStruct()
        {
            gen = new std::mt19937(rd());
            dist = new std::normal_distribution<double>(0, 1);
        }
        ~RandomStruct()
        {
            delete gen;
            delete dist;
        }
    };

    struct TestParamStruct
    {
        double fs;
        double T;
        int L;
        std::vector<double> t;
        TestParamStruct(double sample_freq, int len)
        {
            fs = sample_freq;
            T = 1 / sample_freq;
            L = len;
            t.resize(len, 0.0);
            std::iota(t.begin(), t.end(), 0.0);
        }
    };

    RandomStruct m_rand;
    const double pi = 3.14159265358979323846;
    const double eps = 1.e-10;
private:
    void GenerateSignal(std::vector<complex>& vector, TestParamStruct params, int sel);


    std::vector<complex> FillTestData(double* r, double* i, int data_size);

    TestStatusStruct DoTest(ComplexTestSetStruct& test_set, int num_test, std::string text, FFT::FFT_DIRECTION dir);
    TestStatusStruct DoTest(RealTestSetStruct& test_set, int num_test, std::string text, FFT::FFT_DIRECTION dir);
    TestStatusStruct DoTest(RealTestSetStruct2& test_set, int num_test, std::string text, FFT::FFT_DIRECTION dir);

public:
    
    FFT_TEST(){}
    virtual ~FFT_TEST(){}

    
    void StartTest() override;

private:
    TestStatusStruct TestValidate(const std::vector<ComplexDoubleStruct>& vector, std::vector<ComplexDoubleStruct>* y_ref, int i);
    TestStatusStruct TestValidate(const std::vector<double>& y, std::vector<double>* y_ref, int i);
    void FillComplexFFTSet(ComplexTestSetStruct& testset, int num_test);
    void FillComplexIFFTSet(ComplexTestSetStruct& testset, int num_test);
    void FillRealFFTSet(RealTestSetStruct& testset, int num_test);
    TestStatusStruct TestFFT1(std::string test_case);
    TestStatusStruct TestFFT2(std::string test_case);
    TestStatusStruct TestFFT3(std::string test_case);
    void FillRealIFFTSet1(RealTestSetStruct2& test_set, int num_test);
    void FillRealIFFTSet2(RealTestSetStruct2& test_set, int num_test);
    TestStatusStruct TestFFT4(std::string test_case);
    TestStatusStruct TestFFT5(std::string test_case);
};

inline void FFT_TEST::FillRealIFFTSet1(RealTestSetStruct2& test_set, int num_test)
{
    test_set.test_set_complex.resize(num_test);
    double x4_r[] = { 3.73600971927, 0.692907420144, 0.323994778293, 0.395940905829 };
    double x4_i[] = { +0.0, +0.588446936567, +0.468705124662, -0.105233188027 };


    double yref3[] = { 1.70727485318, 0.674627435809, 1.35410743027 };
    double yref6[] = { 1.02762583699, 0.312990062319, 0.484608254108, 0.433707254955, 0.553741221445, 0.923337089444 };
    double yref7[] = { 0.937385132542, 0.285668125456, 0.347477123146, 0.448955652385, 0.326826716591, 0.606108226383, 0.783588742762 };

    std::vector<complex> xx = FillTestData(x4_r, x4_i, 4);

    std::vector<double> y6(yref6, yref6 + 6);
    std::vector<double> y3(yref3, yref3 + 3);
    std::vector<double> y7(yref7, yref7 + 7);


    

    for (int i = 0; i < test_set.test_set_complex.size(); i++)
        test_set.test_set_complex[i].first = xx;

    test_set.test_set_complex[0].second = y6;
    test_set.test_set_complex[1].second = y7;
    test_set.test_set_complex[2].second = y3;

    test_set.fft_len = { 6, 7, 3 };


}

inline void FFT_TEST::FillRealIFFTSet2(RealTestSetStruct2& test_set, int num_test)
{
    test_set.test_set_complex.resize(num_test);

    double x9_r[] = { 3.73600971927, 0.832790896735, 0.0665134613678, 1.50027312149, 0.133145983226, 0.991252110762, 1.15460337053, 0.425224401181, 1.05454571082 };
    double x9_i[] = { +0.0, -2.2106834422, +0.345237401692, -0.333125094869, -0.442820699453, +0.612790159453, -0.52698583754, +0.0425386784903, +0.0 };

    double yref62[] = { 1.17248192616, 1.03884355901, 1.46066054073, 0.117196954507, -0.0150010465139, -0.0381722146318 };

    double yref162[16] = { 0.937385132542, 0.285668125456, 0.347477123146, 0.448955652385, 0.326826716591, 0.606108226383,
        0.783588742762, 0.0, 0.0, 0., 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    double yref182[18] = { 0.891817101749, 0.276999174323, 0.259022539063, 0.416194243309, 0.314497725217, 0.357185654489,
        0.651331521621, 0.617852887272, -0.101977751316, 0.0585858728232, -0.044912678099, 0.0387649511862,
        -0.0355215061513,0.0337204892884, -0.0328186925233, 0.0326649200925,-0.033433399928,0.0360366668492 };
    double yref192[19] = { 0.844879359552, 0.300509101428, 0.199814298707, 0.418004018336, 0.289679972962, 0.335096483578,
        0.477723077219, 0.746550576759, 0.12045010758, -0.0658029505304, 0.0538699301972,-0.0518328148996,
        0.053472633189, -0.0566694520245, 0.060480341708, -0.0644529049988, 0.0684489015999,-0.0727167181394, 0.0785057570439 };

    std::vector<complex> xx = FillTestData(x9_r, x9_i, 9);

    std::vector<double> y62(yref62, yref62 + 6);
    std::vector<double> y162(yref162, yref162 + 16);
    std::vector<double> y182(yref182, yref182 + 18);
    std::vector<double> y192(yref192, yref192 + 19);



    for (int i = 0; i < test_set.test_set_complex.size(); i++)
        test_set.test_set_complex[i].first = xx;

    test_set.test_set_complex[0].second = y62;
    test_set.test_set_complex[1].second = y182;
    test_set.test_set_complex[2].second = y162;
    test_set.test_set_complex[3].second = y192;

    test_set.fft_len = { 6, 18, 16, 19 };


}

inline std::vector<complex> FFT_TEST::FillTestData(double* re, double* im, int data_size)
{
    std::vector<complex> ret(data_size);

    for (int i = 0; i < data_size; i++)
    {
        ret[i].r = re[i];
        ret[i].i = im[i];
    }

    return ret;
}

inline struct TestStatusStruct FFT_TEST::TestFFT1(std::string test_case)
{
    int num_test = 3;
    ComplexTestSetStruct test_set_complex;
    
    FillComplexFFTSet(test_set_complex, num_test);

    TestStatusStruct ret = DoTest(test_set_complex, num_test, test_case, FFT::FFT_FORWARD);

    return ret;

}

inline TestStatusStruct FFT_TEST::TestFFT2(std::string test_case)
{
    int num_test = 3;

    ComplexTestSetStruct test_set_complex;
    FillComplexIFFTSet(test_set_complex, num_test);

    TestStatusStruct ret = DoTest(test_set_complex, num_test, test_case, FFT::IFFT_BACKWARD);

    return ret;
}

inline TestStatusStruct FFT_TEST::TestFFT3(std::string test_case)
{
    int num_test = 3;
    RealTestSetStruct test_set_double;
    FillRealFFTSet(test_set_double, num_test);

    TestStatusStruct ret = DoTest(test_set_double, num_test, test_case, FFT::FFT_FORWARD);

    return ret;
}

inline TestStatusStruct FFT_TEST::TestFFT4(std::string test_case)
{
    int num_test = 3;
    RealTestSetStruct2 test_set_complex;
    FillRealIFFTSet1(test_set_complex, num_test);

    TestStatusStruct ret = DoTest(test_set_complex, num_test, test_case, FFT::IFFT_BACKWARD);

    return ret;
}

inline TestStatusStruct FFT_TEST::TestFFT5(std::string test_case)
{
    int num_test = 4;
    RealTestSetStruct2 test_set_complex;
    FillRealIFFTSet2(test_set_complex, num_test);

    TestStatusStruct ret = DoTest(test_set_complex, num_test, test_case, FFT::IFFT_BACKWARD);

    return ret;
}

inline TestStatusStruct FFT_TEST::DoTest(ComplexTestSetStruct& test_set,
    int num_test, std::string text, FFT::FFT_DIRECTION dir)
{
    std::unique_ptr<FFT> bb = std::make_unique<FFT>();
    auto pp = bb->GetParameterStruct<complex>();
    auto dd = bb->GetDestinationStruct<complex>();
    pp.len_transform = test_set.fft_len[0];
    pp.algo_type = dir;
    pp.fft_norm = FFT::NO_NEED_NORMALIZE;
    pp.x = (&test_set.test_set_complex[0].first)->data();
    pp.nx = (&test_set.test_set_complex[0].first)->size();
    pp.fft_type = FFT::COMPLEX_FFT;
    bb->SetParameters(&pp, STIntelLib::COMPLEX);
    bb->SetDestination(&dd);
    bb->Process();



    //
    std::shared_ptr<STInterface> creator = std::make_shared<StandardCreator<FFT>>();
    auto fft_obj = creator->Create();



    TestStatusStruct ret;

    for (int i = 0; i < num_test; i++)
    {
        auto *x = (&test_set.test_set_complex[i].first);
        auto *y_ref = &test_set.test_set_complex[i].second;
        

        auto dst
        = fft_obj->GetDestinationStruct<FFT, STIntelLib::COMPLEX>(fft_obj);
        auto params
        = fft_obj->GetParameterStruct<FFT, STIntelLib::COMPLEX>(fft_obj);

        params.len_transform = test_set.fft_len[i];
        params.algo_type = dir;
        params.fft_norm = FFT::NO_NEED_NORMALIZE;
        params.x = x->data();
        params.nx = x->size();
        params.fft_type = FFT::COMPLEX_FFT;

        fft_obj->SetParameterStruct<FFT, STIntelLib::COMPLEX>(fft_obj, &params);
        fft_obj->SetDestinationStruct<FFT, STIntelLib::COMPLEX>(fft_obj, &dst);

        STError_enum err = fft_obj->Process();
        if(err != ST_SUCCESS)
        {
            // error!
        }


        ret = TestValidate(dst.Y, y_ref, i);
        if (ret.status == false)
        {
            ret.text = text;
            break;
        }
    }

    return ret;
}

inline struct TestStatusStruct FFT_TEST::DoTest(
    RealTestSetStruct& test_set,
    int num_test, std::string text, FFT::FFT_DIRECTION dir)
{
    TestStatusStruct ret;

    std::shared_ptr<STInterface> creator = std::make_shared<StandardCreator<FFT>>();
    auto fft_obj = creator->Create();

    for (int i = 0; i < num_test; i++)
    {
        auto *x = &test_set.test_set_double[i].first;
        auto *y_ref = &test_set.test_set_double[i].second;

        auto dst
        = fft_obj->GetDestinationStruct<FFT, STIntelLib::COMPLEX>(fft_obj);
        auto params
        = fft_obj->GetParameterStruct<FFT>(fft_obj);

        params.len_transform = test_set.fft_len[i];
        params.algo_type = dir;
        params.fft_norm = FFT::NO_NEED_NORMALIZE;
        params.x = x->data();
        params.nx = x->size();
        params.fft_type = FFT::REAL_FFT;

        fft_obj->SetDestinationStruct<FFT, STIntelLib::COMPLEX>(fft_obj, &dst);
        fft_obj->SetParameterStruct<FFT>(fft_obj, &params);
        

        fft_obj->Process();

        ret = TestValidate(dst.Y, y_ref, i);
        if (ret.status == false)
        {
            ret.text = text;
            break;
        }
    }

    return ret;
}

inline struct TestStatusStruct FFT_TEST::DoTest(
    RealTestSetStruct2& test_set,
    int num_test, std::string text, FFT::FFT_DIRECTION dir)
{
    TestStatusStruct ret;

    std::shared_ptr<STInterface> creator = std::make_shared<StandardCreator<FFT>>();
    auto fft_obj = creator->Create();

    for (int i = 0; i < num_test; i++)
    {
        auto *x = &test_set.test_set_complex[i].first;
        auto *y_ref = &test_set.test_set_complex[i].second;
        std::vector<double> y;

        auto dst
        = fft_obj->GetDestinationStruct<FFT>(fft_obj);
        auto params
        = fft_obj->GetParameterStruct<FFT, STIntelLib::COMPLEX>(fft_obj);

        params.len_transform = test_set.fft_len[i];
        params.algo_type = dir;
        params.fft_norm = FFT::NO_NEED_NORMALIZE;
        params.x = x->data();
        params.nx = x->size();
        params.fft_type = FFT::REAL_FFT;

        fft_obj->SetDestinationStruct<FFT>(fft_obj, &dst);
        //fft_obj->SetParameterStruct<FFT, STIntelLib::COMPLEX>(fft_obj, &params);
        fft_obj->SetParameterStruct<FFT, STIntelLib::COMPLEX>(fft_obj, &params);
        
        fft_obj->Process();

        ret = TestValidate(dst.Y, y_ref, i);
        if (ret.status == false)
        {
            ret.text = text;
            break;
        }
    }



    return ret;
}
