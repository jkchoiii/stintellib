#include "test_convolution.h"

void CONV_TEST::StartExample()
{
    //STInterface factory;
    //std::unique_ptr<Convolution> conv_obj = factory.Create<Convolution>("Convolution");

    std::shared_ptr<STInterface> creator = std::make_shared<StandardCreator<Convolution>>();
    auto conv_obj = creator->Create();

    Example(conv_obj, Convolution::CONV_FULL, "CONVCOR_FULL");
    Example(conv_obj, Convolution::CONV_SAME, "CONVCOR_SAME");
    Example(conv_obj, Convolution::CONV_VALID, "CONVCOR_VALID");
}

void CONV_TEST::Example(std::shared_ptr<STIntelLib>& obj, Convolution::CONV_TYPE shape, std::string text)
{

    std::unique_ptr<Convolution> bb = std::make_unique<Convolution>();
    auto pp = bb->GetParameterStruct<double>();
    auto dd = bb->GetDestinationStruct<double>();
    pp.shape = shape;
    pp.u.resize(7);
    pp.u = { -1, 2, 3, -2, 0, 1, 2 };
    pp.v.resize(4);
    pp.v = { 2, 4, -1, 1 };
    bb->SetParameters(&pp, STIntelLib::REAL);
    bb->SetDestination(&dd);
    bb->Process();

    //

    auto dst = obj->GetDestinationStruct<Convolution>(obj);
    auto param = obj->GetParameterStruct<Convolution>(obj);

    param.shape = shape;
    param.u.resize(7);
    param.u = { -1, 2, 3, -2, 0, 1, 2 };
    param.v.resize(4);
    param.v = { 2, 4, -1, 1 };

    obj->SetParameterStruct<Convolution>(obj, &param);
    obj->SetDestinationStruct<Convolution>(obj, &dst);
    
    obj->Process();


}
