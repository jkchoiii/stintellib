#include "test_poly.h"

void POLY_TEST::StartTest()
{
    std::shared_ptr<STInterface> creator = std::make_shared<StandardCreator<Poly>>();
    auto poly_obj = creator->Create();

    //auto param = poly_obj->GetParameterStruct<Poly, STIntelLib::COMPLEX>(poly_obj);
    //auto dst = poly_obj->GetDestinationStruct<Poly, STIntelLib::COMPLEX>(poly_obj);
    auto param = poly_obj->GetParameterStruct<Poly>(poly_obj);
    auto dst = poly_obj->GetDestinationStruct<Poly>(poly_obj);


    std::vector<complex> y;
    std::vector<double> p = { 1, 1, 4.3, -2 };
    //std::vector<complex> p(3);
    //p[0].r = 1;
    //p[1].r = 2;
    //p[2].r = 3;
    //p[0].i = 1;
    //p[1].i = 2;
    //p[2].i = 3;

    param.nord = 4;
    param.np = p.size();
    param.p = p.data();
    
    poly_obj->SetParameterStruct<Poly>(poly_obj, &param);
    poly_obj->SetDestinationStruct<Poly>(poly_obj, &dst);

    poly_obj->Process();

}

