#pragma once
#include <numeric>
#include <random>

#include "../test.h"




class FILTER_TEST : public Test
{
    struct ComplexTestSetStruct
    {
        std::vector<std::pair<std::vector<complex>, std::vector<complex>>> test_set_complex;
        std::vector<int> fft_len;
    };

    struct RealTestSetStruct
    {
        std::vector<std::pair<std::vector<double>, std::vector<complex>>> test_set_double;
        std::vector<int> fft_len;
    };

    struct RealTestSetStruct2
    {
        std::vector<std::pair<std::vector<complex>, std::vector<double>>> test_set_complex;
        std::vector<int> fft_len;
    };
    
    
    const double pi = 3.14159265358979323846;
    const double eps = 1.e-10;
private:

public:
    
    FILTER_TEST(){}
    virtual ~FILTER_TEST(){}

    void BesselTest(std::shared_ptr<STInterface>& creator);
    void Cheby1Test(std::shared_ptr<STInterface>& creator);
    void Cheby2Test(std::shared_ptr<STInterface>& creator);
    std::vector<double> GenerateFilterSignal();
    void IIRTest(Filter::SignalBA_Struct<double>& ba);
    void IIRIIRTest(Filter::SignalBA_Struct<double>& ba);
    void ButterTest(std::shared_ptr<STInterface>& creator);
    void StartTest() override;

private:
    
    

};



