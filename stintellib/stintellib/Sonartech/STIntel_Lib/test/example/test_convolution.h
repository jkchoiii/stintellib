#pragma once
#include "../test.h"

class CONV_TEST : public Test
{
private:
    void Example(std::shared_ptr<STIntelLib>& obj, enum Convolution::CONV_TYPE shape, std::string text);

public:
    
    CONV_TEST(){}
    virtual ~CONV_TEST(){}

    void StartExample();
};