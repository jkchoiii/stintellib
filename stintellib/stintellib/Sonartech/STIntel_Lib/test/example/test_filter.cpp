#include "test_filter.h"

#define M_PI 3.14159265358979323846

std::vector<double> FILTER_TEST::GenerateFilterSignal()
{
    double fs = 200;
    double ts = 1 / fs;
    std::vector<double> t(200);
    std::iota(t.begin(), t.end(), 0);
    for (int i = 0; i < 200; i++)
    {
        t[i] = t[i] * ts;
    }
    std::vector<double> s(t.size());
    for (int i = 0; i < t.size(); i++)
    {
        s[i] = 3.0*sin(2.0*M_PI*20.0*t[i]) + 2.0*cos(2.0*M_PI * 40.0 * t[i]) + 2.0*cos(2.0*M_PI * 5.0 * t[i]) + 2.0*sin(2.0*M_PI * 50.0 * t[i]);

    }

    return s;
}

void FILTER_TEST::IIRTest(Filter::SignalBA_Struct<double>& ba)
{
    std::vector<double> s = GenerateFilterSignal();

    std::unique_ptr<IIRFilter> bb = std::make_unique<IIRFilter>();
    auto pp = bb->GetParameterStruct<double>();
    auto dd = bb->GetDestinationStruct<double>();
    
    pp.ba = ba;
    pp.cond.have_zf = false;
    pp.cond.have_zi = false;
    pp.x = s.data();
    pp.nx = s.size();

    
    bb->SetParameters(&pp, STIntelLib::REAL);
    bb->SetDestination(&dd);
    bb->Process();

    std::shared_ptr<STInterface> creator = std::make_shared<StandardCreator<IIRFilter>>();
    auto iir_filter = creator->Create();

    auto param = iir_filter->GetParameterStruct<IIRFilter>(iir_filter);
    auto dst = iir_filter->GetDestinationStruct<IIRFilter>(iir_filter);


    param.x = s.data();
    param.nx = s.size();
    param.ba = ba;
    param.cond.have_zf = false;
    param.cond.have_zi = false;

    iir_filter->SetParameterStruct<IIRFilter>(iir_filter, &param);
    iir_filter->SetDestinationStruct<IIRFilter>(iir_filter, &dst);



    iir_filter->Process();

}

void FILTER_TEST::IIRIIRTest(Filter::SignalBA_Struct<double>& ba)
{
    std::vector<double> s = GenerateFilterSignal();

    std::unique_ptr<IIRFilter> bb = std::make_unique<IIRFilter>();
    auto pp = bb->GetParameterStruct<double>();
    auto dd = bb->GetDestinationStruct<double>();

    pp.ba = ba;
    pp.cond.have_zf = false;
    pp.cond.have_zi = false;
    pp.x = s.data();
    pp.nx = s.size();
    
    bb->SetParameters(&pp, STIntelLib::REAL);
    bb->SetDestination(&dd);
    bb->Process();



    std::shared_ptr<STInterface> creator = std::make_shared<StandardCreator<FiltFilt>>();
    auto iiriir_filter = creator->Create();
    auto param = iiriir_filter->GetParameterStruct<FiltFilt>(iiriir_filter);
    auto dst = iiriir_filter->GetDestinationStruct<FiltFilt>(iiriir_filter);

    param.x = s.data();
    param.nx = s.size();
    param.ba = ba;
    param.cond.have_zf = false;
    param.cond.have_zi = false;

    iiriir_filter->SetParameterStruct<FiltFilt>(iiriir_filter, &param);
    iiriir_filter->SetDestinationStruct<FiltFilt>(iiriir_filter, &dst);

    
    //iiriir_filter->SetParameters(ba, s.size(), s.data(), false, nullptr, false, nullptr);
    iiriir_filter->Process();
    

}

void FILTER_TEST::ButterTest(std::shared_ptr<STInterface>& creator)
{
    std::unique_ptr<Butter> butter = std::make_unique<Butter>();
    auto a = butter->GetDestinationStruct<double>();
    auto b = butter->GetParameterStruct<double>();
    b.band_type = FilterDesigner::DESIGN_IIRBAND_LOWPASS;
    b.n = 5;
    b.Wn[0] = 0.1;
    //butter->SetParameters(b.band_type, 5, 0.1);
    butter->SetParameters(&b, STIntelLib::REAL);
    butter->SetDestination(&a);
    butter->Process();

    // butter filter
    auto designer = creator->Create();

    int filter_order = 5;

    auto param = designer->GetParameterStruct<Butter>(designer);
    auto ba_low = designer->GetDestinationStruct<Butter>(designer);

    param.band_type = FilterDesigner::DESIGN_IIRBAND_LOWPASS;
    param.n = filter_order;
    param.Wn[0] = 0.1;
    

    designer->SetParameterStruct<Butter>(designer, &param);
    designer->SetDestinationStruct<Butter>(designer, &ba_low);
    designer->Process();


    IIRTest(ba_low);
    IIRIIRTest(ba_low);

    param.band_type = FilterDesigner::DESIGN_IIRBAND_HIGHPASS;
    param.n = filter_order;
    param.Wn[0] = 0.45;

    auto ba_high = designer->GetDestinationStruct<Butter>(designer);
    designer->SetParameterStruct<Butter>(designer, &param);
    designer->SetDestinationStruct<Butter>(designer, &ba_high);
    designer->Process();

    IIRTest(ba_high);
    IIRIIRTest(ba_high);

    param.band_type = FilterDesigner::DESIGN_IIRBAND_BANDPASS;
    param.n = filter_order;
    param.Wn[0] = 0.1;
    param.Wn[1] = 0.3;

    auto ba_pass = designer->GetDestinationStruct<Butter>(designer);
    designer->SetParameterStruct<Butter>(designer, &param);
    designer->SetDestinationStruct<Butter>(designer, &ba_pass);
    designer->Process();

    IIRTest(ba_pass);
    IIRIIRTest(ba_pass);


    param.band_type = FilterDesigner::DESIGN_IIRBAND_BANDSTOP;
    param.n = filter_order;
    param.Wn[0] = 0.1;
    param.Wn[1] = 0.3;

    auto ba_stop = designer->GetDestinationStruct<Butter>(designer);
    designer->SetParameterStruct<Butter>(designer, &param);
    designer->SetDestinationStruct<Butter>(designer, &ba_stop);
    designer->Process();

    IIRTest(ba_stop);
    IIRIIRTest(ba_stop);

}

void FILTER_TEST::BesselTest(std::shared_ptr<STInterface>& creator)
{
    std::unique_ptr<Bessel> bb = std::make_unique<Bessel>();
    auto pp = bb->GetParameterStruct<double>();
    auto dd = bb->GetDestinationStruct<double>();
    int norder = 5;
    pp.band_type = FilterDesigner::DESIGN_IIRBAND_LOWPASS;
    pp.n = norder;
    pp.Wn[0] = 0.1;
    bb->SetParameters(&pp, STIntelLib::REAL);
    bb->SetDestination(&dd);
    bb->Process();
    //bb->SetParameters(pp.band_type, pp.n, 0.1);

    // Bessel filter
    auto designer = creator->Create();

    auto ba_low = designer->GetDestinationStruct<Bessel>(designer);
    auto param = designer->GetParameterStruct<Bessel>(designer);

    int filter_order = 5;
    param.band_type = FilterDesigner::DESIGN_IIRBAND_LOWPASS;
    param.n = filter_order;
    param.Wn[0] = 0.1;

    designer->SetParameterStruct<Bessel>(designer, &param);
    designer->SetDestinationStruct<Bessel>(designer, &ba_low);
    designer->Process();

    //
    auto ba_high = designer->GetDestinationStruct<Bessel>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_HIGHPASS;
    param.Wn[0] = 0.45;
    designer->SetParameterStruct<Bessel>(designer, &param);
    designer->SetDestinationStruct<Bessel>(designer, &ba_high);
    designer->Process();

    auto ba_pass = designer->GetDestinationStruct<Bessel>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_BANDPASS;
    param.Wn[0] = 0.1;
    param.Wn[1] = 0.3;
    designer->SetParameterStruct<Bessel>(designer, &param);
    designer->SetDestinationStruct<Bessel>(designer, &ba_pass);
    designer->Process();

    auto ba_stop = designer->GetDestinationStruct<Bessel>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_BANDSTOP;
    param.Wn[0] = 0.1;
    param.Wn[1] = 0.3;
    designer->SetParameterStruct<Bessel>(designer, &param);
    designer->SetDestinationStruct<Bessel>(designer, &ba_stop);
    designer->Process();
    

}

void FILTER_TEST::Cheby1Test(std::shared_ptr<STInterface>& creator)
{
    std::unique_ptr<Cheby1> bb = std::make_unique<Cheby1>();
    auto pp = bb->GetParameterStruct<double>();
    auto dd = bb->GetDestinationStruct<double>();
    int norder = 5;
    pp.band_type = FilterDesigner::DESIGN_IIRBAND_LOWPASS;
    pp.n = norder;
    pp.rp = 10;
    pp.Wn[0] = 0.1;
    bb->SetParameters(&pp, STIntelLib::REAL);
    bb->SetDestination(&dd);

    bb->Process();
    //bb->SetParameters(pp.band_type, pp.n, pp.rp, pp.Wn[0]);


    // Cheby1 filter
    auto designer = creator->Create();

    auto param = designer->GetParameterStruct<Cheby1>(designer);

    auto ba_low = designer->GetDestinationStruct<Cheby1>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_LOWPASS;
    param.n = 5;
    param.Wn[0] = 0.1;
    param.rp = 10;
    designer->SetParameterStruct<Cheby1>(designer, &param);
    designer->SetDestinationStruct<Cheby1>(designer, &ba_low);
    designer->Process();


    auto ba_high = designer->GetDestinationStruct<Cheby1>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_HIGHPASS;
    param.n = 5;
    param.Wn[0] = 0.45;
    param.rp = 10;
    designer->SetParameterStruct<Cheby1>(designer, &param);
    designer->SetDestinationStruct<Cheby1>(designer, &ba_high);
    designer->Process();


    auto ba_pass = designer->GetDestinationStruct<Cheby1>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_BANDPASS;
    param.n = 5;
    param.Wn[0] = 0.1;
    param.Wn[1] = 0.3;
    param.rp = 10;
    designer->SetParameterStruct<Cheby1>(designer, &param);
    designer->SetDestinationStruct<Cheby1>(designer, &ba_pass);
    designer->Process();


    auto ba_stop = designer->GetDestinationStruct<Cheby1>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_BANDSTOP;
    param.n = 5;
    param.Wn[0] = 0.1;
    param.Wn[1] = 0.3;
    param.rp = 10;
    designer->SetParameterStruct<Cheby1>(designer, &param);
    designer->SetDestinationStruct<Cheby1>(designer, &ba_stop);
    designer->Process();

}

void FILTER_TEST::Cheby2Test(std::shared_ptr<STInterface>& creator)
{
    std::unique_ptr<Cheby2> bb = std::make_unique<Cheby2>();
    auto pp = bb->GetParameterStruct<double>();
    auto dd = bb->GetDestinationStruct<double>();
    int norder = 5;
    pp.band_type = FilterDesigner::DESIGN_IIRBAND_LOWPASS;
    pp.n = norder;
    pp.rs = 10;
    pp.Wn[0] = 0.1;
    bb->SetParameters(&pp, STIntelLib::REAL);

    bb->SetDestination(&dd);

    bb->Process();
    //bb->SetParameters(pp.band_type, pp.n, pp.rs, pp.Wn[0]);



    // Cheby1 filter
    auto designer = creator->Create();

    auto param = designer->GetParameterStruct<Cheby2>(designer);

    auto ba_low = designer->GetDestinationStruct<Cheby2>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_LOWPASS;
    param.n = 5;
    param.Wn[0] = 0.1;
    param.rs = 10;
    designer->SetParameterStruct<Cheby2>(designer, &param);
    designer->SetDestinationStruct<Cheby2>(designer, &ba_low);
    designer->Process();


    auto ba_high = designer->GetDestinationStruct<Cheby2>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_HIGHPASS;
    param.n = 5;
    param.Wn[0] = 0.45;
    param.rs = 10;
    designer->SetParameterStruct<Cheby2>(designer, &param);
    designer->SetDestinationStruct<Cheby2>(designer, &ba_high);
    designer->Process();


    auto ba_pass = designer->GetDestinationStruct<Cheby2>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_BANDPASS;
    param.n = 5;
    param.Wn[0] = 0.1;
    param.Wn[1] = 0.3;
    param.rs = 10;
    designer->SetParameterStruct<Cheby2>(designer, &param);
    designer->SetDestinationStruct<Cheby2>(designer, &ba_pass);
    designer->Process();


    auto ba_stop = designer->GetDestinationStruct<Cheby2>(designer);
    param.band_type = FilterDesigner::DESIGN_IIRBAND_BANDSTOP;
    param.n = 5;
    param.Wn[0] = 0.1;
    param.Wn[1] = 0.3;
    param.rs = 10;
    designer->SetParameterStruct<Cheby2>(designer, &param);
    designer->SetDestinationStruct<Cheby2>(designer, &ba_stop);
    designer->Process();

}

void FILTER_TEST::StartTest()
{
    //STInterface factory;
    std::shared_ptr<STInterface> creator = std::make_shared<StandardCreator<Butter>>();

    ButterTest(creator);

    creator.reset();
    creator = std::make_shared<StandardCreator<Bessel>>();
    BesselTest(creator);

    creator.reset();
    creator = std::make_shared<StandardCreator<Cheby1>>();
    Cheby1Test(creator);

    creator.reset();
    creator = std::make_shared<StandardCreator<Cheby2>>();
    Cheby2Test(creator);

}

