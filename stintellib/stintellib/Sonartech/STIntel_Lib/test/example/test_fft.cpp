#include "test_fft.h"
#include <random>
#include <numeric>


void FFT_TEST::StartTest()
{
    TestStatusStruct ret;
    ret = TestFFT1("complex fft");
    if (ret.status == false)
        goto err;
    
    ret = TestFFT2("complex ifft");
    if (ret.status == false)
        goto err;

    ret = TestFFT3("real fft");
    if (ret.status == false)
        goto err;

    ret = TestFFT4("real ifft");
    if (ret.status == false)
        goto err;

    ret = TestFFT5("real ifft");
    if (ret.status == false)
        goto err;


    if(ret.status == false)
    {
    err:
        std::cout << "error: " << ret.num_case << std::endl;
    }
    

}

TestStatusStruct FFT_TEST::TestValidate(const std::vector<complex>& y,
    std::vector<complex>* y_ref, int i)
{
    TestStatusStruct ret;
    std::cout << std::endl << "Case " << i << std::endl;

    for (int j = 0; j < y_ref->size(); j++)
    {
        if (std::abs(y[j].r - (*y_ref)[j].r) > eps)
        {
            std::cout << "false ";
            ret.status = false;
            ret.num_case = i;
            break;
        }

        if (std::abs(y[j].i - (*y_ref)[j].i) > eps)
        {
            std::cout << "false ";
            ret.status = false;
            ret.num_case = i;
            break;
        }

        std::cout << "true ";
    }
    std::cout << std::endl;

    return ret;
}

TestStatusStruct FFT_TEST::TestValidate(const std::vector<double>& y,
    std::vector<double>* y_ref, int i)
{
    std::cout << std::endl << "Case " << i << std::endl;
    TestStatusStruct ret;
    for (int j = 0; j < y_ref->size(); j++)
    {
        
        if (std::abs(y[j] - (*y_ref)[j]) > eps)
        {
            std::cout << "false ";
            ret.status = false;
            ret.num_case = i;
            break;
        }
        else
        {
            std::cout << "true ";
        }

    }
    std::cout << std::endl;

    return ret;
}

void FFT_TEST::FillComplexIFFTSet(ComplexTestSetStruct& pairs, int num_test)
{
    pairs.test_set_complex.resize(num_test);
    double x5_r[] = { 0.211061514527026, 0.106207731678285, 0.162350629014502, 0.242979570044276, 0.633234809309410 };
    double x5_i[] = { +0.623224531114387, +0.062986312136794, +0.584662602672116, +0.421048655806108, +0.521471236524426 };
    
    double y5_r[] = { 0.2711668509147001, 0.0903036033217660, 0.0326381279247792, -0.1374005869860872, -0.0456464806481318 };
    double y5_i[] = { +0.4426786676507662, -0.1116861384584011, +0.0456148496077385, +0.1388532752165509, +0.1077638770977322 };

    double y8_r[] = { 0.1694792818216875, -0.1807265750002852, 0.1300010048114060, -0.0103828507362705, 0.0821824563910471, -0.0709823993633398, 0.0404854188940775, 0.0510051777087037 };
    double y8_i[] = { +0.2766741672817289, +0.0322285401575749, +0.0529076658250882, +0.0549379607279793, +0.1556654252960034, +0.0337974407435408, +0.0871006254165859, -0.0700872943341148 };

    double y3_r[] = { 0.159873291739938, 0.176189084778751, -0.125000861991663 };
    double y3_i[] = { +0.423624481974432, +0.083592966124902, +0.116007083015052 };

    std::vector<complex> x = FillTestData(x5_r, x5_i, 5);
    std::vector<complex> y5 = FillTestData(y5_r, y5_i, 5);
    std::vector<complex> y8 = FillTestData(y8_r, y8_i, 8);
    std::vector<complex> y3 = FillTestData(y3_r, y3_i, 3);

    for (int i = 0; i < pairs.test_set_complex.size(); i++)
        pairs.test_set_complex[i].first = x;

    pairs.test_set_complex[0].second = y5;
    pairs.test_set_complex[1].second = y8;
    pairs.test_set_complex[2].second = y3;

    pairs.fft_len = { 5, 8, 3 };
}

void FFT_TEST::FillRealFFTSet(RealTestSetStruct& pairs, int num_test)
{
    pairs.test_set_double.resize(num_test);

    double x[7] = { 0.937385132542, 0.285668125456, 0.347477123146,  0.448955652385, 0.326826716591, 0.606108226383, 0.783588742762 };

    double yref7_r[] = {3.73600971927, 0.692907420144, 0.323994778293, 0.395940905829};
    double yref7_i[] = {+0.0, +0.588446936567, +0.468705124662, -0.105233188027};

    double yref16_r[] = {3.73600971927, 0.832790896735, 0.0665134613678, 1.50027312149, 0.133145983226, 0.991252110762, 1.15460337053, 0.425224401181, 1.05454571082};
    double yref16_i[] = {+0.0, -2.2106834422, +0.345237401692, -0.333125094869, -0.442820699453, +0.612790159453, -0.52698583754, +0.0425386784903, +0.0};

    double yref5_r[] = {2.34631275012, 0.48232879742, 0.687977658876};
    double yref5_i[] = {+0.0, +0.0987917292121, -0.0723194036227};

    std::vector<double> xx(x, x + 7);
    std::vector<complex> y7 = FillTestData(yref7_r, yref7_i, 7 / 2 + 1);
    std::vector<complex> y16 = FillTestData(yref16_r, yref16_i, 16 / 2 + 1);
    std::vector<complex> y5 = FillTestData(yref5_r, yref5_i, 5 / 2 + 1);

    for (int i = 0; i < pairs.test_set_double.size(); i++)
        pairs.test_set_double[i].first = xx;

    pairs.test_set_double[0].second = y7;
    pairs.test_set_double[1].second = y16;
    pairs.test_set_double[2].second = y5;

    pairs.fft_len = { 7, 16, 5 };
}

void FFT_TEST::FillComplexFFTSet(ComplexTestSetStruct& pairs, int num_test)
{
    pairs.test_set_complex.resize(num_test);
    // x
    double x5_r[] = { 0.293848340517710, 0.432658290043139, 0.638136660660825, 0.523377526876359, 0.146439036982003 };
    double x5_i[] = { 0.543040331839914, 0.507949811338692, 0.014141443357630, 0.431524418557386, 0.982433436513920 };
    std::vector<complex> x = FillTestData(x5_r, x5_i, 5);

    // y
    double y5_r[] = { 2.034459855080035, -1.163477761938224, 0.302336705735982, 0.066216063691746, 0.229706840019013 };
    double y5_i[] = { +2.479089441607542, +0.303378415338802, -0.584079752541479, -0.465893684778681, +0.982707239573387 };
    std::vector<complex> y5 = FillTestData(y5_r, y5_i, 5);

    double y8_r[] = { 2.034459855080035, 0.761711158054026, -0.121423890379805, 0.861724646436443, 0.122388221241041, -0.438609664267351, -0.274274675942418, -0.595188926080287 };
    double y8_i[] = { +2.479089441607542, -1.699508261045320, +1.602051561829424, -0.531316766704685, +0.600140981815385, -0.455551269624340, +1.420613088162984, +0.928803878678324 };
    std::vector<complex> y8 = FillTestData(y8_r, y8_i, 8);

    double y3_r[] = { 1.364643291221674, 0.186101456438663, -0.669199726107206 };
    double y3_i[] = { +1.065131586536237, +0.459944193374903, +0.104045215608603 };
    std::vector<complex> y3 = FillTestData(y3_r, y3_i, 3);

    for (int i = 0; i < pairs.test_set_complex.size(); i++)
        pairs.test_set_complex[i].first = x;

    pairs.test_set_complex[0].second = y5;
    pairs.test_set_complex[1].second = y8;
    pairs.test_set_complex[2].second = y3;

    pairs.fft_len = { 5, 8, 3 };

}

void FFT_TEST::GenerateSignal(std::vector<complex>& X, TestParamStruct params, int sel)
{
    // Input signal
    X.resize(params.L);
    memset(X.data(), 0, params.L * sizeof(complex));
    if (sel == 1)
    {
        for (int i = 0; i < params.L; i++)
        {
            double rand = (*m_rand.dist)(*m_rand.gen);
            //X[i].r = 0.7 * sin(2.0 * pi * 50.0 * (params.t[i] * params.T)) + sin(2.0 * pi * 120.0 * (params.t[i] * params.T)) + 2.0*rand;
            X[i].r = 0.7 * sin(2.0 * pi * 50.0 * (params.t[i] * params.T)) + sin(2.0 * pi * 120.0 * (params.t[i] * params.T));
            X[i].i = 0.0;
        }
    }
    else if (sel == 2)
    {
        for (int i = 0; i < params.L; i++)
        {
            params.t[i] = (params.t[i] - 50.0) / 100.0;
            X[i].r = 1.0 / (4.0*std::sqrt(2.0*pi*0.01)) * (std::exp(-(params.t[i] * params.t[i]) / (2.0*0.01)));
            X[i].i = 0.0;
        }
    }

}
