#pragma once

#include "test.h"
#include "example/test_convolution.h"
#include "example/test_fft.h"
#include "example/test_filter.h"
#include "example/test_poly.h"
#include "example/test_hilbert.h"