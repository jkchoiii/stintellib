#ifndef PAR_ALGORITHM_HEADER_
#define PAR_ALGORITHM_HEADER_

#ifndef HAVE_STRUCT_TIMESPEC
    #define HAVE_STRUCT_TIMESPEC
#endif

#define MAX_BEAM_COUNT 256
#define MAX_DATA_COUNT 2800
#define MAX_NUM_THREADS 4

#define USHRT_MAX     0xffff        // maximum unsigned short value
#define DBL_MIN       2.2250738585072014e-308 // minimum double value

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <pthread.h>

//#include "threadpool.h"
#include "ParAutogainDataStructure.h"
#include "ParAutogain.h"

#endif PAR_ALGORITHM_HEADER_