#ifndef USEFUL_DATA_TYPE_
#define USEFUL_DATA_TYPE_

typedef char int8_t;
typedef unsigned char uint8_t;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int int32_t;
typedef unsigned int uint32_t;
typedef long int64_t;
typedef long uint64_t;
typedef float float32_t;
typedef double float64_t;

#endif