#ifndef PAR_AUTOGAIN_DATA_STRUCTURE_
#define PAR_AUTOGAIN_DATA_STRUCTURE_

#include "UsefulDataType.h"

struct DataPairStruct
{
    uint16_t* raw;     // MAX_BEAM_COUNT * MAX_DATA_COUNT
    float64_t* src;     // MAX_BEAM_COUNT * MAX_DATA_COUNT
    float64_t* dst;     // MAX_BEAM_COUNT * MAX_DATA_COUNT
};

struct RawDataTypeBoundary
{
    int64_t upper;
    int64_t lower;
};

struct AutogainParameterStruct
{
    float64_t base;
    float64_t penalty;
    float64_t sample_freq;
    float64_t set_intense;
    uint32_t blank;
    uint32_t data_count;
    uint32_t range;
    struct RawDataTypeBoundary bound;
};

struct AlgorithmStruct
{
    struct DataPairStruct data;
    struct AutogainParameterStruct* params;
    struct SplineStruct* spline;

};


//

struct ParAutogainStruct
{
    int32_t tid;
    bool task_done;

    //float64_t* data_addr;
    int32_t start_data_idx;
    int32_t end_data_idx;
    int32_t thread_total_beam;
    int32_t start_beam;
    int32_t end_beam;
    int32_t total_beam;
    
    struct AlgorithmStruct* algo;
};

struct SplineCalculateStruct
{
    float64_t* s;           // MAX_BEAM_COUNT * MAX_DATA_COUNT
    float64_t* w;           // MAX_BEAM_COUNT * MAX_DATA_COUNT
    float64_t* e;           // MAX_BEAM_COUNT * MAX_DATA_COUNT
    float64_t* f;           // MAX_BEAM_COUNT * MAX_DATA_COUNT

    float64_t* a0;          // MAX_BEAM_COUNT
    float64_t* a1;          // MAX_BEAM_COUNT
    float64_t* mu;          // MAX_BEAM_COUNT
};

struct SplineResultStruct
{
    float64_t* s_pad;       // MAX_BEAM_COUNT * MAX_DATA_COUNT
    float64_t* gain_res;    // MAX_BEAM_COUNT * MAX_DATA_COUNT
    float64_t* spline_res;  // MAX_BEAM_COUNT * MAX_DATA_COUNT
};

struct SplineStruct
{
    struct SplineCalculateStruct calc;
    struct SplineResultStruct result;
};

#endif PAR_AUTOGAIN_DATA_STRUCTURE_