#include "ParAutogain.h"

//void AllocateAlgorithmDataStructure(struct AlgorithmStruct** algo, struct SplineStruct** spline, struct ParAutogainStruct** par_pack, threadpool_t** pool)
void AllocateAlgorithmDataStructure(struct AlgorithmStruct** algo, struct SplineStruct** spline, struct ParAutogainStruct** par_pack, pthread_t** threads)
{
    *algo = (struct AlgorithmStruct*)malloc(sizeof(struct AlgorithmStruct));
    
    (*algo)->data.src = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT * MAX_DATA_COUNT);
    (*algo)->data.dst = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT * MAX_DATA_COUNT);

    (*algo)->params = (struct AutogainParameterStruct*)malloc(sizeof(struct AutogainParameterStruct));
    memset((*algo)->params, 0, sizeof(struct AutogainParameterStruct));

    //

    *spline = (struct SplineStruct*)malloc(sizeof(struct SplineStruct));
    (*spline)->calc.s = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT * (MAX_DATA_COUNT - 2));
    (*spline)->calc.w = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT * (MAX_DATA_COUNT - 2));
    (*spline)->calc.e = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT * (MAX_DATA_COUNT - 2));
    (*spline)->calc.f = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT * (MAX_DATA_COUNT - 2));

    (*spline)->calc.a0 = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT);
    (*spline)->calc.a1 = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT);
    (*spline)->calc.mu = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT);

    (*spline)->result.gain_res = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT * MAX_DATA_COUNT);
    (*spline)->result.s_pad = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT * (MAX_DATA_COUNT + 2));
    (*spline)->result.spline_res = (float64_t*)malloc(sizeof(float64_t) * MAX_BEAM_COUNT * MAX_DATA_COUNT);

    //*pool = threadpool_create(MAX_NUM_THREADS, MAX_NUM_THREADS * 2, 0);
    (*threads) = (pthread_t*)malloc(sizeof(pthread_t) * MAX_NUM_THREADS);

    *par_pack = (struct ParAutogainStruct*)malloc(sizeof(struct ParAutogainStruct) * MAX_NUM_THREADS);

    (*algo)->spline = *spline;

     
    if (*par_pack != NULL) 
    {
        for (uint32_t i = 0; i < MAX_NUM_THREADS; i++)
        {
            (*par_pack)[i].algo = *algo;
        }
    }
    
    

}

//void FreeAlgorithmDataStructure(struct AlgorithmStruct** algo, struct SplineStruct** spline, struct ParAutogainStruct** par_pack, threadpool_t** pool)
void FreeAlgorithmDataStructure(struct AlgorithmStruct** algo, struct SplineStruct** spline, struct ParAutogainStruct** par_pack, pthread_t** threads)
{
    free((*algo)->data.src);
    free((*algo)->data.dst);
    free((*algo)->params);
    free(*algo);

    //

    free((*spline)->result.spline_res);
    free((*spline)->result.gain_res);
    free((*spline)->result.s_pad);

    free((*spline)->calc.a0);
    free((*spline)->calc.a1);
    free((*spline)->calc.mu);

    free((*spline)->calc.s);
    free((*spline)->calc.w);
    free((*spline)->calc.e);
    free((*spline)->calc.f);

    free(*spline);

    free(*par_pack);

    //threadpool_destroy(*pool, 1);
    free(*threads);

}

void SetAutogainParameters(struct AutogainParameterStruct* params, uint32_t data_count, uint32_t range, float32_t altitude, float64_t set_intense)
{
    params->blank = (uint32_t)floorf(data_count / (float32_t)range * altitude);
    params->data_count = data_count;
    params->sample_freq = 31012.5;
    params->range = range;

    params->penalty = 1e-1;
    params->base = 20.0;
    params->set_intense = set_intense;
    //params->bound.upper = USHRT_MAX;
    params->bound.upper = 255;
    params->bound.lower = 0;
}

//void SetDataForAlgorithm(uint16_t* src, float64_t* dst, uint32_t beam_count, uint32_t data_count)
void SetDataForAlgorithm(uint16_t* src, float64_t* dst, struct ParAutogainStruct* pack)
{
    uint32_t beam_count = pack->total_beam;
    uint32_t data_count = pack->algo->params->data_count;

    long data_idx = pack->start_beam * data_count;
    for (uint32_t i = pack->start_beam; i < pack->end_beam; i++)
    {
        for (uint32_t j = 0; j < data_count; j++)
        {
            uint16_t val = src[beam_count * j + i];
            if (val > pack->algo->params->bound.upper)
                val = pack->algo->params->bound.upper;
            dst[data_idx] = val;
            data_idx++;
        }
    }
}

//void SetDataForDisplay(float64_t* src, uint16_t* dst, uint32_t beam_count, uint32_t data_count)
void SetDataForDisplay(float64_t* src, uint16_t* dst, struct ParAutogainStruct* pack)
{
    uint32_t beam_count = pack->total_beam;
    uint32_t data_count = pack->algo->params->data_count;

    long data_idx = pack->start_beam * data_count;

    //long data_idx = 0;
    for (uint32_t i = 0; i < data_count; i++)
    {
        for (uint32_t j = pack->start_beam; j < pack->end_beam; j++)
        {
            float64_t val = src[data_count * j + i];
            if (val > pack->algo->params->bound.upper)
                val = pack->algo->params->bound.upper;
            dst[data_idx] = val;
            data_idx++;
        }
    }
}

uint32_t SetParallelPack(struct ParAutogainStruct* par_pack, struct AlgorithmStruct* algo, int32_t beam_count, int32_t data_count)
{
    uint32_t num_threads = 1;

    if (algo->params->range > 30)
    {
        num_threads = 4;
    }
    else
    {
        num_threads = 2;
    }

    num_threads = 4; // linux test


    int32_t total_beam = beam_count / num_threads;
    uint32_t rem = beam_count % num_threads;

    for (uint32_t i = 0; i < num_threads; i++)
    {
        par_pack[i].tid = i;
        par_pack[i].task_done = false;

        if (i != num_threads - 1)
        {
            FillDataIndexInfomation(par_pack, i, total_beam, beam_count, data_count, 0);
        }
        else
        {
            FillDataIndexInfomation(par_pack, i, total_beam + rem, beam_count, data_count, rem);
        }
    }

    return num_threads;
}

void EvalSecondOrderDifferential(const float64_t* src, float64_t* dst, uint32_t data_count)
{
    for (uint32_t i = 0; i < data_count - 2; i++)
    {
        dst[i] = src[i] - 2.0 * src[i + 1] + src[i + 2];
    }
}

void InitSimpleSpline(struct AlgorithmStruct* algo, uint32_t acc, uint32_t beam_idx)
{
    uint32_t data_count = algo->params->data_count;
    uint32_t cur_pos = beam_idx * (data_count - 2);
    uint32_t blank = algo->params->blank;

    memset(&algo->spline->calc.s[cur_pos], 0, sizeof(float64_t) * (data_count - 2));
    memset(&algo->spline->calc.w[cur_pos], 0, sizeof(float64_t) * (data_count - 2));
    memset(&algo->spline->calc.e[cur_pos], 0, sizeof(float64_t) * (data_count - 2));
    memset(&algo->spline->calc.f[cur_pos], 0, sizeof(float64_t) * (data_count - 2));
    memset(&algo->spline->result.s_pad[beam_idx * (data_count + 2)], 0, sizeof(float64_t) * (2 + data_count));

    cur_pos = cur_pos + blank;

    EvalSecondOrderDifferential(&algo->data.src[acc], &algo->spline->calc.w[cur_pos], data_count - blank);

    struct SplineCalculateStruct* sp = &algo->spline->calc;
    
    sp->a0[beam_idx] = 6.0 + algo->params->penalty * 2.0 / 3.0;
    sp->a1[beam_idx] = 4.0 - algo->params->penalty / 6.0;
    
    sp->f[cur_pos] = 1.0 / sp->a0[beam_idx];
    sp->s[cur_pos] = sp->f[cur_pos] * sp->w[cur_pos];
    sp->e[cur_pos] = sp->a1[beam_idx] * sp->f[cur_pos];
    
    sp->f[1 + cur_pos] = 1.0 / (sp->a0[beam_idx] - sp->a1[beam_idx] * sp->e[cur_pos]);
    sp->s[1 + cur_pos] = sp->f[1 + cur_pos] * (sp->w[1 + cur_pos] + sp->a1[beam_idx] * sp->s[cur_pos]);
    
    sp->mu[beam_idx] = sp->a1[beam_idx] - sp->e[cur_pos];
    sp->e[1 + cur_pos] = sp->mu[beam_idx] * sp->f[1 + cur_pos];

}

void EvalSplineSmooth(uint32_t beam_idx, struct AlgorithmStruct* algo)
{
    struct SplineCalculateStruct* spline = &algo->spline->calc;
    
    uint32_t access_pts = beam_idx * (algo->params->data_count - 2);
    uint32_t blank = algo->params->blank;
    uint32_t acc = access_pts + blank;
    uint32_t data_count = algo->params->data_count - blank;
    
    //printf("%u %u\n", beam_idx, beam_idx * algo->params->data_count);

    InitSimpleSpline(algo, acc, beam_idx);

    for (uint32_t i = 2; i < data_count - 2; i++)
    {
        spline->f[acc + i] = 1.0 / (spline->a0[beam_idx] - spline->mu[beam_idx] * spline->e[acc + i - 1] - spline->f[acc + i - 2]);
        spline->s[acc + i] = spline->f[acc + i] * (spline->w[acc + i] + spline->mu[beam_idx] * spline->s[acc + i - 1] - spline->s[acc + i - 2]);
        spline->mu[beam_idx] = spline->a1[beam_idx] - spline->e[acc + i - 1];
        spline->e[acc + i] = spline->mu[beam_idx] * spline->f[acc + i];
    }
    spline->s[acc + data_count - 4] += spline->e[acc + data_count - 4] * spline->s[acc + data_count - 3];

    for (int32_t i = data_count - 5; i >= 0; i--)
    {
        spline->s[acc + i] = spline->s[acc + i] + spline->e[acc + i] * spline->s[acc + i + 1] - spline->f[acc + i] * spline->s[acc + i + 2];
    }

    memcpy(&algo->spline->result.s_pad[(beam_idx * (algo->params->data_count + 2)) + 2], &spline->s[access_pts], sizeof(float64_t) * (algo->params->data_count - 2));

    EvalSecondOrderDifferential(&algo->spline->result.s_pad[(beam_idx * (algo->params->data_count + 2))], &algo->spline->result.spline_res[access_pts], algo->params->data_count + 2);

    for (uint32_t i = 0; i < data_count; i++)
    {
        algo->spline->result.spline_res[acc + i] = algo->data.src[acc + i] - algo->spline->result.spline_res[acc + i];
    }
}


void GetSmoothedBase(uint32_t beam_idx, float64_t* spline_res, struct AutogainParameterStruct* params)
{
    uint32_t access_pts = beam_idx * params->data_count;
    
    float64_t max_val = DBL_MIN;

    for (uint32_t i = params->blank; i < params->data_count; i++)
    {
        if (max_val < spline_res[access_pts + i])
            max_val = spline_res[access_pts + i];
    }

    float64_t smooth_base = max_val * params->base * 0.01;

    for (uint32_t i = params->blank; i < params->data_count; i++)
    {
        if (spline_res[access_pts + i] < smooth_base)
            spline_res[access_pts + i] = smooth_base;
    }
}

void GetGainSignal(uint32_t beam_idx, float64_t* raw, float64_t* spline_res, float64_t* gain_res, struct AutogainParameterStruct* params)
{
    uint32_t access_pts = beam_idx * params->data_count;

    for (uint32_t i = params->blank; i < params->data_count; i++)
    {
        gain_res[access_pts + i] = raw[access_pts + i] / spline_res[access_pts + i];
    }

    float64_t gainer = spline_res[access_pts + params->blank];
    for (uint32_t i = 0; i < params->blank; i++)
    {
        gain_res[access_pts + i] = raw[access_pts + i] / gainer;
    }
    
}

void GetRescaledGainSignal(uint32_t beam_idx, float64_t* gain_res, float64_t* dst, struct AutogainParameterStruct* params)
{
    uint32_t access_pts = beam_idx * params->data_count;

    float64_t intense_min = 20.0;
    float64_t intense_mid = 5.0;
    float64_t intense_max = 1.0;


    // define the slope
    float64_t slope_lower = (intense_min - intense_mid) / 50.0;
    float64_t slope_higher = (intense_mid - intense_max) / 50.0;

    float64_t cut_low = -0.2;
    float64_t cut_high = 0.0;
    if (params->set_intense < 50.0)
    {
        cut_high = intense_min - slope_lower * params->set_intense;
    }
    else
    {
        cut_high = intense_mid - slope_higher * (params->set_intense - 50.0);
    }

    // trunc values
    for (uint32_t i = 0; i < params->data_count; i++)
    {
        dst[access_pts + i] = gain_res[access_pts + i] * (255 / (cut_high - cut_low));

        if (dst[access_pts + i] > params->bound.upper)
        {
            dst[access_pts + i] = params->bound.upper;
        }
        else if (dst[access_pts + i] < params->bound.lower)
        {
            dst[access_pts + i] = params->bound.lower;
        }

    }

}

void* Process(struct ParAutogainStruct* pack)
{
    SetDataForAlgorithm(pack->algo->data.raw, pack->algo->data.src, pack);

    for (uint32_t i = pack->start_beam; i <= pack->end_beam; i++)
    {
        EvalSplineSmooth(i, pack->algo);
        GetSmoothedBase(i, pack->algo->spline->result.spline_res, pack->algo->params);
        GetGainSignal(i, pack->algo->data.src, pack->algo->spline->result.spline_res, pack->algo->spline->result.gain_res, pack->algo->params);
        GetRescaledGainSignal(i, pack->algo->spline->result.gain_res, pack->algo->data.dst, pack->algo->params);

    }

    SetDataForDisplay(pack->algo->data.dst, pack->algo->data.raw, pack);
    //printf("%d DONE! \n", pack->tid);
    pack->task_done = true;

    return NULL;
}

void FillDataIndexInfomation(struct ParAutogainStruct* pack, int32_t index, int32_t thread_total_beam, int32_t beam_count, int32_t data_count, uint32_t rem)
{
    pack[index].total_beam = beam_count;
    pack[index].thread_total_beam = thread_total_beam;

    pack[index].start_beam = index * (pack[index].thread_total_beam - rem);
    pack[index].end_beam = pack[index].start_beam + pack[index].thread_total_beam - 1;

    pack[index].start_data_idx = pack[index].start_beam * data_count;
    pack[index].end_data_idx = (pack[index].end_beam + 1) * data_count - 1;
    

}
