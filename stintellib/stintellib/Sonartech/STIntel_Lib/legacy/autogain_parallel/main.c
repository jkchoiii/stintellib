#include "ParAlgorithmHeader.h"
//#include "FileRead.h"
//#include <wincrypt.h>


struct ReadDataStruct
{
    uint16_t* beam_count_vec;
    uint16_t* data_count_vec;
    uint16_t* full_data;
};

struct ReadDataStruct* ReadFullData(uint32_t ping_length)
{
    struct ReadDataStruct* d = (struct ReadDataStruct*)malloc(sizeof(struct ReadDataStruct));
    
    d->beam_count_vec = (uint16_t*)malloc(sizeof(uint16_t) * ping_length);
    d->data_count_vec = (uint16_t*)malloc(sizeof(uint16_t) * ping_length);

    // 20meter
    ReadFile(d->data_count_vec, "diver_datacount_271202_2242.ac1.bin");
    ReadFile(d->beam_count_vec, "diver_beamcount_271202_2242.ac1.bin");


    uint32_t total_length = 0;
    for (uint32_t i = 0; i < ping_length; i++)
    {
        //full_data[ii] = (uint16_t*)malloc(sizeof(uint16_t)*beam_count_vec[ii] * data_count_vec[ii]);
        total_length += d->beam_count_vec[i] * d->data_count_vec[i];
    }
    d->full_data = (uint16_t*)malloc(sizeof(uint16_t) * total_length);
    ReadFile(d->full_data, "diver_full_data_271202_2242.ac1.bin");

    return d;
}

int main()
{
    uint32_t ping_length = 280;
    struct ReadDataStruct* full_data = ReadFullData(ping_length);
    int32_t data_count = full_data->data_count_vec[0];
    int32_t beam_count = full_data->beam_count_vec[0];
    uint16_t* linux_data = (uint16_t*)malloc(sizeof(uint16_t) * data_count * beam_count);
    
    double start, end, total = 0;
    double fs = 31012.5;

    //
    uint32_t range = data_count * 750.0 / fs;
    //

    float32_t altitude = 0.0;
    struct AlgorithmStruct* algo = NULL;
    struct SplineStruct* spline = NULL;
    struct ParAutogainStruct* par_pack = NULL;
    pthread_t* threads = NULL;
    AllocateAlgorithmDataStructure(&algo, &spline, &par_pack, &threads);
    algo->data.raw = linux_data;
    
    //
    float64_t set_intense = 60;
    
    for (uint32_t ping = 0; ping < ping_length; ping++)
    {
        memcpy(linux_data, full_data->full_data + (ping * data_count * beam_count), sizeof(uint16_t) * data_count * beam_count);

        //start = (double)clock() / CLOCKS_PER_SEC;

        SetAutogainParameters(algo->params, data_count, range, altitude, set_intense);
        uint32_t num_threads = SetParallelPack(par_pack, algo, beam_count, data_count);

        for (uint32_t i = 0; i < num_threads; i++)
        {
            pthread_create(&threads[i], NULL, Process, &par_pack[i]);
        }

        for (uint32_t i = 0; i < num_threads; i++)
        {
            pthread_join(threads[i], NULL);
            //printf("%d joined\n", i);
        }
        //printf("all joined... \n");


        //end = (double)clock() / CLOCKS_PER_SEC;
        //total += (end - start);

        //WriteFile(linux_data, data_count * beam_count, "D:/matlab_reader/reader/read_xtf/vs_res/vs_autogain.bin");
        //WriteFile(algo->data.dst, data_count * beam_count, "D:/matlab_reader/reader/read_xtf/vs_res/vs_autogain.bin");
    }

    //printf("elapsed time: %lf\n", total);

    FreeAlgorithmDataStructure(&algo, &spline, &par_pack, &threads);
    
    //
    free(linux_data);
    free(full_data->full_data);
    free(full_data->beam_count_vec);
    free(full_data->data_count_vec);

    return 0;
}

