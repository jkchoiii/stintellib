#ifndef PAR_AUTOGAIN_
#define PAR_AUTOGAIN_

#include "ParAlgorithmHeader.h"

void AllocateAlgorithmDataStructure(struct AlgorithmStruct** data, struct SplineStruct** spline, struct ParAutogainStruct** par_pack, pthread_t** threads);
void FreeAlgorithmDataStructure(struct AlgorithmStruct** algo, struct SplineStruct** spline, struct ParAutogainStruct** par_pack, pthread_t** threads);
//void AllocateAlgorithmDataStructure(struct AlgorithmStruct** data, struct SplineStruct** spline, struct ParAutogainStruct** par_pack, threadpool_t** pool);
//void FreeAlgorithmDataStructure(struct AlgorithmStruct** algo, struct SplineStruct** spline, struct ParAutogainStruct** par_pack, threadpool_t** pool);

void SetAutogainParameters(struct AutogainParameterStruct* params, uint32_t data_count, uint32_t range, float32_t altitude, float64_t set_intense);
void SetDataForAlgorithm(uint16_t* src, float64_t* dst, struct ParAutogainStruct* pack);
void SetDataForDisplay(float64_t* src, uint16_t* dst, struct ParAutogainStruct* pack);

uint32_t SetParallelPack(struct ParAutogainStruct* par_pack, struct AlgorithmStruct* algo, int32_t beam_count, int32_t data_count);

void* Process(struct ParAutogainStruct* pack);
void EvalSplineSmooth(uint32_t beam_idx, struct AlgorithmStruct* algo);
void InitSimpleSpline(struct AlgorithmStruct* algo, uint32_t acc, uint32_t beam_idx);
void EvalSecondOrderDifferential(const float64_t* src, float64_t* dst, uint32_t data_count);
void GetRescaledGainSignal(uint32_t beam_idx, float64_t* gain_res, float64_t* dst, struct AutogainParameterStruct* params);
void GetGainSignal(uint32_t beam_idx, float64_t* raw, float64_t* spline_res, float64_t* gain_res, struct AutogainParameterStruct* params);

void FillDataIndexInfomation(struct ParAutogainStruct* pack, int32_t index, int32_t thread_total_beam, int32_t beam_count, int32_t data_count, uint32_t rem);


#endif PAR_AUTOGAIN_