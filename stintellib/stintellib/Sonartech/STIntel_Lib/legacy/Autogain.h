#pragma once

#include <vector>
#include "../../DefineDataType.h"
#define NOMINMAX


namespace st
{
    ;  // 아래 선언되는 내용의 들여쓰기 방지를 위해 ";" 추가
struct IntegerNumericLimitStruct
{
    i64 min_lim;
    i64 max_lim;
};

struct SplineStruct
{
    // Fast Compact Algorithms and Software for Spline Smoothing,
    // Howard L. Weinert,
    // Springer p.1-17
    //
    std::vector<f64> s; // s
    std::vector<f64> w;
    std::vector<f64> e;
    std::vector<f64> f;

    std::vector<f64> s_pad;

    f64 a0;
    f64 a1;
    f64 mu;
};

struct IntensityStruct
{
    f64 min;
    f64 mid;
    f64 max;
    IntensityStruct()
    {
        min = 20.0;
        mid = 10.0;
        max = 1.0;
    }

};

struct Autogain
{
    f32 m_sample_freq;
    u32 m_blank;
    f64 m_penalty;
    //
    std::vector<f64> m_gain_result;
    std::vector<f64> m_spline_result;
    //std::vector<u16> m_spline;

    SplineStruct m_spline;


    IntensityStruct m_intense;
    const f64 m_cut_low = -0.1;
    f64 m_data_max_level = 16384.0;
    

private:
    void Init(i32 data_count, f32 altitude, u32 range);
    static void EvalSecondOrderDifferential(const f64* src, f64* dst, const i32 data_count);
    void InitSimpleSpline(const f64* data, f64 penalty, i32 data_count);
    void EvalSplineSmooth(const f64* data, f64* dst, f64 penalty, i32 data_count);
    static void GetSmoothedBase(std::vector<f64>& data, const u32 blank, const i32 data_count, const f64 percentage, const f64 base);
    static void GetGainSignal(const std::vector<f64>& raw, const std::vector<f64>& spline, std::vector<f64>& dst, const i32 data_count, const u32 blank);
    f64 GetIntensityCutHigh(const IntensityStruct& intense_params, const f64 intensity);
    void RescaleGainedData(const std::vector<f64>& gain_data, std::vector<f64>& dst, const u32 blank, const f64 intensity, const i32 data_count, const IntegerNumericLimitStruct limits);
    f64 GetDataMaxLevel();

public:
    Autogain();
    ~Autogain();

    i32 GetPossibleMaxIntensity(i32 max_intensity);
    void Process(std::vector<f64>& src, std::vector<f64>& dst, f64 intensity, i32 data_count, f32 altitude, u32 range, const IntegerNumericLimitStruct limits);
    void GetSplineGraph(std::vector<f64>& src, std::vector<f64>& dst, i32 data_count, f32 altitude, u32 range);
    void SetSmoothPenalty(f64 penalty);
    void SetDataMaxLevel(double data_max_level);
};


}


