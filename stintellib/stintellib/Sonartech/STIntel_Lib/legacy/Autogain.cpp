#include "../../stdafx.h"
#include "stdafx.h"

#include "Autogain.h"


namespace st
{
;  // 아래 선언되는 내용의 들여쓰기 방지를 위해 ";" 추가

Autogain::Autogain()
{
    const i32 reserve_size = 100000;
    m_gain_result.reserve(reserve_size);
    m_spline_result.reserve(reserve_size);
    m_spline.s.reserve(reserve_size);
    m_spline.f.reserve(reserve_size);
    m_spline.w.reserve(reserve_size);
    m_spline.e.reserve(reserve_size);
    m_spline.s_pad.reserve(reserve_size);
}

i32 Autogain::GetPossibleMaxIntensity(i32 max_intensity)
{
    i32 sel_max_intensity = 0;

    for (i32 i = max_intensity; i >= 0; i--)
    {
        f64 cut_high = GetIntensityCutHigh(m_intense, static_cast<f64>(i));
        if (cut_high - m_cut_low > 0)
        {
            sel_max_intensity = i;
            break;
        }
    }

    return sel_max_intensity;
}

Autogain::~Autogain()
{
}

void Autogain::Init(i32 data_count, f32 altitude, u32 range)
{
    m_gain_result.resize(data_count);
    m_spline_result.resize(data_count);
    std::fill(m_gain_result.begin(), m_gain_result.end(), 0);
    std::fill(m_spline_result.begin(), m_spline_result.end(), 0);

    const f32 c = 1500;
    m_sample_freq = static_cast<f32>(data_count) / static_cast<f32>(range) * (c / 2.0F);
    m_blank = static_cast<u32>(std::floor(static_cast<f32>(data_count) / static_cast<f32>(range) * altitude));
}

void Autogain::EvalSecondOrderDifferential(const f64* src, f64* dst, const i32 data_count)
{
    for (i32 i = 0; i < data_count - 2; i++)
    {
        dst[i] = src[i] - 2.0*src[i + 1] + src[i + 2];
    }
}

void Autogain::EvalSplineSmooth(const f64* data, f64* dst, f64 penalty, i32 data_count)
{
    InitSimpleSpline(data, penalty, data_count);

    for (i32 i = 2; i < data_count - 2; i++)
    {
        m_spline.f[i] = 1.0 / (m_spline.a0 - m_spline.mu*m_spline.e[i - 1] - m_spline.f[i - 2]);
        m_spline.s[i] = m_spline.f[i] * (m_spline.w[i] + m_spline.mu*m_spline.s[i - 1] - m_spline.s[i - 2]);
        m_spline.mu = m_spline.a1 - m_spline.e[i - 1];
        m_spline.e[i] = m_spline.mu*m_spline.f[i];
    }
    m_spline.s[data_count - 4] += m_spline.e[data_count - 4] * m_spline.s[data_count - 3];

    for (i32 i = data_count - 5; i >= 0; i--)
    {
        m_spline.s[i] = m_spline.s[i] + m_spline.e[i] * m_spline.s[i + 1] - m_spline.f[i] * m_spline.s[i + 2];
    }

    (void)std::copy(m_spline.s.begin(), m_spline.s.end(), m_spline.s_pad.begin() + 2);
    EvalSecondOrderDifferential(m_spline.s_pad.data(), dst, data_count + 2);

    for (i32 i = 0; i < data_count; i++)
    {
        dst[i] = data[i] - dst[i];
    }
    
}

void Autogain::GetSmoothedBase(std::vector<f64>& data, const u32 blank, const i32 data_count, const f64 percentage, const f64 base)
{
    f64 max_val = DBL_MIN;
    for (i32 i = static_cast<i32>(blank); i < data_count; i++)
    {
        if (max_val < data[i])
        {
            max_val = data[i];
        }
    }

    const f64 smooth_base = max_val * base * percentage;

    for (i32 i = static_cast<i32>(blank); i < data_count; i++)
    {
        if(data[i] < smooth_base)
        {
            data[i] = smooth_base;
        }
    }
}

void Autogain::GetGainSignal(const std::vector<f64>& raw, const std::vector<f64>& spline, std::vector<f64>& dst, const i32 data_count, const u32 blank)
{
    for (i32 i = static_cast<i32>(blank); i < data_count; i++)
    {
        dst[i] = raw[i] / spline[i];
    }

    const f64 gainer = spline[blank];
    for (i32 i = 0; i < static_cast<i32>(blank); i++)
    {
        dst[i] = raw[i] / gainer;
    }
    //for (i32 i = 0; i < blank; i++)
    //{
    //    dst[i] = raw[i];
    //}
}

void Autogain::InitSimpleSpline(const f64* data, f64 penalty, i32 data_count)
{
    m_spline.s.resize(data_count - 2);
    m_spline.w.resize(data_count - 2);
    m_spline.e.resize(data_count - 2);
    m_spline.f.resize(data_count - 2);
    m_spline.s_pad.resize(data_count + 2);
    
    std::fill(m_spline.s.begin(), m_spline.s.end(), 0);
    std::fill(m_spline.w.begin(), m_spline.w.end(), 0);
    std::fill(m_spline.e.begin(), m_spline.e.end(), 0);
    std::fill(m_spline.f.begin(), m_spline.f.end(), 0);
    std::fill(m_spline.s_pad.begin(), m_spline.s_pad.end(), 0);
    
    // second order differential
    EvalSecondOrderDifferential(data, m_spline.w.data(), data_count);

    // toeplitz values
    m_spline.a0 = 6.0 + penalty * 2.0 / 3.0;
    m_spline.a1 = 4.0 - penalty / 6.0;

    // initial values
    m_spline.f[0] = 1.0 / m_spline.a0;
    m_spline.s[0] = m_spline.f[0] * m_spline.w[0];
    m_spline.e[0] = m_spline.a1*m_spline.f[0];

    m_spline.f[1] = 1.0 / (m_spline.a0 - m_spline.a1*m_spline.e[0]);
    m_spline.s[1] = m_spline.f[1] * (m_spline.w[1] + m_spline.a1*m_spline.s[0]);

    m_spline.mu = m_spline.a1 - m_spline.e[0];
    m_spline.e[1] = m_spline.mu*m_spline.f[1];

}


void Autogain::Process(std::vector<f64>& src, std::vector<f64>& dst, f64 intensity, i32 data_count, f32 altitude, u32 range, const IntegerNumericLimitStruct limits)
{
    Init(data_count, altitude, range);

    EvalSplineSmooth(&src[m_blank], &m_spline_result[m_blank], m_penalty, data_count- static_cast<i32>(m_blank));
    GetSmoothedBase(m_spline_result, m_blank, data_count, 0.01, 20);
    GetGainSignal(src, m_spline_result, m_gain_result, data_count, m_blank);

    RescaleGainedData(m_gain_result, dst, m_blank, intensity, data_count, limits);

}

void Autogain::GetSplineGraph(std::vector<f64>& src, std::vector<f64>& dst, i32 data_count, f32 altitude, u32 range)
{
    Init(data_count, altitude, range);
    EvalSplineSmooth(&src[m_blank], &dst[m_blank], m_penalty, data_count - static_cast<i32>(m_blank));
}

f64 Autogain::GetIntensityCutHigh(const IntensityStruct& intense_params, const f64 intensity)
{
    f64 cut_high = 0;
    if (intensity <= 50.0)
    {
        f64 div = 5.0;
        const f64 slope_lower = (intense_params.min - intense_params.mid) / 50.0;
        cut_high = (intense_params.min - slope_lower * intensity) * ((50.0 / div + 1.0) - intensity / div);
    }
    else
    {
        const f64 slope_higher = (intense_params.mid - intense_params.max) / 50.0;
        cut_high = intense_params.mid - slope_higher * (intensity - 50.0);
    }

    return cut_high;
}

void Autogain::RescaleGainedData(const std::vector<f64>& gain_data, std::vector<f64>& dst, u32 /*blank*/, const f64 intensity, const i32 data_count, const IntegerNumericLimitStruct limits)
{
    // define the slope
    //const f64 cut_low = -0.2;
    f64 cut_high = GetIntensityCutHigh(m_intense, intensity);

    // trunc values
    for (i32 i = 0; i < data_count; i++)
    {
        dst[i] = gain_data[i] * (GetDataMaxLevel() / (cut_high - m_cut_low));

        if(dst[i] > static_cast<f64>(limits.max_lim))
        {
            dst[i] = static_cast<f64>(limits.max_lim);
        }
        else if(dst[i] < static_cast<f64>(limits.min_lim))
        {
            dst[i] = static_cast<f64>(limits.min_lim);
        }
        else
        {
            //no thing
        }

    }

}

f64 Autogain::GetDataMaxLevel()
{
    return m_data_max_level;
}

void Autogain::SetSmoothPenalty(f64 penalty)
{
    m_penalty = penalty;
}

void Autogain::SetDataMaxLevel(double data_max_level)
{
    m_data_max_level = data_max_level;
}
}
