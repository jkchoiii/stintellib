#pragma once
#include <string>
#include <memory>
#include "core/STIntelLib.h"
#include "core/algorithm/convolution.h"
#include "core/algorithm/fft.h"
#include "core/algorithm/hilbert.h"
#include "core/algorithm/envelope.h"
#include "core/polynomial/poly.h"
#include "core/signal/design/buttap.h"
#include "core/signal/design/besselap.h"
#include "core/signal/design/chevy1ap.h"
#include "core/signal/design/chevy2ap.h"
//
#include "core/signal/filter/iirfilter.h"
#include "core/signal/filter/filtfilt.h"
//


class STInterface
{
public:
    virtual ~STInterface() = default;
    // template method
    virtual std::shared_ptr<STIntelLib> Create() final
    {
        return this->Factory();
    }

protected:
    // factory method
    virtual std::shared_ptr<STIntelLib> Factory() = 0;
};

template <typename Product>
class StandardCreator final : public STInterface
{
protected:

    virtual std::shared_ptr<STIntelLib> Factory() override
    {
        return std::make_shared<Product>();
    }
};


