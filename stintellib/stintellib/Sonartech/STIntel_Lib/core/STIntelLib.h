#pragma once
#define SCL_SECURE_NO_WARNINGS
#include <memory>
#include "../backend/Memory.h"


#ifdef _MSC_VER
#if _MSC_VER > 1920
#define PRAGMA_OMP_SIMD __pragma(omp simd)
#else
#define PRAGMA_OMP_SIMD //__pragma(omp parallel for)
#endif
#endif

struct ComplexDoubleStruct
{
    double r;
    double i;

    ComplexDoubleStruct()
    {
        r = 0.0;
        i = 0.0;
    }
    ComplexDoubleStruct(const double rv, const double iv)
    {
        r = rv;
        i = iv;
    }
};
typedef ComplexDoubleStruct complex;

class STIntelLib : protected STMemoryManager
{

protected:
    
    struct DestinationStruct 
    {
        void* dst;
        int length;
        DestinationStruct() 
        {
            dst = nullptr;
            length = -1;
        }
    };

    struct StateStruct 
    {
        bool param_state;
        bool dst_state;
        StateStruct()
        {
            param_state = false;
            dst_state = false;
        }

    };
    StateStruct m_state;

    STError_enum err = ST_SUCCESS;
    int64_t m_beam_count{};
    int64_t m_data_count{};
    
    enum STError_enum IsAlgorithmReady() 
    {
        return ((m_state.param_state & m_state.dst_state) == true) ? ST_SUCCESS : ST_INVALID_INPUT;
    }
    
    void InitSetParameters() 
    {
        m_state.param_state = true;
    }

    bool GetStateParameter()
    {
        return m_state.param_state;
    }

    void InitSetDestination() 
    {
        m_state.dst_state = true;
    }

    bool GetStateDestination()
    {
        return m_state.param_state;
    }

    void UninitSetParameters()
    {
        m_state.dst_state = false;
        m_state.param_state = false;
    }

    bool GetStateReady()
    {
        return m_state.dst_state & m_state.param_state;
    }

    DestinationStruct m_result;
    
    int GetLengthOfDestination() const
    {
        int ret_val = -1;

        if (m_state.param_state == true)
        {
            ret_val = m_result.length;
        }

        return ret_val;
    }

    virtual enum STError_enum SetDestination(void* dst)
    {
        enum STError_enum ierr = ST_SUCCESS;

        m_result.dst = dst;
        InitSetDestination();

        /*if (m_state.param_state == true)
        {
        m_result.dst = dst;
        InitSetDestination();
        }
        else
        {
        ierr = ST_INVALID_INPUT;
        }*/

        return ierr;
    }

    virtual enum STError_enum ProcessDetail() = 0;

public:
    enum INPUT_DATA_TYPE
    {
        REAL,
        COMPLEX,
        UNKNOWN
    };

    template <typename T>
    struct ResultStruct
    {
        std::unique_ptr<T[]> dst;
        int length;
        ResultStruct()
        {
            //dst = nullptr;
            length = -1;
        }
    };






    enum STError_enum Process() {

        enum STError_enum ierr = ST_INVALID_INPUT;

        if (IsAlgorithmReady() == ST_SUCCESS) 
        {
            ierr = ProcessDetail();
        }

        UninitSetParameters();

        return ierr;
    }

    STIntelLib()
    {
        m_beam_count = 0;
        m_data_count = 0;
    }

    ~STIntelLib() = default;

    
    template <typename T>
    auto GetParameterStruct(const std::shared_ptr<STIntelLib> obj)
    {   
        return ((T*)(obj.get()))->template GetParameterStruct<double>();
    }

    template <typename T, INPUT_DATA_TYPE INPUT_TYPE>
    auto GetParameterStruct(const std::shared_ptr<STIntelLib> obj)
    {
        if (INPUT_TYPE == COMPLEX)
        {
            return ((T*)(obj.get()))->template GetParameterStruct<complex>();
        }
    }

    template<typename T>
    STError_enum SetParameterStruct(std::shared_ptr<STIntelLib>& obj, void* param)
    {
        return ((T*)(obj.get()))->SetParameters(param, REAL);
    }

    template<typename T, INPUT_DATA_TYPE INPUT_TYPE>
    STError_enum SetParameterStruct(std::shared_ptr<STIntelLib>& obj, void* param)
    {
        STError_enum ierr = ST_INVALID_INPUT;

        if(INPUT_TYPE == COMPLEX)
        {
            ierr = ((T*)(obj.get()))->SetParameters(param, COMPLEX);
        }

        return ierr;
    }

    template <typename T>
    auto GetDestinationStruct(const std::shared_ptr<STIntelLib> obj)
    {
        return ((T*)(obj.get()))->template GetDestinationStruct<double>();
    }

    template <typename T, INPUT_DATA_TYPE INPUT_TYPE>
    auto GetDestinationStruct(const std::shared_ptr<STIntelLib> obj)
    {
        if (INPUT_TYPE == COMPLEX)
        {
            return ((T*)(obj.get()))->template GetDestinationStruct<complex>();
        }

    }

    template<typename T>
    STError_enum SetDestinationStruct(std::shared_ptr<STIntelLib>& obj, void* dst) 
    {// hi
        return ((T*)(obj.get()))->SetDestination(dst);
    }

    template<typename T, INPUT_DATA_TYPE INPUT_TYPE>
    STError_enum SetDestinationStruct(std::shared_ptr<STIntelLib>& obj, void* dst)
    {
        return ((T*)(obj.get()))->SetDestination(dst);
    }

};
