#pragma once

#include <complex>
#include <vector>
#include "polynomial.h"


class Poly : public Polynomial
{
public:
    template<typename T>
    struct PolynomialParameterStruct
    {
        int nord;
        int np;
        T* p;

        PolynomialParameterStruct()
        {
            nord = -1;
            //ny = -1;
            np = -1;
            p = nullptr;
        }
    };

    template <typename T>
    struct PolynomialDestinationStruct
    {
        std::vector<T> y;
    };



private:


    PolynomialParameterStruct<void> m_param;



private:
    template<typename T>
    STError_enum CheckVarSize(const PolynomialParameterStruct<T>& param, void* dst);

    template<typename T>
    STError_enum GetPolynomialRoots(const int nord, T* p, T* y, T one, T zero);

    template<typename T>
    void GetReversedResult(int nord, T* dst);

    STError_enum ProcessDetail() override;

    STError_enum SetParameters(const int nord, const int np, void* p, INPUT_DATA_TYPE input_type);

    template <typename T>
    void ResizeDestinationStruct(DestinationStruct& result)
    {
        ((PolynomialDestinationStruct<T>*)(result.dst))->y.resize(GetLengthOfDestination());
    }

public:

    Poly()
    {
    }

    virtual ~Poly()
    {
    }

    template<typename T>
    PolynomialParameterStruct<T> GetParameterStruct()
    {
        return PolynomialParameterStruct<T>();
    }

    template<typename T>
    PolynomialDestinationStruct<T> GetDestinationStruct()
    {
        return PolynomialDestinationStruct<T>();
    }

    STError_enum SetParameters(void* param, INPUT_DATA_TYPE input_type)
    {
        auto p = static_cast<PolynomialParameterStruct<void>*>(param);
        const auto ierr = SetParameters(p->nord, p->np, p->p, input_type);

        return ierr;
    }

    virtual enum STError_enum SetDestination(void* dst) override
    {
        enum STError_enum ierr = ST_SUCCESS;

        m_result.dst = dst;
        InitSetDestination();

        /*if (m_state.param_state == true)
        {
        m_result.dst = dst;
        InitSetDestination();
        }
        else
        {
        ierr = ST_INVALID_INPUT;
        }*/

        return ierr;
    }

};

template <typename T>
STError_enum Poly::CheckVarSize(const PolynomialParameterStruct<T>& param, void* dst)
{
    STError_enum ierr = ST_SUCCESS;

    if (param.nord < 0)
    {
        STPrintError("Polynomial order cannot be negative");
        ierr = ST_INVALID_INPUT;
    }

    if (ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(dst, &ierr);
    }

    // More error handling
    if (ierr == ST_SUCCESS)
    {
        STReturnArrayTooSmallError(param.np, param.nord, &ierr);
        if (ierr == ST_SUCCESS)
        {
            STReturnNullPointerError(param.p, &ierr);
        }
    }

    return ierr;
}

template <typename T>
STError_enum Poly::GetPolynomialRoots(const int nord, T* p, T* y, T one, T zero)
{
    STError_enum ierr = ST_SUCCESS;

    // Special case 
    if (nord == 0)
    {
        y[0] = 1.0;
    }
    else if (nord == 1) // Linear case
    {
        y[0] = 1.0;
        y[1] = -p[0];
    }
    else
    {
        // Set space
        T* temp1;
        T* temp2;
        CallocMemory(&temp1, nord + 1, &ierr);
        if (ierr == ST_SUCCESS)
        {
            CallocMemory(&temp2, nord + 1, &ierr);
            if (ierr == ST_SUCCESS)
            {
                // Initialize
                y[0] = -p[0];
                y[1] = one;  //y =-p_1 + x 
                for (int i = 2; i <= nord; i++) {
                    // x*(a_0 + a_1 x + ... + a_n x^{n-1}) = a_0 x + a_1 x^2 + ... + a_n x^i
                    for (int j = i; j >= 1; j--)
                    {
                        temp1[j] = y[j - 1]; // shift right
                    }
                    temp1[0] = zero;
                    // -p_i*(a_0 + .... + a_n x^{i-1}) =-p_i a_0 - ... p_i a_n x^{i-1}
                    for (int j = 1; j <= i; j++)
                    {
                        temp2[j - 1] = -y[j - 1] * p[i - 1]; // multiply
                    }
                    temp2[i] = zero;
                    // -p_i a_0 + p_i a_1 x + ... + p_i a_n x^{i-1} 
                    //          -     a_0 x - ...                   - a_n x^i
                    for (int j = 1; j <= i + 1; j++)
                    {
                        y[j - 1] = temp1[j - 1] + temp2[j - 1]; // sum previous two loops
                    }

                }

                // Free space
                FreeMemory(&temp1);
                FreeMemory(&temp2);
            }
        }
        
    }

    return ierr;
}

template <typename T>
void Poly::GetReversedResult(int nord, T* y)
{
    // Reverse y for consistency with matlab
    int mn = (nord + 1);
    T temp;
    for (int j = 0; j < mn / 2; j++)
    {
        temp = y[j];
        y[j] = y[mn - 1 - j];
        y[mn - 1 - j] = temp;
    }

}
