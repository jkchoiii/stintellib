#pragma once

#include <complex>
#include <string>
#include <fftw/fftw3.h>
#include <fftw/fftw3_mkl.h>
#include <mkl_cblas.h>
#include <vector>

#include "../STIntelLib.h"

class Polynomial : public STIntelLib
{

protected:
    std::vector<std::complex<double>> cmplx_dst;
    std::vector<std::complex<double>> cmplx_p;
    

private:
    virtual STError_enum ProcessDetail() = 0;
        
public:

    
    Polynomial()
    {

    }
    virtual ~Polynomial()
    {
    }

    //STError_enum SetParameters(const int nord, ALGORITHM_TYPE algorithm_type, INPUT_DATA_TYPE input_data_type);

protected:
    INPUT_DATA_TYPE m_input_type;

    
    
};
