#include "poly.h"


STError_enum Poly::ProcessDetail()
{
    STError_enum ierr = CheckVarSize(m_param, m_result.dst);

    if (ierr == ST_SUCCESS)
    {
        if(m_input_type == REAL)
        {
            ResizeDestinationStruct<double>(m_result);

            const double one = 1.0;
            const double zero = 0.0;
            double* dst_ptr = static_cast<PolynomialDestinationStruct<double>*>(m_result.dst)->y.data();
            GetPolynomialRoots(m_param.nord, static_cast<double*>(m_param.p), dst_ptr, one, zero);
            GetReversedResult(m_param.nord, dst_ptr);
        }
        else
        {
            ResizeDestinationStruct<complex>(m_result);
            complex* dst_ptr = static_cast<PolynomialDestinationStruct<complex>*>(m_result.dst)->y.data();

            const std::complex<double> one(1.0, 0.0);
            const std::complex<double> zero(0.0, 0.0);
            GetPolynomialRoots(m_param.nord, cmplx_p.data(), cmplx_dst.data(), one, zero);
            // Purge any numerical junk
            for (int i = 0; i<m_param.nord + 1; i++)
            {
                if (abs(cmplx_dst[i].imag()) < std::numeric_limits<double>::epsilon())
                {
                    cmplx_dst[i] = std::complex<double>(cmplx_dst[i].real(), 0.0);
                }
            }

            GetReversedResult(m_param.nord, cmplx_dst.data());

            for (int i = 0; i < m_result.length; i++)
            {
                dst_ptr[i].r = cmplx_dst[i].real();
                dst_ptr[i].i = cmplx_dst[i].imag();
            }
        }
    }
    
    return ierr;

}

STError_enum Poly::SetParameters(const int nord, const int np, void* p, INPUT_DATA_TYPE input_data_type)
{
    STError_enum ierr = ST_SUCCESS;

    if (nord < 0)
    {
        STPrintError("polynomial order cannot be negative");
        ierr = ST_INVALID_INPUT;
    }
    STReturnArrayTooSmallError(np, nord, &ierr);
    if(ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(p, &ierr);
    }
        
    if (ierr == ST_SUCCESS)
    {
        //m_algo_type = algorithm_type;
        m_input_type = input_data_type;

        // Set space 
        m_result.length = nord + 1;
        //m_param.ny = nord + 1;
        m_param.nord = nord;
        m_param.np = np;
        m_param.p = p;

        if (input_data_type == COMPLEX)
        {
            cmplx_dst.resize(m_result.length);
            std::fill(cmplx_dst.begin(), cmplx_dst.end(), 0);
            cmplx_p.resize(nord);
            for (int i = 0; i < np; i++)
            {
                cmplx_p[i].real(static_cast<complex*>(p)[i].r);
                cmplx_p[i].imag(static_cast<complex*>(p)[i].i);
            }
        }


        InitSetParameters();
    }

    return ierr;
}
