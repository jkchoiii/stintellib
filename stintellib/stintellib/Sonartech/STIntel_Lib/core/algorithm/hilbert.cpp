#include "hilbert.h"

Hilbert::Hilbert()
{
    
    //fft_forward.dst = fft_forward.fft_obj->GetDestinationStruct<FFT, FFT::COMPLEX>(fft_forward.fft_obj);
    //fft_forward.param = fft_forward.fft_obj->GetParameterStruct<FFT>(fft_forward.fft_obj);

    //fft_backward.dst = fft_backward.fft_obj->GetDestinationStruct<FFT, FFT::COMPLEX>(fft_backward.fft_obj);
    //fft_backward.param = fft_backward.fft_obj->GetParameterStruct<FFT, FFT::COMPLEX>(fft_backward.fft_obj);

}

Hilbert::~Hilbert()
{
}

STError_enum Hilbert::Transform2FrequencyDomain(const int n, double* xr, complex** hilbert)
{
    STError_enum ierr = ST_SUCCESS;

    CallocMemory(&m_calc.x, n, &ierr);
    if (ierr == ST_SUCCESS)
    {
        CallocMemory(hilbert, n, &ierr);
    }

    if (ierr == ST_SUCCESS)
    {
        // rfft
        fft_forward.param.nx = n;
        fft_forward.param.x = xr;
        fft_forward.param.len_transform = n;
        fft_forward.param.algo_type = FFT::FFT_FORWARD;
        fft_forward.param.fft_norm = FFT::NO_NEED_NORMALIZE;
        fft_forward.param.fft_type = FFT::REAL_FFT;

        fft_forward.fft_obj->SetParameters(&fft_forward.param, REAL);
        fft_forward.fft_obj->SetDestination(&fft_forward.dst);

        ierr = fft_forward.fft_obj->Process();

        memcpy(*hilbert, fft_forward.dst.Y.data(), sizeof(complex)*fft_forward.dst.Y.size());

    }

    return ierr;
}

STError_enum Hilbert::InverseFourierTransform(const int n, complex** x, complex* hilbert)
{
    fft_backward.param.nx = n;
    fft_backward.param.x = *x;
    fft_backward.param.len_transform = n;
    fft_backward.param.algo_type = FFT::IFFT_BACKWARD;
    fft_backward.param.fft_norm = FFT::NO_NEED_NORMALIZE;
    fft_backward.param.fft_type = FFT::COMPLEX_FFT;

    fft_backward.fft_obj->SetParameters(&fft_backward.param, COMPLEX);
    fft_backward.fft_obj->SetDestination(&fft_backward.dst);
    
    STError_enum ierr = fft_backward.fft_obj->Process();

    
    if (ierr == ST_SUCCESS)
    {
        memcpy(hilbert, fft_backward.dst.Y.data(), sizeof(complex)*fft_backward.dst.Y.size());
    }
    else
    {
        STPrintError("Failed to inverse transform!");
    }

    return ierr;
}

STError_enum Hilbert::ProcessDetail()
{
    STError_enum ierr = ST_SUCCESS;

    const double two = 2.0;
    const complex zero(0.0, 0.0);
    const int n = m_param.xr.size();

    
    if (ierr == ST_SUCCESS)
    {
        ierr = Transform2FrequencyDomain(n, m_param.xr.data(), &m_calc.h);

        if (ierr == ST_SUCCESS)
        {
            // copy h to workspace x
            cblas_zcopy(n, m_calc.h, 1, m_calc.x, 1);
        }
    }
    
    if(ierr == ST_SUCCESS)
    {
        // even case
        if (n % 2 == 0)
        {
            const int nw = n / 2 - 1;
            cblas_zdscal(nw, two, &m_calc.x[1], 1);
            const int upper_bound = 2 * nw + 2;

            if (upper_bound > n)
            {
                STPrintError("Bounds exceeded");
                ierr = ST_ALGORITHM_FAILURE;
            }

            if (ierr == ST_SUCCESS)
            {
                PRAGMA_OMP_SIMD
                for (int i = nw + 2; i < upper_bound; i++)
                {
                    m_calc.x[i] = zero;
                }
            }
        }
        else // odd case
        {
            int nw = (n + 1) / 2;

            cblas_zdscal(nw, two, &m_calc.x[1], 1);
            nw = n - (n + 1) / 2;
            const int upper_bound = (n + 1) / 2 + nw;
            if (upper_bound > n)
            {
                STPrintError("Bounds exceeded");
                ierr = ST_ALGORITHM_FAILURE;
            }

            if(ierr == ST_SUCCESS)
            {
                PRAGMA_OMP_SIMD
                for (int i = (n + 1) / 2; i < upper_bound; i++)
                {
                    m_calc.x[i] = zero;
                }
            }
        }
    }

    if (ierr == ST_SUCCESS)
    {
        ResizeDestinationStruct<complex>(m_result);
        
        //ierr = InverseFourierTransform(n, &m_calc.x, static_cast<complex*>(m_result.dst));
        ierr = InverseFourierTransform(n, &m_calc.x, static_cast<HilbertDestinationStruct<complex>*>(m_result.dst)->H.data());
    }
    

    // Free space


    FreeMemory(&m_calc.x);
    FreeMemory(&m_calc.h);


    return ierr;
}



STError_enum Hilbert::SetParameters(const int n, double* x)
{
    STError_enum ierr = ST_SUCCESS;

    STSetArrayTooSmallErrorAndReturnNULL(n, 1, &ierr);
    if(ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(x, &ierr);
    }

    if (ierr == ST_SUCCESS)
    {
        m_param.xr.resize(n);
        memcpy(m_param.xr.data(), x, sizeof(double)*n);

        m_result.length = n;

        InitSetParameters();
    }

    return ierr;
}
