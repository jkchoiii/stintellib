#include "envelope.h"

Envelope::Envelope()
{
}

Envelope::~Envelope()
{
}

STError_enum Envelope::GetRemovedDCSignal(double* xr, double* xrw, const int n, double* xmean)
{
    STError_enum ierr = ST_SUCCESS;

    IppStatus status = ippsMean_64f(xr, n, xmean);
    if (status != ippStsNoErr)
    {
        STPrintError("IPP failed to compute mean!");
        ierr = ST_IPP_FAILURE;
        *xmean = 0.0;
    }
    else
    {
        PRAGMA_OMP_SIMD
        for (int i = 0; i < n; i++)
        {
            xrw[i] = xr[i] - (*xmean);
        }
    }

    return ierr;
}

STError_enum Envelope::GetHilbertTransform(int n, double* xr, std::vector<complex>& ht)
{

    
    m_hilbert.param.xr.resize(n);
    memcpy(m_hilbert.param.xr.data(), xr, sizeof(double)*n);

    m_hilbert.ht->SetParameters(&m_hilbert.param, REAL);
    m_hilbert.ht->SetDestination(&m_hilbert.dst);

    const STError_enum ierr = m_hilbert.ht->Process();

    if (ierr != ST_SUCCESS)
    {
        STPrintError("Error computing hilbert transform");
    }
    else
    {
        ht.resize(m_hilbert.dst.H.size());
        std::copy(m_hilbert.dst.H.begin(), m_hilbert.dst.H.end(), ht.begin());
    }
    return ierr;
}

STError_enum Envelope::GetAbsoluteValuesForHilbertTransform(int n, Ipp64fc* ht, double* dst)
{
    STError_enum ierr = ST_SUCCESS;

    Ipp64fc* p_src = ht;
    Ipp64f* p_dst = static_cast<Ipp64f*>(dst);

    IppStatus status = ippsMagnitude_64fc(p_src, p_dst, n);

    if (status != ippStsNoErr)
    {
        STPrintError("Failed to compute magnitude");
        ierr = ST_IPP_FAILURE;
    }
    
    if (ierr != ST_SUCCESS)
    {
        STPrintError("Error computing absolute value of array");
        memset(dst, 0, sizeof(double)*n);
    }

    p_src = nullptr;
    p_dst = nullptr;

    return ierr;
}

STError_enum Envelope::ProcessDetail()
{
    STError_enum ierr = ST_SUCCESS;

    const int n = m_param.xr.size();
    double mean = 0.0;
    double* xrw = nullptr;

    CallocMemory(&xrw, n, &ierr);
    if (ierr == ST_SUCCESS)
    {
        ierr = GetRemovedDCSignal(m_param.xr.data(), xrw, n, &mean);
    }

    // hilbert transform
    ierr = GetHilbertTransform(m_param.xr.size(), xrw, m_ht);
    if (ierr == ST_SUCCESS)
    {

        // Take absolute value
        m_envelope.resize(GetLengthOfDestination());
        ierr = GetAbsoluteValuesForHilbertTransform(n, reinterpret_cast<Ipp64fc*>(m_ht.data()),
                                                       m_envelope.data());

        if(ierr == ST_SUCCESS)
        {
            // Add the mean back to the envelope
            ResizeDestinationStruct<double>(m_result);
            PRAGMA_OMP_SIMD
            for (int i = 0; i<n; i++)
            {   
                static_cast<EnvelopeDestinationStruct<double>*>(m_result.dst)->upper[i] = mean + m_envelope[i];
                static_cast<EnvelopeDestinationStruct<double>*>(m_result.dst)->lower[i] = mean - m_envelope[i];
                //static_cast<double*>(m_result.dst)[i] = static_cast<double*>(m_result.dst)[i] + mean;
            }
        }
        
    }

    // Free space
    FreeMemory(&xrw);


    return ierr;
}

STError_enum Envelope::SetParameters(const int n, double* x)
{
    STError_enum ierr = ST_SUCCESS;

    STSetArrayTooSmallErrorAndReturnNULL(n, 1, &ierr);
    if (ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(x, &ierr);
    }

    if (ierr == ST_SUCCESS)
    {
        //m_param.n = n;
        m_param.xr.resize(n);
        memcpy(m_param.xr.data(), x, sizeof(double)*n);

        m_result.length = n;
        InitSetParameters();
    }

    return ierr;
}
