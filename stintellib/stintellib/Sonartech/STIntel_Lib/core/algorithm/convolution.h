#pragma once
#include <ipps.h>
#include <mkl_cblas.h>
#include "../STIntelLib.h"
#include <vector>



class Convolution : public STIntelLib
{
    enum CONV_TYPE;


private:
    template<typename T>
    struct ConvParamStruct
    {
        std::vector<double> u; // 컨벌루션 입력신호 u
        std::vector<double> v; // 컨벌루션 입력신호 v
        enum CONV_TYPE shape;  // 구체적인 컨벌루션 방법 선택(full, same, valid)
    };

    template<typename T>
    struct ConvDestinationStruct
    {
        std::vector<double> w; // 컨벌루션 결과
    };

public:
    enum CONV_TYPE
    {
        CONV_FULL = 0,  // 전체 컨벌루션
        CONV_VALID = 1, // 크기가 u와 동일한 컨벌루션의 중앙부
        CONV_SAME = 2   // 모서리를 0으로 채우지 않고 계산한 컨벌루션의 부분
    };

private:
    ConvParamStruct<void> m_params;

    enum STError_enum ConvolveIppIntel(const int n1, double* ap1,
                                       const int n2, double* ap2,
                                       double* ret);

    enum STError_enum ConvolveComputeTrimIndices(const CONV_TYPE mode, const int n1, const int n2, int* n_left, int* n_right);
    int SetLengthOfDestination(const int n1, const int n2, const CONV_TYPE mode);

    template <typename T>
    void ResizeDestinationStruct(DestinationStruct& result) const
    {
        static_cast<ConvDestinationStruct<T>*>(result.dst)->w.resize(GetLengthOfDestination());
    }

    STError_enum ProcessDetail() override;
    STError_enum SetParameters(const int n1, double* ap1, const int n2, double* ap2, const enum CONV_TYPE mode);


public:
    template<typename T>
    ConvParamStruct<T> GetParameterStruct()
    {
        return ConvParamStruct<T>();
    }

    template<typename T>
    ConvDestinationStruct<T> GetDestinationStruct()
    {
        return ConvDestinationStruct<T>();
    }

    STError_enum SetParameters(void* param, INPUT_DATA_TYPE input_type)
    {
        auto p = static_cast<ConvParamStruct<void>*>(param);
        const STError_enum ierr = SetParameters(p->u.size(), p->u.data(), p->v.size(), p->v.data(), p->shape);

        return ierr;
    }

    virtual enum STError_enum SetDestination(void* dst) override
    {
        enum STError_enum ierr = ST_SUCCESS;

        m_result.dst = dst;
        InitSetDestination();

        /*if (m_state.param_state == true)
        {
            m_result.dst = dst;
            InitSetDestination();
        }
        else
        {
            ierr = ST_INVALID_INPUT;
        }*/

        return ierr;
    }

    Convolution();

    
};

