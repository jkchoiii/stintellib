//#include "stdafx.h"
#include "Convolution.h"


Convolution::Convolution()
{
    //error_check = std::make_unique<ErrorCheck>();
}

enum STError_enum Convolution::ConvolveIppIntel(const int n1, double* ap1, const int n2, double* ap2, double* ret)
{
    const IppEnum funCfgNormNo = static_cast<IppEnum>(ippAlgDirect | ippsNormNone);
    STError_enum ierr = ST_SUCCESS;
    const int src1Len = n1;
    const int src2Len = n2;
    int bufSizeNo = 0;

    IppStatus status = ippsConvolveGetBufferSize(src1Len, src2Len, ipp64f, funCfgNormNo, &bufSizeNo);

    if (status != ippStsNoErr)
    {
        STPrintError("Error getting buffer size");
        ierr = ST_IPP_FAILURE;
    }

    if (ierr == ST_SUCCESS)
    {
        Ipp8u* pBuffer = ippsMalloc_8u(bufSizeNo);
        if (pBuffer != nullptr)
        {
            Ipp64f* pSrc1 = static_cast<Ipp64f*>(ap1);
            Ipp64f* pSrc2 = static_cast<Ipp64f*>(ap2);
            Ipp64f* pDst = static_cast<Ipp64f*>(ret);
            status = ippsConvolve_64f(pSrc1, src1Len, pSrc2, src2Len, pDst, funCfgNormNo, pBuffer);
            if (status != ippStsNoErr)
            {
                STPrintError("Error in Intel convolution");
                ierr = ST_IPP_FAILURE;
            }

            ippsFree(pBuffer);

            pDst = nullptr;
            pSrc1 = nullptr;
            pSrc2 = nullptr;
        }
    }
    
    return ierr;

}

enum STError_enum Convolution::ConvolveComputeTrimIndices(const CONV_TYPE mode, const int n1, const int n2, int* n_left, int* n_right)
{
    enum STError_enum ierr = ST_SUCCESS;
    int lc;
    *n_left = 0;
    *n_right = 0;

    if (n1 < 1)
    {
        STPrintError("Error n1 must be positive");
        ierr = ST_INVALID_INPUT;
    }

    if (ierr == ST_SUCCESS)
    {
        if (n2 < 1)
        {
            STPrintError("Error n2 must be positive");
            ierr = ST_INVALID_INPUT;
        }
    }
    

    if(ierr == ST_SUCCESS)
    {
        // full
        if (mode == CONV_FULL)
        {
            lc = n1 + n2 - 1; // Length of full convolution
            *n_left = 0;
            *n_right = lc;
        }
        // valid
        else if (mode == CONV_VALID)
        {
            lc = std::max(n1, n2) - std::min(n1, n2) + 1;
            *n_left = 0;
            *n_right = 0;
            if (n2 > n1)
            {
                *n_left = n1 / 2 + 1;
                *n_right = *n_left + lc;
            }
            else
            {
                *n_left = n2 / 2 + 1;
                *n_right = *n_left + lc;
            }
        }
        // same
        else if (mode == CONV_SAME)
        {
            lc = std::max(n1, n2);
            if (n1 < n2)
            {
                *n_left = n1 / 2;
                *n_right = *n_left + lc;
            }
            else
            {
                *n_left = n2 / 2;
                *n_right = *n_left + lc;
            }
        }
        else
        {
            STPrintError("Invalid convolution type");
            ierr = ST_INVALID_INPUT;
        }
    }

    return ierr;
    
}

enum STError_enum Convolution::ProcessDetail()
{
    enum STError_enum ierr = ST_SUCCESS;
    const int n1 = m_params.u.size();
    double* ap1 = m_params.u.data();
    const int n2 = m_params.v.size();
    double* ap2 = m_params.v.data();

    
    //double* ret = static_cast<double*>(m_result.dst);
    const int lc = m_result.length;

    const CONV_TYPE mode = m_params.shape;

    int nc, n_left, n_right;
    double *cwork = nullptr;
    double *awork = nullptr;

    ResizeDestinationStruct<double>(m_result);
    double* ret = static_cast<ConvDestinationStruct<double>*>(m_result.dst)->w.data();

    if (n1 == 1 && n2 == 1)
    {
        ret[0] = ap1[0] * ap2[0];
    }
    // classify mode, check sizes, and issue warnings
    if (mode == CONV_FULL)
    {
        STReturnArrayTooSmallError(lc, n1 + n2 - 1, &ierr);
    }
    else if (mode == CONV_VALID)
    {
        STReturnArrayTooSmallError(lc, std::max(n1, n2) - std::max(n1, n2) + 1, &ierr);
    }
    else if (mode == CONV_SAME)
    {
        STReturnArrayTooSmallError(lc, std::max(n1, n2), &ierr);
    }
    else
    {
        STReturnArrayTooSmallError(lc, n1 + n2 - 1, &ierr);
    }

    if (ierr == ST_SUCCESS)
    {
        // Possibly can save a copy later by simplying pointing to the output 
        nc = n1 + n2 - 1;
        if (mode == CONV_FULL)
        {
            cwork = ret;
        }
        else
        {
            CallocMemory(&cwork, nc, &ierr);
        }

        if (ierr == ST_SUCCESS)
        {
            ierr = ConvolveIppIntel(n1, ap1, n2, ap2, cwork);
        }
        

        if (ierr == ST_SUCCESS)
        {
            if (mode == CONV_FULL)
            {
                ierr = ST_SUCCESS;
            }
            else
            {
                ierr = ConvolveComputeTrimIndices(mode, n1, n2, &n_left, &n_right);
                if (ierr == ST_SUCCESS)
                {
                    int k = 0;

                    PRAGMA_OMP_SIMD
                    for (int i = n_left; i < n_right; i++)
                    {
                        k = i - n_left;
                        ret[k] = cwork[i];
                    }

                    FreeMemory(&cwork);
                }
            }
        }
    }

    return ierr;
}

STError_enum Convolution::SetParameters(
    const int n1, double* ap1, 
    const int n2, double* ap2, 
    const CONV_TYPE mode)
{
    STError_enum ierr = ST_SUCCESS;
    STReturnArrayTooSmallError(n1, 1, &ierr);
    if (ierr == ST_SUCCESS)
    {
        STReturnArrayTooSmallError(n2, 1, &ierr);
    }

    // 
    if (ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(ap1, &ierr);
        if (ierr == ST_SUCCESS)
        {
            STReturnNullPointerError(ap2, &ierr);
        }
    }

    if (ierr == ST_SUCCESS)
    {
        m_params.u.resize(n1);
        memcpy(m_params.u.data(), ap1, sizeof(double)*n1);
        m_params.v.resize(n2);
        memcpy(m_params.v.data(), ap2, sizeof(double)*n2);

        m_params.shape = mode;
        m_result.length = SetLengthOfDestination(n1, n2, mode);

        InitSetParameters();
    }

    return ierr;
    
}

int Convolution::SetLengthOfDestination(const int n1, const int n2, const CONV_TYPE mode)
{
    int length = -1;

    if (n1 == 1 && n2 == 1)
    {
        length = 1;
    }
    else
    {
        if (mode == CONV_FULL)
        {
            length = n1 + n2 - 1;
        }
        else if (mode == CONV_VALID)
        {
            length = std::max(n1, n2) - std::min(n1, n2) + 1;
        }
        else if (mode == CONV_SAME)
        {
            length = std::max(n1, n2);
        }
    }

    

    return length;
}
