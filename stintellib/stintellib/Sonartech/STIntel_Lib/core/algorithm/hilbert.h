#pragma once
#include <ipps.h>
#include <mkl_cblas.h>
#include <vector>
#include "../STIntelLib.h"

#include "fft.h"


class STIntelLib;
class Hilbert : public STIntelLib
{
    
private:


    struct HilbertCalculationStruct
    {
        complex* x;
        complex* h;
        HilbertCalculationStruct()
        {
            x = nullptr;
            h = nullptr;
        }
    };
    template <typename T, typename T2>
    struct FFTStruct
    {
        std::unique_ptr<FFT> fft_obj;
        FFT::FFTParamStruct<T> param;
        FFT::FFTDestinationStruct<T2> dst;
        FFTStruct()
        {
            fft_obj = std::make_unique<FFT>();
            param = fft_obj->GetParameterStruct<T>();
            dst = fft_obj->GetDestinationStruct<T2>();
            
            //std::shared_ptr<STInterface> creator = std::make_shared<StandardCreator<FFT>>();
            //fft_obj = creator->Create();
        }
    };

    
    HilbertCalculationStruct m_calc;

    FFTStruct<double, complex> fft_forward;
    FFTStruct<complex, complex> fft_backward;


private:
    STError_enum Transform2FrequencyDomain(const int n, double* xr, complex** hilbert);
    STError_enum InverseFourierTransform(const int n, complex** x, complex* hilbert);
    STError_enum ProcessDetail() override;

    
    template <typename T>
    void ResizeDestinationStruct(DestinationStruct& result) const
    {
        static_cast<HilbertDestinationStruct<T>*>(result.dst)->H.resize(GetLengthOfDestination());
    }

    STError_enum SetParameters(const int n, double* x);


public:
    Hilbert();
    virtual ~Hilbert();

    template<typename T>
    struct HilbertParameterStruct
    {
        std::vector<double> xr; // 입력 실수 신호 xr(벡터)
    };

    template<typename T>
    struct HilbertDestinationStruct
    {
        std::vector<complex> H; // 변환 결과
    };

    template<typename T>
    HilbertParameterStruct<T> GetParameterStruct()
    {
        return HilbertParameterStruct<T>();
    }

    template<typename T>
    HilbertDestinationStruct<T> GetDestinationStruct()
    {
        return HilbertDestinationStruct<T>();
    }

    STError_enum SetParameters(void* param, INPUT_DATA_TYPE input_type)
    {
        auto p = static_cast<HilbertParameterStruct<void>*>(param);
        const STError_enum ierr = SetParameters(p->xr.size(), p->xr.data());

        return ierr;
    }

    virtual enum STError_enum SetDestination(void* dst) override
    {
        enum STError_enum ierr = ST_SUCCESS;

        m_result.dst = dst;
        InitSetDestination();

        /*if (m_state.param_state == true)
        {
        m_result.dst = dst;
        InitSetDestination();
        }
        else
        {
        ierr = ST_INVALID_INPUT;
        }*/
    

        return ierr;
    }


private:
    HilbertParameterStruct<double> m_param;

};

