#pragma once
#include "../STIntelLib.h"

#include <string>
#include <fftw/fftw3.h>
#include <fftw/fftw3_mkl.h>
#include <mkl_cblas.h>
#include <vector>

class STIntelLib;
class FFT : public STIntelLib
{
    
private:
    enum FFT_DIRECTION;
    enum FFT_FREQ_TYPE;
    enum FFT_NORMALIZE_TYPE;

    enum FFTW_INDEX
    {
        REAL_VAL,
        IMAG_VAL
    };


    struct InputConditionStruct
    {
        int lhs_if;
        int rhs_if;
        int loop_equal_size;
        int loop_trunc;
        int loop_padding;
        InputConditionStruct(int lhs, int rhs, int equal, int trunc, int pad)
        {
            lhs_if = lhs;
            rhs_if = rhs;
            loop_equal_size = equal;
            loop_trunc = trunc;
            loop_padding = pad;
        }

    };

    
    INPUT_DATA_TYPE m_input_type;
    FFT_DIRECTION m_algo_type;



public:
    enum FFT_ALGORITHM_TYPE
    {
        COMPLEX_FFT, // 입력 신호가 복소수인 경우 COMPLEX_FFT를 사용해야 함
        REAL_FFT,    // 입력 신호가 실수인 경우 REAL_FFT를 사용해야 함. DFT가 실수 입력에 대해 계산되면 출력은 에르미트 대칭임. 즉, 음의 주파수는 양의 주파수의 conjugate이므로 음의 주파수 항이 중복되는 것과 마찬가지임. 따라서 음의 주파수 항을 계산하지 않으므로 이 옵션으로 계산된 결과의 길이는 (n/2+1)임
        UNKNOWN      // 구조체 생성시 초기값. 알고리즘 수행 안 됨.
    };

    template <typename T>
    struct FFTParamStruct
    {
        int nx; // 입력신호 x의 길이
        T* x;  // 입력신호 x의 포인터
        int len_transform; // 지정된 수로 n-point fft/ifft를 수행함
        FFT_DIRECTION algo_type; // fft/ifft 선택
        FFT_NORMALIZE_TYPE fft_norm; // 신호 길이로 정규화 여부 선택
        FFT_ALGORITHM_TYPE fft_type; // real-fft인지 complex-fft인지 선택

        FFTParamStruct()
        {
            x = nullptr;
            nx = 0;
            len_transform = 0;
            algo_type = FFT_UNKNOWN;
            fft_norm = NO_NEED_NORMALIZE;
            fft_type = UNKNOWN;
        }
    };


    template <typename T>
    struct FFTDestinationStruct
    {
        std::vector<T> Y{}; // fft/ifft 변환 결과
    };

    enum FFT_DIRECTION
    {
        FFT_FORWARD,
        IFFT_BACKWARD,
        FFT_UNKNOWN
    };

    enum FFT_NORMALIZE_TYPE
    {
        NEED_NORMALIZE, // tranform 결과를 신호 길이로 나누는 옵션
        NO_NEED_NORMALIZE // 정규화 안함(matlab 결과와 같음)
    };

private:
    FFTParamStruct<void> m_params;

private:
    STError_enum ProcessFFTFreqs(const int i, const double dt, double* freqs);
    

    STError_enum CheckVarSize(const int nx, const void* x, const int ny, const void* y);

    STError_enum FillTransformShape(InputConditionStruct ctrl, double* in, double* x);
    STError_enum FillTransformShape(InputConditionStruct ctrl, fftw_complex* in, complex* x);

    void ProcessFFT(fftw_plan* fftw_plan, void** in);
    STError_enum ProcessTransform(fftw_plan* p, void** in, const int nx, void* x, const int ny, const int n);
    template <typename T>
    void ResizeDestinationStruct(DestinationStruct& result);

    STError_enum ProcessDetail() override;
    
    STError_enum SetParameters(const int nx, void* x, const int n_point_transform, INPUT_DATA_TYPE input_data_type, FFT_DIRECTION algorithm_type, FFT_NORMALIZE_TYPE norm_type);

public:

    template<typename T>
    FFTParamStruct<T> GetParameterStruct()
    {
        return FFTParamStruct<T>();
    }

    template<typename T>
    FFTDestinationStruct<T> GetDestinationStruct()
    {
        return FFTDestinationStruct<T>();
    }


    FFT()
    {
        fftw_cleanup();
    }
    virtual ~FFT()
    {
        fftw_cleanup();
    }

    
    int GetFFTNextPow2(const int n, STError_enum *ierr);
    void GetFFTFreq(const int n, const double dt, std::vector<double>& dst, STError_enum *ierr);

    STError_enum SetParameters(void* param, INPUT_DATA_TYPE input_type);
    //STError_enum SetDestination(void* param);

    virtual enum STError_enum SetDestination(void* dst) override
    {
        enum STError_enum ierr = ST_SUCCESS;

        m_result.dst = dst;
        InitSetDestination();

        return ierr;
    }

};

template <typename T>
void FFT::ResizeDestinationStruct(DestinationStruct& result)
{
    auto ptr = (FFTDestinationStruct<T>*)(result.dst);
    ptr->Y.resize(GetLengthOfDestination());
}


