#include "fft.h"

STError_enum FFT::CheckVarSize(const int nx, const void* x, const int ny, const void* y)
{
    STError_enum ierr = ST_SUCCESS;

    // Size checking
    STReturnArrayTooSmallError(nx, 1, &ierr);
    if (ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(x, &ierr);
    }
    if (ierr == ST_SUCCESS)
    {
        if (m_input_type == REAL)
        {
            STReturnArrayTooSmallError(m_params.len_transform, 1, &ierr);
            STReturnArrayTooSmallError(ny, 1, &ierr);
            if (ierr == ST_SUCCESS)
            {
                STReturnNullPointerError(y, &ierr);
            }
        }
    }

    return ierr;
}

STError_enum FFT::FillTransformShape(InputConditionStruct ctrl, double* in, double* x)
{
    STError_enum ierr = ST_SUCCESS;
    // Equal size transforms
    if (ctrl.lhs_if == ctrl.rhs_if)
    {
        PRAGMA_OMP_SIMD
            for (int i = 0; i < ctrl.loop_equal_size; i++)
            {
                in[i] = x[i];
            }
    }
    // Truncate x to length of output array y
    else if (ctrl.lhs_if >  ctrl.rhs_if)
    {
        PRAGMA_OMP_SIMD
            for (int i = 0; i < ctrl.loop_trunc; i++)
            {
                in[i] = x[i];
            }
    }
    // Pad x to length of output array y
    else if (ctrl.lhs_if <  ctrl.rhs_if)
    {
        PRAGMA_OMP_SIMD
            for (int i = 0; i < ctrl.lhs_if; i++)
            {
                in[i] = x[i];
            }

        PRAGMA_OMP_SIMD
            for (int i = ctrl.lhs_if; i < ctrl.loop_padding; i++)
            {
                in[i] = 0.0;
            }
    }
    else
    {
        STPrintError("Could not classify job (nx,ny)");
        ierr = ST_ALGORITHM_FAILURE;
    }
    return ierr;
}

STError_enum FFT::FillTransformShape(InputConditionStruct ctrl, fftw_complex* in, complex* x)
{

    STError_enum ierr = ST_SUCCESS;
    // Equal size transforms
    if (ctrl.lhs_if == ctrl.rhs_if)
    {
        PRAGMA_OMP_SIMD
            for (int i = 0; i < ctrl.loop_equal_size; i++)
            {
                in[i][REAL_VAL] = x[i].r;
                in[i][IMAG_VAL] = x[i].i;
            }
    }
    // Truncate x to length of output array y
    else if (ctrl.lhs_if >  ctrl.rhs_if)
    {
        PRAGMA_OMP_SIMD
            for (int i = 0; i < ctrl.loop_trunc; i++)
            {
                in[i][REAL_VAL] = x[i].r;
                in[i][IMAG_VAL] = x[i].i;
            }
    }
    // Pad x to length of output array y
    else if (ctrl.lhs_if <  ctrl.rhs_if)
    {
        PRAGMA_OMP_SIMD
            for (int i = 0; i < ctrl.lhs_if; i++)
            {
                in[i][REAL_VAL] = x[i].r;
                in[i][IMAG_VAL] = x[i].i;
            }

        PRAGMA_OMP_SIMD
            for (int i = ctrl.lhs_if; i < ctrl.loop_padding; i++)
            {
                in[i][REAL_VAL] = 0.0;
                in[i][IMAG_VAL] = 0.0;
            }
    }
    else
    {
        STPrintError("Could not classify job (nx,ny)");
        ierr = ST_ALGORITHM_FAILURE;
    }
    return ierr;
}

void FFT::ProcessFFT(fftw_plan* p, void** in)
{
    fftw_execute(*p);
    // free plan and data
    fftw_destroy_plan(*p);
    fftw_cleanup();
    fftw_free(static_cast<fftw_complex*>(*in));
}

STError_enum FFT::ProcessTransform(fftw_plan* p, void** in, const int nx, void* x,
                                   const int ny, const int n)
{
    STError_enum ierr = ST_SUCCESS;

    if(m_input_type == COMPLEX)
    {
        if(m_algo_type == FFT::FFT_FORWARD)
        {
            InputConditionStruct ctrl(nx, ny, n, ny, ny);
            ierr = FillTransformShape(ctrl, static_cast<fftw_complex*>(*in), static_cast<complex*>(x));
            ProcessFFT(p, in);
        }
        else
        {
            InputConditionStruct ctrl(nx, ny, n, ny, ny);
            ierr = FillTransformShape(ctrl, static_cast<fftw_complex*>(*in), static_cast<complex*>(x));
            ProcessFFT(p, in);
        }
    }
    else
    {
        if (m_algo_type == FFT::FFT_FORWARD)
        {
            InputConditionStruct ctrl(nx, n, n, n, n);
            ierr = FillTransformShape(ctrl, static_cast<double*>(*in), static_cast<double*>(x));
            ProcessFFT(p, in);            
        }
        else
        {
            InputConditionStruct ctrl(nx, n, nx, n, n);
            ierr = FillTransformShape(ctrl, static_cast<fftw_complex*>(*in), static_cast<complex*>(x));
            ProcessFFT(p, in);

        }
    }

    return ierr;
}

STError_enum FFT::ProcessDetail()
{
    STError_enum ierr = ST_SUCCESS;
    const int nx = m_params.nx;
    const int ny = m_result.length;

    if (CheckVarSize(nx, m_params.x, ny, m_result.dst) == ST_SUCCESS)
    {
        fftw_plan p;

        if (m_input_type == FFT::COMPLEX)
        {
            const int n = ny;
            const complex* x = static_cast<complex*>(m_params.x);

            ResizeDestinationStruct<complex>(m_result);

            complex* dst_addr = static_cast<FFTDestinationStruct<complex>*>(m_result.dst)->Y.data();
            fftw_complex* out = reinterpret_cast<fftw_complex*>(dst_addr);

            fftw_complex* in = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex) * static_cast<size_t>(n)));

            if(m_algo_type == FFT::FFT_FORWARD)
            {
                p = fftw_plan_dft_1d(n, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
                ierr = ProcessTransform(&p, (void**)&in, nx, (void*)x, ny, m_params.len_transform);

                if (m_params.fft_norm == NEED_NORMALIZE)
                {
                    const double xnorm = 1.0 / static_cast<double>(ny);
                    cblas_zdscal(ny, xnorm, static_cast<fftw_complex*>(out), 1);
                }

            }
            else if(m_algo_type == FFT::IFFT_BACKWARD)
            {
                p = fftw_plan_dft_1d(n, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
                ierr = ProcessTransform(&p, (void**)&in, nx, (void*)x, ny, m_params.len_transform);

                // ifft scale
                if (ierr == ST_SUCCESS)
                {
                    const double xnorm = 1.0 / static_cast<double>(ny);
                    cblas_zdscal(ny, xnorm, static_cast<fftw_complex*>(out), 1);

                    if (m_params.fft_norm == NEED_NORMALIZE)
                    {
                        const double scale = static_cast<double>(ny);
                        cblas_zdscal(ny, scale, static_cast<fftw_complex*>(out), 1);
                    }
                }
            }
        }
        else if (m_input_type == FFT::REAL)
        {
            if (m_algo_type == FFT::FFT_FORWARD)
            {
                ResizeDestinationStruct<complex>(m_result);
                complex* dst_addr = static_cast<FFTDestinationStruct<complex>*>(m_result.dst)->Y.data();
                
                const int n = m_params.len_transform;
                const double* x = static_cast<double*>(m_params.x);
                double* in = static_cast<double*>(fftw_malloc(sizeof(double) * static_cast<size_t>(n)));
                fftw_complex* out = reinterpret_cast<fftw_complex*>(dst_addr);

                p = fftw_plan_dft_r2c_1d(n, in, out, FFTW_ESTIMATE);
                ierr = ProcessTransform(&p, (void**)&in, nx, (void*)x, ny, m_params.len_transform);

                if (m_params.fft_norm == NEED_NORMALIZE)
                {
                    const double xnorm = 1.0 / static_cast<double>(ny);
                    cblas_zdscal(ny, xnorm, static_cast<fftw_complex*>(out), 1);
                }

            }
            else if (m_algo_type == FFT::IFFT_BACKWARD)
            {
                ResizeDestinationStruct<double>(m_result);
                double* dst_addr = static_cast<FFTDestinationStruct<double>*>(m_result.dst)->Y.data();


                const int n = m_result.length;
                const int ntf = m_params.len_transform / 2 + 1;
                const fftw_complex* x = static_cast<fftw_complex*>(m_params.x);
                fftw_complex* in = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex) * static_cast<size_t>(ntf)));
                double* out = static_cast<double*>(dst_addr);

                p = fftw_plan_dft_c2r_1d(n, in, out, FFTW_ESTIMATE);
                ierr = ProcessTransform(&p, (void**)&in, nx, (void*)x, ny, ntf);

                // ifft scale
                if (ierr == ST_SUCCESS)
                {
                    const double xnorm = 1.0 / static_cast<double>(ny);
                    cblas_dscal(ny, xnorm, static_cast<double*>(out), 1);

                    if (m_params.fft_norm == NEED_NORMALIZE)
                    {
                        const double scale = static_cast<double>(ny);
                        cblas_dscal(ny, scale, static_cast<double*>(out), 1);
                    }
                }
            }
        }
    }
    
    return ierr;
}

int FFT::GetFFTNextPow2(const int n, STError_enum* ierr)
{
    int ret_val = 0;
    bool go = true;
    *ierr = ST_SUCCESS;
    // Base cases
    if (n < 0)
    {
        STPrintError("Invalid n. n must be positive");
        *ierr = ST_INVALID_INPUT;
        ret_val = 0;
        go = false;
    }
    if (go == true)
    {
        // Simple base cases
        if (n == 0)
        {
            ret_val = 1;
            go = false;
        }
        if (go == true)
        {
            if (n == 1)
            {
                ret_val = 1;
                go = false;
            }
            if (go == true)
            {
                // General computation
                const int itemp = static_cast<int>(ceil(log2(static_cast<double>(n)))); // get log2 of and round up
                const long n2t = static_cast<long>(pow(2, itemp)); // compute next power of 2 greater than n
                const int n2 = static_cast<int>(n2t);
                // Catch any overflow problems associated with in32_t and int64_t 
                if (n2t != n2)
                {
                    STPrintError("fft:nextpow2, overflow error\n");
                    *ierr = ST_ALGORITHM_FAILURE;
                    ret_val = 0;
                }
                else
                {
                    ret_val = n2;
                }
            }
            
        }
    }
    
    return ret_val;
}

void FFT::GetFFTFreq(const int n, const double dt, std::vector<double>& freqs, STError_enum* ierr)
{
    //ResultStruct<double> freqs;
    *ierr = ST_SUCCESS;

    STSetArrayTooSmallErrorAndReturnNULL(n, 1, ierr);
    if (dt <= 0.0)
    {
        STPrintError("Invalid sampling spacing");
        *ierr = ST_INVALID_INPUT;
    }

    if (*ierr == ST_SUCCESS)
    {
        //AllocateMemory(&freqs.dst, n, ierr);
        //freqs.dst = std::make_unique<double[]>(n);
        freqs.resize(n);
        *ierr = ProcessFFTFreqs(n, dt, freqs.data());
        //freqs.length = n;

        if (*ierr != ST_SUCCESS)
        {
            STPrintError("Error computing frequencies");
            //FreeMemory(&freqs.dst);
        }
    }
    
    
}

STError_enum FFT::ProcessFFTFreqs(const int n, const double dt, double* freqs)
{
    STError_enum ierr;
    int nlim;
    STReturnArrayTooSmallError(n, 1, &ierr);
    STReturnNullPointerError(freqs, &ierr);
    if (dt <= 0.0)
    {
        STPrintError("Invalid sample spacing");
        memset(freqs, 0, sizeof(double)*n);
        return ST_INVALID_INPUT;
    }
    // Easy case
    if (n == 1)
    {
        freqs[0] = 0.0;
        return ST_SUCCESS;
    }
    // Set denominator
    const double xden = 1.0 / (dt * static_cast<double>(n));

    if (n % 2 == 0)
    {
        nlim = n / 2 - 1;
        int j = 0;
        PRAGMA_OMP_SIMD
        for (int i = 0; i <= nlim; i++)
        {
            freqs[j] = static_cast<double>(i)*xden;
            j = j + 1;
        }

        nlim = -n / 2;
        PRAGMA_OMP_SIMD
        for (int i = nlim; i <= -1; i++)
        {
            freqs[j] = static_cast<double>(i)*xden;
            j = j + 1;
        }
    }
    else
    {
        nlim = (n - 1) / 2;
        int j = 0;
        PRAGMA_OMP_SIMD
        for (int i = 0; i <= nlim; i++)
        {
            freqs[j] = static_cast<double>(i)*xden;
            j = j + 1;
        }

        nlim = -(n - 1) / 2;
        PRAGMA_OMP_SIMD
        for (int i = nlim; i <= -1; i++)
        {
            freqs[j] = static_cast<double>(i)*xden;
            j = j + 1;
        }
    }
    return ST_SUCCESS;
}

STError_enum FFT::SetParameters(void* param, INPUT_DATA_TYPE input_type)
{
    auto p = static_cast<FFTParamStruct<void>*>(param);
    STError_enum ierr = ST_SUCCESS;
    if (p->fft_type == FFT_ALGORITHM_TYPE::COMPLEX_FFT)
    {
        input_type = COMPLEX;
        ierr = SetParameters(p->nx, p->x, p->len_transform, input_type, p->algo_type, p->fft_norm);
    }
    else if (p->fft_type == FFT_ALGORITHM_TYPE::REAL_FFT)
    {
        input_type = REAL;
        ierr = SetParameters(p->nx, p->x, p->len_transform, input_type, p->algo_type, p->fft_norm);
    }
    
    return ierr;
}

STError_enum FFT::SetParameters(const int nx, void* x, const int n_point_transform, INPUT_DATA_TYPE input_data_type, FFT_DIRECTION algorithm_type, FFT_NORMALIZE_TYPE norm_type)
{
    enum STError_enum ierr = ST_SUCCESS;


    //if (ierr == ST_SUCCESS)
    //{
    //    if (fftfreq_type != FFT_FREQ_TYPE::NEED_FFTFREQ && fftfreq_type != FFT_FREQ_TYPE::NO_NEED_FFTFREQ)
    //    {
    //        ierr = ST_INVALID_INPUT;
    //    }
    //}
    //

    if (ierr == ST_SUCCESS)
    {
        if (norm_type != FFT_NORMALIZE_TYPE::NEED_NORMALIZE && norm_type != FFT_NORMALIZE_TYPE::NO_NEED_NORMALIZE)
        {
            ierr = ST_INVALID_INPUT;
        }
    }

    if (ierr == ST_SUCCESS)
    {
        if (algorithm_type != FFT_FORWARD && algorithm_type != IFFT_BACKWARD)
        {
            ierr = ST_INVALID_INPUT;
        }
    }
    

    if (ierr == ST_SUCCESS)
    {
        STSetArrayTooSmallErrorAndReturnNULL(n_point_transform, 1, &ierr);
    }
    
    if (ierr == ST_SUCCESS)
    {
        STSetArrayTooSmallErrorAndReturnNULL(nx, 1, &ierr);
        if (ierr == ST_SUCCESS)
        {
            m_params.nx = nx;
            //m_params.x = static_cast<T*>(x);
            m_params.x = x;
            m_params.len_transform = n_point_transform;
            //m_params.fft_freq = fftfreq_type;
            m_params.fft_norm = norm_type;

            InitSetParameters();
        }
    }

    if (GetStateParameter() == true)
    {
        // Set space and fourier transform

        m_algo_type = algorithm_type;
        m_input_type = input_data_type;

        if (input_data_type == REAL)
        {
            if (algorithm_type == FFT_FORWARD)
            {
                m_result.length = n_point_transform / 2 + 1;
            }
            else if (algorithm_type == IFFT_BACKWARD)
            {
                m_result.length = n_point_transform;
            }

        }
        else if (input_data_type == COMPLEX)
        {
            m_result.length = n_point_transform;
        }
        else
        {
            ierr = ST_INVALID_INPUT;
            // impossible
        }

    }

    return ierr;
}

