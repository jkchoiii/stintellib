#pragma once
#include <ipps.h>
#include <mkl_cblas.h>
#include <vector>

#include "../STIntelLib.h"
#include "hilbert.h"

class Envelope : public STIntelLib
{
    
    struct HilbertTransformStruct
    {
        std::unique_ptr<Hilbert> ht;
        Hilbert::HilbertDestinationStruct<double> dst;
        Hilbert::HilbertParameterStruct<double> param;
        HilbertTransformStruct()
        {
            ht = std::make_unique<Hilbert>();
            dst = ht->GetDestinationStruct<double>();
            param = ht->GetParameterStruct<double>();

        }
    };
    HilbertTransformStruct m_hilbert;
    //std::shared_ptr<Hilbert> m_ht_obj;
    
private:
    template <typename T>
    struct EnvelopeParameterStruct
    {
        std::vector<double> xr; // 입력 실수 신호 xr(벡터)
    };

    template <typename T>
    struct EnvelopeDestinationStruct
    {
        std::vector<double> upper; // 상부 포락선
        std::vector<double> lower; // 하부 포락선
    };

    std::vector<double> m_envelope;
    std::vector<complex> m_ht;

    EnvelopeParameterStruct<double> m_param;
    std::unique_ptr<FFT> m_fft;

public:
    enum ALGORITHM_TYPE
    {
        HILBERT,
        ENVELOPE
    };

    ALGORITHM_TYPE m_algo_type;

private:
    STError_enum GetHilbertTransform(int n, double* xr, std::vector<ComplexDoubleStruct>& ht);
    STError_enum GetAbsoluteValuesForHilbertTransform(int n, Ipp64fc* ht, double* dst);
    STError_enum GetRemovedDCSignal(double* xr, double* xrw, const int n, double* mean);

    STError_enum ProcessDetail() override;
    
    STError_enum SetParameters(const int n, double* x);

    template <typename T>
    void ResizeDestinationStruct(DestinationStruct& result) const
    {
        static_cast<EnvelopeDestinationStruct<T>*>(result.dst)->upper.resize(GetLengthOfDestination());
        static_cast<EnvelopeDestinationStruct<T>*>(result.dst)->lower.resize(GetLengthOfDestination());
    }

public:
    Envelope();
    virtual ~Envelope();


    template<typename T>
    EnvelopeParameterStruct<T> GetParameterStruct()
    {
        return EnvelopeParameterStruct<T>();
    }

    template<typename T>
    EnvelopeDestinationStruct<T> GetDestinationStruct()
    {
        return EnvelopeDestinationStruct<T>();
    }

    STError_enum SetParameters(void* param, INPUT_DATA_TYPE input_type)
    {
        auto p = static_cast<EnvelopeParameterStruct<void>*>(param);
        const STError_enum ierr = SetParameters(p->xr.size(), p->xr.data());

        return ierr;
    }

    virtual enum STError_enum SetDestination(void* dst) override
    {
        enum STError_enum ierr = ST_SUCCESS;

        m_result.dst = dst;
        InitSetDestination();

        /*if (m_state.param_state == true)
        {
        m_result.dst = dst;
        InitSetDestination();
        }
        else
        {
        ierr = ST_INVALID_INPUT;
        }*/

        return ierr;
    }
    
    
};

