#pragma once
#include <vector>
#include "filter.h"

class FiltFilt : public Filter
{

private:
    //DestinationStruct m_result;
    //FilterStruct<double> m_param;
    PrivateFilterParameterStruct m_param;


private:
    STError_enum ProcessDetail() override;
    STError_enum SetParameters(SignalBA_Struct<double>& ba, const int nx, double* x, const bool have_zi, double* zi, const bool have_zf/*, double* zf*/);

public:
    FiltFilt()
    {
    }

    virtual ~FiltFilt()
    {
    }

    

    virtual enum STError_enum SetDestination(void* dst)
    {
        enum STError_enum ierr = ST_SUCCESS;

        m_result.dst = dst;
        InitSetDestination();


        return ierr;
    }

    STError_enum SetParameters(void* param, INPUT_DATA_TYPE input_type)
    {
        auto p = static_cast<FilterParameterStruct<void>*>(param);
        const STError_enum ierr = SetParameters(p->ba, p->nx, p->x, p->cond.have_zi, p->cond.zi.data(), p->cond.have_zf);

        return ierr;
    }

    template<typename T>
    FilterParameterStruct<T> GetParameterStruct()
    {
        return FilterParameterStruct<T>();
    }

    template<typename T>
    FilterDestinationStruct<T> GetDestinationStruct()
    {
        return FilterDestinationStruct<T>();
    }

};
