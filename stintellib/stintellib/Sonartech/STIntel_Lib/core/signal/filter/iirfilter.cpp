#include "iirfilter.h"

STError_enum IIRFilter::ProcessDetail()
{
    STError_enum ierr = ST_SUCCESS;

    IppsIIRState_64f *pIIRState = nullptr;
    IppStatus status;
    int bufferSize = 0;
    Ipp8u *pBuf = nullptr;
    
    const Ipp64f* pSrc = static_cast<const Ipp64f*>(m_param.x.data());
    FilterDestinationStruct<double>* dst_ptr = static_cast<FilterDestinationStruct<double>*>(m_result.dst);
    dst_ptr->y.resize(m_param.x.size());
    Ipp64f* pDst = static_cast<Ipp64f*>(dst_ptr->y.data());

    const int norder = std::max(m_param.ba.nb, m_param.ba.na) - 1;

    // Set the space for the taps    
    Ipp64f* pTaps = ippsMalloc_64f(2 * (norder + 1));
    if (pTaps != nullptr)
    {
        // Portions of taps may be unitialized
        if (m_param.ba.na != m_param.ba.nb)
        {
            status = ippsSet_64f(0.0, pTaps, 2 * (norder + 1));
            if (status != ippStsNoErr)
            {
                STPrintError("Error zeroing out pTaps");
                ierr = ST_IPP_FAILURE;
                //goto ERROR;
            }
        }
        if (ierr == ST_SUCCESS)
        {
            // Copy the taps
            //status = ippsCopy_64f(b, &pTaps[0], m_param.ba.nb);
            status = ippsCopy_64f(m_param.ba.b.data(), &pTaps[0], m_param.ba.nb);
            if (status != ippStsNoErr)
            {
                STPrintError("Error copying b");
                ierr = ST_IPP_FAILURE;
                //goto ERROR;
            }
        }

        if (ierr == ST_SUCCESS)
        {
            status = ippsCopy_64f(m_param.ba.a.data(), &pTaps[norder + 1], m_param.ba.na);
            if (status != ippStsNoErr)
            {
                STPrintError("Error copying a");
                ierr = ST_IPP_FAILURE;
                //goto ERROR;
            }
        }

        if (ierr == ST_SUCCESS)
        {
            // Get the state size
            status = ippsIIRGetStateSize_64f(norder, &bufferSize);
            pBuf = ippsMalloc_8u(bufferSize);
            if (pBuf != nullptr)
            {
                // Initialize
                if (m_param.cond.have_zi)
                {
                    status = ippsIIRInit_64f(&pIIRState, pTaps, norder, m_param.cond.zi.data(), pBuf);
                }
                else
                {
                    status = ippsIIRInit_64f(&pIIRState, pTaps, norder, nullptr, pBuf);
                }
                if (status != ippStsNoErr)
                {
                    STPrintError("Error initializing IIR filter");
                    ierr = ST_IPP_FAILURE;
                    //goto ERROR;
                }
            }
            
        }

        if (ierr == ST_SUCCESS)
        {
            // Filter
            status = ippsIIR_64f(pSrc, pDst, m_param.x.size(), pIIRState);
            if (status != ippStsNoErr)
            {
                STPrintError("Error computing IIR filter");
                ierr = ST_IPP_FAILURE;
                //goto ERROR;
            }
        }

        if (ierr == ST_SUCCESS)
        {
            // Get the final conditions 
            if (m_param.cond.have_zf)
            {
                status = ippsIIRGetDlyLine_64f(pIIRState, static_cast<FilterDestinationStruct<double>*>(m_result.dst)->zf.data());
                if (status != ippStsNoErr)
                {
                    STPrintError("Error getting delay line");
                    ierr = ST_IPP_FAILURE;
                    //goto ERROR;
                }
            }
        }
    }
    else
    {
        ierr = ST_ALLOC_FAILURE;
    }

    // Error handling and memory cleanup
    if (ierr != ST_SUCCESS)
    {
        STPrintWarning("Setting filtered signal to input signal");
        ippsCopy_64f(pSrc, pDst, m_param.x.size());
    }

    if (pBuf != nullptr)
    {
        ippsFree(pBuf);
        pBuf = nullptr;
    }

    if (pTaps != nullptr)
    {
        ippsFree(pTaps);
        pTaps = nullptr;
    }

    pDst = nullptr;
    pSrc = nullptr;

    return ierr;
}

STError_enum IIRFilter::SetParameters(
    SignalBA_Struct<double>& ba,
    const int nx, double* x,
    const bool have_zi, double* zi, 
    const bool have_zf/*, double* zf*/)
{
    STError_enum ierr = CheckFIRFilterInputs(ba, nx, x, have_zi, zi, have_zf/*, zf*/);

    if(ierr == ST_SUCCESS)
    {
        ierr = CheckIIRFilterInputs(ba);
    }
    
    if (ierr == ST_SUCCESS)
    {
        m_param.ba = ba;
        m_param.x.resize(nx);
        memcpy(m_param.x.data(), x, sizeof(double)*nx);

        const int cond_size = std::max(ba.na - 1, ba.nb - 1);
        m_param.cond.have_zi = have_zi;
        if (have_zi == true)
        {
            m_param.cond.zi.resize(cond_size);
            memcpy(m_param.cond.zi.data(), zi, sizeof(double)*cond_size);
        }
        
        m_param.cond.have_zf = have_zf;
        if (have_zf == true)
        {
            static_cast<FilterDestinationStruct<double>*>(m_result.dst)->zf.resize(cond_size);
        }

        InitSetParameters();
    }

    return ierr;

}

