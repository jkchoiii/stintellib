#pragma once
#include <vector>
#include "filter.h"

class IIRFilter : public Filter
{

private:
    //DestinationStruct m_result;
    //FilterParameterStruct<double> m_param;
    PrivateFilterParameterStruct m_param;

private:
    STError_enum ProcessDetail() override;
    STError_enum SetParameters(SignalBA_Struct<double>& ba, const int nx, double* x, const bool have_zi, double* zi, const bool have_zf/*, double* zf*/);
public:
    IIRFilter()
    {
    }

    virtual ~IIRFilter()
    {
    }

    virtual enum STError_enum SetDestination(void* dst)
    {
        enum STError_enum ierr = ST_SUCCESS;

        m_result.dst = dst;
        InitSetDestination();

        /*if (m_state.param_state == true)
        {
        m_result.dst = dst;
        InitSetDestination();
        }
        else
        {
        ierr = ST_INVALID_INPUT;
        }*/

        return ierr;
    }

    

    STError_enum SetParameters(void* param, INPUT_DATA_TYPE input_type)
    {
        STError_enum ierr = ST_SUCCESS;
        auto p = static_cast<FilterParameterStruct<void>*>(param);

        const int zi_size = std::max(p->ba.na - 1, p->ba.nb - 1);
        if (p->cond.have_zi == true)
        {
            if (zi_size != p->cond.zi.size())
            {
                ierr = ST_INVALID_INPUT;
            }
        }

        if (ierr == ST_SUCCESS)
        {
            ierr = SetParameters(p->ba, p->nx, p->x, p->cond.have_zi, p->cond.zi.data(), p->cond.have_zf);
        }
        
        return ierr;
    }

    template<typename T>
    FilterParameterStruct<T> GetParameterStruct()
    {
        return FilterParameterStruct<T>();
    }

    template<typename T>
    FilterDestinationStruct<T> GetDestinationStruct()
    {
        return FilterDestinationStruct<T>();
    }

    
    
};
