#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include <complex>
#include <vector>

#include "../../STIntelLib.h"


class Filter : public STIntelLib
{
public:

    template<typename T>
    struct SignalBA_Struct
    {
        std::vector<double> b; // 다항 계수(유리 전달 함수)의 분자
        std::vector<double> a; // 다항 계수(유리 전달 함수)의 분모
        int na;    // 분자 계수의 개수
        int nb;    // 분모 계수의 개수

        SignalBA_Struct()
        {
            na = 0;
            nb = 0;
        }
    };
protected:
    struct FilterConditionStruct
    {
        bool have_zi;  // 필터 지연에 대한 초기 조건을 설정할 것인지 여부(true / false)
        std::vector<double> zi; // have_zi가 true인 경우 해당 필터 지연 조건을 채워 넣어야 함
        bool have_zf;  // 알고리즘 결과로 필터 최종 조건도 출력할 것인지 여부(true /false)

        FilterConditionStruct()
        {
            have_zi = false;
            have_zf = false;
        }
    };

    template<typename T>
    struct FilterParameterStruct
    {
        SignalBA_Struct<double> ba; // 분자 및 분모 계수인 b, a로 정의되는 유리 전달 함수(Butter, Bessel, Cheby의 결과)
        FilterConditionStruct cond; // 필터 지연에 대한 초기 조건 zi 및 최종 조건 zf에 대한 정의
        //std::vector<double> x;
        int nx; // 입력 데이터의 개수
        double* x; // 입력 데이터의 double 포인터
        FilterParameterStruct()
        {
            nx = -1;
            x = nullptr;
        }

    };

    struct PrivateFilterParameterStruct
    {
        SignalBA_Struct<double> ba;
        FilterConditionStruct cond;
        std::vector<double> x;
    };


    template<typename T>
    struct FilterDestinationStruct
    {
        std::vector<double> y; // 필터링 결과
        std::vector<double> zf; // 필터 최종 조건
    };


protected:
    
    STError_enum CheckFIRFilterInputs(SignalBA_Struct<double>& ba, int n, double* x, bool have_zi, double* zi, bool have_zf/*, double* zf*/);
    STError_enum CheckIIRFilterInputs(SignalBA_Struct<double>& ba);


private:
    virtual STError_enum ProcessDetail() = 0;


public:
    Filter()
    {

    }

    virtual ~Filter()
    {
    }

    //inline STError_enum Filter::SetDestination(void* dst)
    //{
    //    enum STError_enum ierr = ST_SUCCESS;

    //    if (m_state.param_state == true)
    //    {
    //        m_result.dst = dst;
    //        InitSetDestination();
    //    }
    //    else
    //    {
    //        ierr = ST_INVALID_INPUT;
    //    }

    //    return ierr;
    //}

    

};

inline STError_enum Filter::CheckFIRFilterInputs(
    SignalBA_Struct<double>& ba,
    int n, double* x,    
    const bool have_zi, double* zi, 
    const bool have_zf/*, double* zf*/)
{
    STError_enum ierr = ST_SUCCESS;

    STReturnArrayTooSmallError(n, 1);
    STReturnNullPointerError(x, &ierr);

    if (ierr == ST_SUCCESS)
    {
        if (ba.nb > 0)
        {
            STReturnNullPointerError(ba.b.data(), &ierr);
        }
    }

    if(ierr == ST_SUCCESS)
    {
        // Check final conditions    
        if (have_zi)
        {
            STReturnNullPointerError(zi, &ierr);
        }
    }


    return ierr;
}

inline STError_enum Filter::CheckIIRFilterInputs(SignalBA_Struct<double>& ba)
{
    STError_enum ierr = ST_SUCCESS;

    if (ba.na <= 0 && ba.nb <= 0)
    {
        ierr = ST_INVALID_INPUT;
    }

    if (ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(ba.a.data(), &ierr);
    }

    if (ierr == ST_SUCCESS)
    {
        // First coefficient of a must be non-zero
        if (ba.a[0] == 0.0)
        {
            STPrintError("Error first coefficient of a is zero");
            ierr = ST_INVALID_INPUT;
        }
    }

    return ierr;
}
