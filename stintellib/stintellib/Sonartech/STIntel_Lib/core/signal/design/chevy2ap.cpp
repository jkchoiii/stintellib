#include "chevy2ap.h"

STError_enum Cheby2::SetParameters(
    const IIRDesignBandType btype,
    /*const IIRDesignFilterType ftype,
    const IIRDesignProduceType ptype,*/
    const int n, const double rs, const double W0, const double W1)
{
    STError_enum ierr = ValidateFilterType(n, btype, /*ftype, ptype, */W0, W1);

    if(ierr == ST_SUCCESS)
    {
        FillFilterParameters(n, W0, W1, rs, btype/*,ftype, ptype*/);
        InitSetParameters();
    }

    return ierr;
}

STError_enum Cheby2::ValidateDesignParameters(SignalZPK_PrivateStruct& zpk_param, FilterParamStruct<double>& filter_param)
{
    complex* p = zpk_param.p;
    const int n = filter_param.n;
    const int npoles = zpk_param.npoles;

    STError_enum ierr = ST_SUCCESS;

    if (ST_SUCCESS == STReturnArrayTooSmallError(n, 1))
    {
        STReturnNullPointerError(p, &ierr);
    }

    if (ierr == ST_SUCCESS)
    {
        if (npoles != n)
        {
            if (npoles < n)
            {
                STPrintError(" Error insufficient space in p");
                ierr = ST_INVALID_INPUT;
            }
            else
            {
                STPrintWarning(" Warning not all poles will be initialized");
            }
        }
    }


    return ierr;
}

STError_enum Cheby2::FilterDesign(FilterParamStruct<double>& params, SignalZPK_PrivateStruct& zpk)
{
    STError_enum ierr = ST_SUCCESS;
    const int n = params.n;

    complex* p = zpk.p;
    complex* z = zpk.z;

    double* k = &zpk.k;
    const double rs = params.r;
    *k = 1.0;

    int ntarg;
    if (n % 2 == 1)
    {
        ntarg = n - 1;
    }
    else
    {
        ntarg = n;
    }

    if(zpk.nzeros != ntarg)
    {
        if(zpk.nzeros < ntarg)
        {
            STPrintError("Error insufficient space in z");
            ierr = ST_ARRAY_TOO_SMALL;
        }
        else
        {
            STPrintWarning("Warning not all zeros will be initialized");
        }
    }

    double rdb = pow(10.0, 0.1*rs);
    if (ierr == ST_SUCCESS)
    {        
        if (rdb <= 1.0)
        {
            STPrintError("Error ripple factor is complex");
            ierr = ST_INVALID_INPUT;
        }
    }
    
    if(ierr == ST_SUCCESS)
    {
        const std::complex<double> zone(1.0, 0.0);

        // Ripple factor
        const double twoni = 1.0 / (2.0*static_cast<double>(n));
        const double eps = 1.0 / sqrt(rdb - 1.0);
        const double xmu = asinh(1.0 / eps) / static_cast<double>(n);

        // Compute zeros
        std::complex<double> zden = zone;

        int j = 0;
        if (n % 2 == 1)
        {
            for (int i = 0; i<n / 2; i++)
            {
                const double xm = static_cast<double>(-n + 1 + 2 * i);
                std::complex<double> temp = std::complex<double>(0.0, 1.0 / (sin(xm*M_PI*twoni)));
                z[j].r = temp.real();
                z[j].i = temp.imag();
                zden = (-temp)*zden;
                
                j = j + 1;
            }
            for (int i = 0; i<n / 2; i++)
            {
                const double xm = static_cast<double>(2 + 2 * i);
                std::complex<double> temp = std::complex<double>(0.0, 1.0 / (sin(xm*M_PI*twoni)));
                z[j].r = temp.real();
                z[j].i = temp.imag();
                zden = (-temp)*zden;

                j = j + 1;
            }
        }
        // Even
        else
        {
            for (int i = 0; i<n; i++)
            {
                const double xm = static_cast<double>(-n + 1 + 2 * i);
                std::complex<double> temp = std::complex<double>(0.0, 1.0 / (sin(xm*M_PI*twoni)));
                z[j].r = temp.real();
                z[j].i = temp.imag();
                zden = (-temp)*zden;
                j = j + 1;
            }
        }
        // Poles around unit circle like butterworth; then warp into cheby II
        const double sinhmu = 0.5*(exp(xmu) - exp(-xmu));
        const double coshmu = 0.5*(exp(xmu) + exp(-xmu));
        std::complex<double> znum = zone;
        for (int i = 0; i<n; i++)
        {
            std::complex<double> arg = std::complex<double>(0.0, M_PI*static_cast<double>(-n + 1 + 2 * i)*twoni);
            std::complex<double> temp = -exp(arg);

            // Warp into cheby ii
            temp = std::complex<double>(sinhmu*temp.real(), coshmu*temp.imag());
            temp = zone / temp;
            znum = (-temp)*znum;
            p[i].r = temp.real();
            p[i].i = temp.imag();

        }
        *k = (znum / zden).real();
    }
    
    return ierr;
}

STError_enum Cheby2::ProcessDetail()
{
    STError_enum ierr = ST_SUCCESS;

    SignalZPK_PrivateStruct zpk;
    //------------------------------------------------------------------------//
    memset(&zpk, 0, sizeof(SignalZPK_PrivateStruct));

    /*****************************************/
    /*     Begin Analog Prototype Design     */
    /*****************************************/

    zpk.nzeros = m_params.n;
    zpk.npoles = m_params.n;

    if (m_params.n % 2 == 1)
    {
        zpk.nzeros = zpk.nzeros - 1;
    }

    CallocMemory(&zpk.z, zpk.nzeros, &ierr);
    CallocMemory(&zpk.p, zpk.npoles, &ierr);
    
    if (ierr == ST_SUCCESS)
    {
        ierr = FilterDesign(m_params, zpk);
    }

    if (ierr != ST_SUCCESS)
    {
        STPrintError("Error designing filter prototype");
    }
    else
    {
        PrewarpStruct prewarp;
        // prewarp the frequencies
        PrewarpFrequencies(prewarp, m_produce_type, m_params);

        ierr = TransformPrototypeToAppropriateBand(zpk, prewarp, m_band_type);

    }

    FreeZPKStruct(zpk);

    return ierr;
}
