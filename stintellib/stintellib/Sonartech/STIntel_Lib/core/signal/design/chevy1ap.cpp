#include "chevy1ap.h"

STError_enum Cheby1::SetParameters(
    const IIRDesignBandType btype,
    /*const IIRDesignFilterType ftype,
    const IIRDesignProduceType ptype,*/
    const int n, const double rp, const double W0, const double W1)
{
    STError_enum ierr = ValidateFilterType(n, btype, /*ftype, ptype, */W0, W1);

    if(ierr == ST_SUCCESS)
    {
        FillFilterParameters(n, W0, W1, rp, btype/*, ftype, ptype*/);
        InitSetParameters();
    }

    return ierr;
}

STError_enum Cheby1::ValidateDesignParameters(SignalZPK_PrivateStruct& zpk_param, FilterParamStruct<double>& filter_param)
{
    complex* p = zpk_param.p;
    const int n = filter_param.n;
    const int npoles = zpk_param.npoles;

    STError_enum ierr = ST_SUCCESS;

    if (ST_SUCCESS == STReturnArrayTooSmallError(n, 1))
    {
        STReturnNullPointerError(p, &ierr);
    }

    if (ierr == ST_SUCCESS)
    {
        if (npoles != n)
        {
            if (npoles < n)
            {
                STPrintError(" Error insufficient space in p");
                ierr = ST_INVALID_INPUT;
            }
            else
            {
                STPrintWarning(" Warning not all poles will be initialized");
            }
        }
    }


    return ierr;
}

STError_enum Cheby1::FilterDesign(FilterParamStruct<double>& params, SignalZPK_PrivateStruct& zpk)
{
    STError_enum ierr = ST_SUCCESS;
    const int n = params.n;

    complex* p = zpk.p;

    double* k = &zpk.k;
    const double rp = params.r;
    *k = 1.0;

    double rpdb = pow(10.0, 0.1*rp);
    if (rpdb <= 1.0)
    {
        STPrintError("Error complex valued ripple factor");
        ierr = ST_INVALID_INPUT;
    }

    if(ierr == ST_SUCCESS)
    {
        // Ripple factor
        const double eps = sqrt(rpdb - 1.0);
        const double xmu = 1.0 / static_cast<double>(n)*asinh(1.0 / eps);

        // Arrange poles in an ellipse on the left half of the S-plane
        std::complex<double> zprod = std::complex<double>(1.0, 0.0);

        const double twoni = 1.0 / (2.0*static_cast<double>(n));

        //#pragma omp simd reduction(*:zprod)
        for (int i = 0; i<n; i++)
        {
            double xm = static_cast<double>(-n + 1 + 2 * i);
            double theta = M_PI*xm*twoni;
            std::complex<double> arg = std::complex<double>(xmu, theta); //xmu + theta*_Complex_I;
            std::complex<double> pp = -sinh(arg); // p =-sinh(mu + 1j*theta)
            zprod = (-pp)*zprod;
            p[i].r = pp.real();
            p[i].i = pp.imag();

        }
        *k = zprod.real(); //Take real

        if (n % 2 == 0)
        {
            *k = *k / sqrt(1.0 + eps*eps);
        }
    }
    
    return ierr;
}

STError_enum Cheby1::ProcessDetail()
{
    STError_enum ierr = ST_SUCCESS;

    SignalZPK_PrivateStruct zpk;
    //------------------------------------------------------------------------//
    memset(&zpk, 0, sizeof(SignalZPK_PrivateStruct));

    /*****************************************/
    /*     Begin Analog Prototype Design     */
    /*****************************************/

    
    zpk.z = nullptr;
    zpk.nzeros = 0;
    zpk.npoles = m_params.n;
    CallocMemory(&zpk.p, zpk.npoles, &ierr);
    
    if (ierr == ST_SUCCESS)
    {
        ierr = FilterDesign(m_params, zpk);
    }

    if (ierr != ST_SUCCESS)
    {
        STPrintError("Error designing filter prototype");
    }
    else
    {
        PrewarpStruct prewarp;
        // prewarp the frequencies
        PrewarpFrequencies(prewarp, m_produce_type, m_params);

        ierr = TransformPrototypeToAppropriateBand(zpk, prewarp, m_band_type);

    }

    FreeZPKStruct(zpk);

    return ierr;
}
