#include "filter_designer.h"



STError_enum FilterDesigner::Process_ZPKLp2Lp(const SignalZPK_PrivateStruct& zpk, double w0, SignalZPK_PrivateStruct& zpk_lp)
{
    STError_enum ierr = ST_SUCCESS;

    zpk_lp.k = 0.0;
    if (zpk.npoles < zpk.nzeros)
    {
        STPrintError("Negative order not fully considered");
        ierr = ST_INVALID_INPUT;
    }

    if (ierr == ST_SUCCESS)
    {
        // Scale all points radially from origin to shift cutoff frequency
        //zw0 = w0 + 0.0*_Complex_I;

        std::complex<double> zw0 = std::complex<double>(w0, 0.0);
        std::complex<double> z, p;
        for (int i = 0; i<zpk.nzeros; i++)
        {
            z.real(zpk.z[i].r);
            z.imag(zpk.z[i].i);

            zpk_lp.z[i].r = (zw0*z).real();
            zpk_lp.z[i].i = (zw0*z).imag();
        }
        for (int i = 0; i<zpk.npoles; i++)
        {
            p.real(zpk.p[i].r);
            p.imag(zpk.p[i].i);

            zpk_lp.p[i].r = (zw0*p).real();
            zpk_lp.p[i].i = (zw0*p).imag();
        }
        // Each shifted pole decreases gain by w0, each shifted zero increases
        // it.  Cancel out the net change to keep overall gain the same
        int ndeg = zpk.npoles - zpk.nzeros;
        zpk_lp.k = zpk.k*pow(w0, ndeg); //k^npoles/(k^nzeros)
    }
    
    return ierr;
}

STError_enum FilterDesigner::Process_ZPKLp2Hp(const SignalZPK_PrivateStruct& zpk, double w0, SignalZPK_PrivateStruct& zpk_hp)
{
    zpk_hp.k = 0.0;

    // Invert positions radially about unit circle to convert LPF TO HPF
    // Scale all points radially from origin to shift cutoff frequency

    std::complex<double> zw0 = std::complex<double>(w0, 0.0);
    std::complex<double> z;
    std::complex<double> p;

    for (int i = 0; i<zpk.nzeros; i++)
    {
        z.real(zpk.z[i].r);
        z.imag(zpk.z[i].i);

        zpk_hp.z[i].r = (zw0/z).real();
        zpk_hp.z[i].i = (zw0/z).imag();

    }
    for (int i = 0; i < zpk.npoles; i++)
    {
        p.real(zpk.p[i].r);
        p.imag(zpk.p[i].i);

        zpk_hp.p[i].r = (zw0 / p).real();
        zpk_hp.p[i].i = (zw0 / p).imag();

    }
    // If lowpass had zeros at infinity, inverting moves them to origin
    for (int i = zpk.nzeros; i < zpk.npoles; i++)
    {
        zpk_hp.z[i] = complex(0, 0);
    }
    // Compute scale factors
    std::complex<double> zprod = std::complex<double>(1.0, 0.0); //1.0 + 0.0*_Complex_I;
    for (int i = 0; i < zpk.nzeros; i++)
    {
        z.real(zpk.z[i].r);
        z.imag(zpk.z[i].i);

        zprod = (-z)*zprod;
    }
    std::complex<double> pprod = std::complex<double>(1.0, 0.0); //1.0 + 0.0*_Complex_I;
    for (int i = 0; i < zpk.npoles; i++)
    {
        p.real(zpk.p[i].r);
        p.imag(zpk.p[i].i);

        pprod = (-p)*pprod;
    }
    // Cancel out gain change caused by inversion
    std::complex<double> zt = zprod / pprod;
    zpk_hp.k = zpk.k*zt.real();

    return ST_SUCCESS;
}

STError_enum FilterDesigner::Process_ZPKLp2Bp(const SignalZPK_PrivateStruct& zpk, double w0, double bw, SignalZPK_PrivateStruct& zpk_bp)
{
    STError_enum ierr = ST_SUCCESS;

    
    STReturnArrayTooSmallError(zpk_bp.npoles, 2 * zpk.npoles, &ierr);
    if (ierr == ST_SUCCESS)
    {
        STReturnArrayTooSmallError(zpk_bp.nzeros, zpk.nzeros + zpk.npoles, &ierr);
    }
    if (ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(zpk_bp.p, &ierr);
    }
    if (ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(zpk_bp.z, &ierr);
    }
    
    if (ierr == ST_SUCCESS)
    {

        // Define some constants
        std::complex<double> zw02 = std::complex<double>(w0 * w0, 0.0); //w0*w0 + 0.0*_Complex_I;  //w0^2
        std::complex<double> zbw2 = std::complex<double>(bw / 2.0, 0.0); //bw/2.0 + 0.0*_Complex_I; //bw/2
                                                                         // Duplicate poles and zeros and shift from baseband to +w0 and -w0
        std::complex<double> z, p;
        int j = zpk.nzeros;
        for (int i = 0; i < zpk.nzeros; i++)
        {
            z.real(zpk.z[i].r);
            z.imag(zpk.z[i].i);
            std::complex<double> zlp = zbw2 * z;       //Scales zeros to desired bw
            std::complex<double> zlp2 = zlp * zlp;        //zlp^2
            std::complex<double> zdif = zlp2 - zw02;    //zlp^2 - w0^2
            std::complex<double> zsqrt = sqrt(zdif);   //sqrt(zlp^2 - w0^2)


            z = zlp + zsqrt; //zlp+sqrt(zlp^2-w0^2)
            zpk_bp.z[i].r = z.real();
            zpk_bp.z[i].i = z.imag();

            z = zlp - zsqrt; //zlp-sqrt(zlp^2-w0^2)
            zpk_bp.z[j].r = z.real();
            zpk_bp.z[j].i = z.imag();

            j = j + 1;
        }
        j = zpk.npoles;
        for (int i = 0; i < zpk.npoles; i++)
        {
            p.real(zpk.p[i].r);
            p.imag(zpk.p[i].i);

            std::complex<double> plp = zbw2 * p;       //Scales poles to desired bw
            std::complex<double> plp2 = plp * plp;        //plp^2
            std::complex<double> pdif = plp2 - zw02;    //plp^2 - w0^2
            std::complex<double> psqrt = sqrt(pdif);   //sqrt(plp^2 - w0^2)

            p = plp + psqrt; //zlp+sqrt(zlp^2-w0^2)
            zpk_bp.p[i].r = p.real();
            zpk_bp.p[i].i = p.imag();

            p = plp - psqrt; //zlp-sqrt(zlp^2-w0^2)
            zpk_bp.p[j].r = p.real();
            zpk_bp.p[j].i = p.imag();

            j = j + 1;
        }
        // Move degree zeros to origin leaving degree zeros at infinity for BPF 
        for (int i = 2 * zpk.nzeros; i < zpk_bp.nzeros; i++)
        {
            zpk_bp.z[i] = complex(0, 0);
        }
        // Cancel out gain change from frequency scaling
        zpk_bp.k = zpk.k*pow(bw, zpk.npoles - zpk.nzeros); //k*bw*bw^degree

    }
    return ierr;
}

STError_enum FilterDesigner::Process_ZPKLp2Bs(const SignalZPK_PrivateStruct& zpk, double w0, double bw,
    SignalZPK_PrivateStruct& zpk_bs)
{
    STError_enum ierr = ST_SUCCESS;

    std::complex<double> zone = std::complex<double>(1.0, 0.0); //1.0 + 0.0*_Complex_I;

    STReturnArrayTooSmallError(zpk_bs.npoles, 2 * zpk.npoles, &ierr);
    if (ierr == ST_SUCCESS)
    {
        STReturnArrayTooSmallError(zpk_bs.nzeros, 2 * zpk.npoles, &ierr);
    }
    if (ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(zpk_bs.p, &ierr);
    }
    if (ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(zpk_bs.z, &ierr);
    }

    if(ierr == ST_SUCCESS)
    {
        // Initialize some constants 
        std::complex<double> zw02 = std::complex<double>(w0 * w0, 0.0); // w0*w0 + 0.0*_Complex_I;  //w0^2
        std::complex<double> zbw2 = std::complex<double>(bw / 2.0, 0.0); // bw/2.0 + 0.0*_Complex_I; //bw/2
        // Duplicate poles and zeros and shift from baseband to +w0 and -w0
        std::complex<double> zprod = zone;   //Initialize product
        int j = zpk.nzeros;
        std::complex<double> z, p;
        for (int i = 0; i<zpk.nzeros; i++)
        {
            z.real(zpk.z[i].r);
            z.imag(zpk.z[i].i);
            // Invert to a highpass filter with desired bandwidth
            std::complex<double> zhp = zbw2 / z;       // bw2/2/z
            std::complex<double> zhp2 = zhp * zhp;        // zhp^2
            std::complex<double> zdif = zhp2 - zw02;    // zhp^2 - w0^2
            std::complex<double> zsqrt = sqrt(zdif);   // sqrt(zhp^2 - w0^2)
            z = zhp + zsqrt; // zhp+sqrt(zhp^2-w0^2)
            zpk_bs.z[i].r = z.real();
            zpk_bs.z[i].i = z.imag();

            z = zhp - zsqrt; // zhp-sqrt(zhp^2-w0^2)
            zpk_bs.z[j].r = z.real();
            zpk_bs.z[j].i = z.imag();

            // Update product
            z.real(zpk.z[i].r);
            z.imag(zpk.z[i].i);

            zprod = (-z)*zprod;
            j = j + 1;
        }

        std::complex<double> pprod = zone; //Initialize product
        j = zpk.npoles;
        for (int i = 0; i<zpk.npoles; i++)
        {
            p.real(zpk.p[i].r);
            p.imag(zpk.p[i].i);

            // Invert to a highpass filter with desired bandwidth
            std::complex<double> php = zbw2 / p;       // bw2/2/p
            std::complex<double> php2 = php * php;        // php^2
            std::complex<double> pdif = php2 - zw02;    // php^2 - w0^2
            std::complex<double> psqrt = sqrt(pdif);   // sqrt(php^2 - w0^2)
            p = php + psqrt; // php+sqrt(php^2-w0^2)
            zpk_bs.p[i].r = p.real();
            zpk_bs.p[i].i = p.imag();

            p = php - psqrt; // php-sqrt(php^2-w0^2)
            zpk_bs.p[j].r = p.real();
            zpk_bs.p[j].i = p.imag();

            // Update product
            p.real(zpk.p[i].r);
            p.imag(zpk.p[i].i);
            pprod = (-p)*pprod;
            j = j + 1;

        }

        // Move any zeros at infinity to center of stopband
        j = 2 * zpk.nzeros;
        for (int i = 0; i < zpk.npoles - zpk.nzeros; i++)
        {
            zpk_bs.z[j] = complex(0.0, w0); //0.0 + w0*_Complex_I;
            j = j + 1;
        }
        for (int i = 0; i < zpk.npoles - zpk.nzeros; i++)
        {
            zpk_bs.z[j] = complex(0.0, -w0); //0.0 - w0*_Complex_I;
            j = j + 1;
        }
        // Cancel out gain change caused by inversion
        std::complex<double> zdiv = zprod / pprod;
        zpk_bs.k = zpk.k*real(zdiv);
    }
    
    return ST_SUCCESS;
}

STError_enum FilterDesigner::Process_ZPKBilinearTranform(const SignalZPK_PrivateStruct& zpkt, SignalZPK_PrivateStruct& zpkz, const double fs)
{
    STError_enum ierr = ST_SUCCESS;
    std::complex<double> zden, zdif, znum;
    
    if (zpkt.nzeros > zpkt.npoles)
    {
        STPrintError("Cannot have more zeros than poles");
        ierr = ST_INVALID_INPUT;
    }

    if (ierr == ST_SUCCESS)
    {
        // Bilinear transform
        std::complex<double> fs2(2.0 * fs,0);
        std::complex<double> z, p;
        for (int i = 0; i<zpkt.nzeros; i++)
        {
            z.real(zpkt.z[i].r);
            z.imag(zpkt.z[i].i);

            znum = fs2 + z;
            zden = fs2 - z;
            z = znum / zden; //(fs2 + z)/(fs2 - z)

            zpkz.z[i].r = z.real();
            zpkz.z[i].i = z.imag();
        }

        for (int i = 0; i<zpkt.npoles; i++)
        {
            p.real(zpkt.p[i].r);
            p.imag(zpkt.p[i].i);

            znum = fs2 + p;
            zden = fs2 - p;
            p = znum / zden; //(fs2 + p)/(fs2 - p) 

            zpkz.p[i].r = p.real();
            zpkz.p[i].i = p.imag();
        }

        // Any zeros at infinity get moved to Nyquist frequency
        for (int i = zpkt.nzeros; i<zpkt.npoles; i++)
        {
            zpkz.z[i] = complex(-1.0, 0.0); //-1.0 + 0.0*_Complex_I;
        }


        // Compensate for gain change
        znum = std::complex<double>(1.0, 0.0); //1.0 + 0.0*_Complex_I;
        for (int i = 0; i<zpkt.nzeros; i++)
        {
            z.real(zpkt.z[i].r);
            z.imag(zpkt.z[i].i);

            zdif = fs2 - z;
            znum = znum*zdif;
        }

        zden = std::complex<double>(1.0, 0.0); //1.0 + 0.0*_Complex_I;
        for (int i = 0; i<zpkt.npoles; i++)
        {
            p.real(zpkt.p[i].r);
            p.imag(zpkt.p[i].i);

            zdif = fs2 - p;
            zden = zden*zdif;
        }

        const std::complex<double> zk = znum / zden;
        zpkz.k = zpkt.k*real(zk);
    }
    
    return ierr;
}

void FilterDesigner::ErrorCallingFilterTransform(SignalZPK_PrivateStruct& zpk)
{
    STPrintError("Error calling zpk2lp2lp");
    FreeMemory(&zpk.p);
    FreeMemory(&zpk.z);

    memset(&zpk, 0, sizeof(SignalZPK_PrivateStruct));

}

void FilterDesigner::SetTransformSpace(SignalZPK_PrivateStruct& zpk, STError_enum* ierr)
{
    if (zpk.npoles > 0)
    {
        CallocMemory(&zpk.p, zpk.npoles, ierr);
    }

    if (zpk.nzeros > 0)
    {
        CallocMemory(&zpk.z, zpk.nzeros, ierr);
    }
}

FilterDesigner::SignalZPK_PrivateStruct FilterDesigner::ZPKLp2Lp(
    SignalZPK_PrivateStruct& zpk, double w0, STError_enum* ierr)
{
    SignalZPK_PrivateStruct zpk_lp;

    zpk_lp.nzeros = zpk.nzeros;
    zpk_lp.npoles = zpk.npoles;

    SetTransformSpace(zpk_lp, ierr);

    if (*ierr == ST_SUCCESS)
    {
        *ierr = Process_ZPKLp2Lp(zpk, w0, zpk_lp);
    }
    
    if (*ierr != ST_SUCCESS)
    {
        ErrorCallingFilterTransform(zpk_lp);
    }

    return zpk_lp;
}

FilterDesigner::SignalZPK_PrivateStruct FilterDesigner::ZPKLp2Hp(SignalZPK_PrivateStruct& zpk, double w0, STError_enum* ierr)
{
    SignalZPK_PrivateStruct zpk_hp;

    zpk_hp.nzeros = std::max(zpk.nzeros, zpk.npoles);
    zpk_hp.npoles = zpk.npoles;

    SetTransformSpace(zpk_hp, ierr);

    if (*ierr == ST_SUCCESS)
    {
        *ierr = Process_ZPKLp2Hp(zpk, w0, zpk_hp);
    }
    
    if (*ierr != ST_SUCCESS)
    {
        ErrorCallingFilterTransform(zpk_hp);
    }

    return zpk_hp;
}

FilterDesigner::SignalZPK_PrivateStruct FilterDesigner::ZPKLp2Bp(SignalZPK_PrivateStruct& zpk, double w0, double bw, STError_enum* ierr)
{
    SignalZPK_PrivateStruct zpk_bp;
    
    zpk_bp.nzeros = zpk.nzeros + zpk.npoles;
    zpk_bp.npoles = 2 * zpk.npoles;
    SetTransformSpace(zpk_bp, ierr);

    if (*ierr == ST_SUCCESS)
    {
        *ierr = Process_ZPKLp2Bp(zpk, w0, bw, zpk_bp);
    }
    
    if (*ierr != ST_SUCCESS)
    {
        ErrorCallingFilterTransform(zpk_bp);
    }

    return zpk_bp;
}

FilterDesigner::SignalZPK_PrivateStruct FilterDesigner::ZPKLp2Bs(SignalZPK_PrivateStruct& zpk, double w0, double bw, STError_enum* ierr)
{
    SignalZPK_PrivateStruct zpk_bs;

    zpk_bs.nzeros = 2 * zpk.npoles;
    //zpk_bs.nzeros = 2 * zpk.nzeros;
    zpk_bs.npoles = 2 * zpk.npoles;

    SetTransformSpace(zpk_bs, ierr);

    if(*ierr == ST_SUCCESS)
    {
        *ierr = Process_ZPKLp2Bs(zpk, w0, bw, zpk_bs);
    }
    
    if (*ierr != ST_SUCCESS)
    {
        ErrorCallingFilterTransform(zpk_bs);
    }

    return zpk_bs;
}

FilterDesigner::SignalZPK_PrivateStruct FilterDesigner::ZPKBilinearTransform(SignalZPK_PrivateStruct& zpkt, const double fs, STError_enum* ierr)
{
    SignalZPK_PrivateStruct zpkz;
    zpkz.nzeros = std::max(zpkt.nzeros, zpkt.npoles);
    zpkz.npoles = zpkt.npoles;

    // Set space
    SetTransformSpace(zpkz, ierr);
    if (*ierr == ST_SUCCESS)
    {
        // Perform bilinear transform
        *ierr = Process_ZPKBilinearTranform(zpkt, zpkz, fs);
    }

    if (*ierr != ST_SUCCESS)
    {
        ErrorCallingFilterTransform(zpkz);
    }
    return zpkz;
}

void FilterDesigner::CheckBAzVarSize(const SignalZPK_PrivateStruct& zpk_iir, const SignalBAz_Struct& ba, STError_enum* ierr)
{
    // Some checks
    if (zpk_iir.k == 0.0)
    {
        STPrintWarning("System gain is zero");
    }
    if (zpk_iir.nzeros < 0)
    {
        STPrintError("Error invalid number of zeros");
        *ierr = ST_INVALID_INPUT;
    }
    if (zpk_iir.npoles < 0)
    {
        STPrintError("Error invalid number of poles");
        *ierr = ST_INVALID_INPUT;
    }

    if (*ierr == ST_SUCCESS)
    {
        STReturnArrayTooSmallError(ba.nb, zpk_iir.nzeros + 1, ierr);
    }
    
    if (*ierr == ST_SUCCESS)
    {
        STReturnArrayTooSmallError(ba.na, zpk_iir.npoles + 1, ierr);
    }
        
    if (*ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(ba.b, ierr);
    }
        
    if (*ierr == ST_SUCCESS)
    {
        STReturnNullPointerError(ba.a, ierr);
    }
        

}

STError_enum FilterDesigner::ProcessPolynomialTransferFunctionFromZPK(
    const SignalZPK_PrivateStruct& zpk_iir, SignalBAz_Struct& ba)
{
    STError_enum ierr = ST_SUCCESS;
    CheckBAzVarSize(zpk_iir, ba, &ierr);
    
    if(ierr == ST_SUCCESS)
    {
        // Compute polynomial representation of poles by expanding:
        //  (p - p_1)*(p - p_2)*...*(p - p_npoles)
        m_poly.param.nord = zpk_iir.npoles;
        m_poly.param.np = zpk_iir.npoles;
        m_poly.param.p = zpk_iir.p;
        m_poly.finder->SetParameters(&m_poly.param, COMPLEX);
        
        m_poly.finder->SetDestination(&m_poly.dst);
        
        ierr = m_poly.finder->Process();

        if (ierr != ST_SUCCESS)
        {
            STPrintError("Error calling poly; poles");
        }
        else
        {
            //std::copy(m_poly.dst.begin(), m_poly.dst.end(), &ba.a[0]);
            memcpy(&ba.a[0], m_poly.dst.y.data(), m_poly.dst.y.size() * sizeof(complex));
            ba.na = m_poly.dst.y.size();
        }


        if (ierr == ST_SUCCESS)
        {
            // Compute polynomial representation of zeros by expanding:
            //  (z - z_1)*(z - z_2)*...*(z - z_nzeros)

            m_poly.param.nord = zpk_iir.nzeros;
            m_poly.param.np = zpk_iir.nzeros;
            m_poly.param.p = zpk_iir.z;
            m_poly.finder->SetParameters(&m_poly.param, COMPLEX);
            m_poly.finder->SetDestination(&m_poly.dst);

            ierr = m_poly.finder->Process();
            if (ierr != ST_SUCCESS)
            {
                STPrintError("Error calling poly; zeros");
            }
            else
            {
                //std::copy(m_poly.dst.begin(), m_poly.dst.end(), ba.b);
                memcpy(&ba.b[0], m_poly.dst.y.data(), m_poly.dst.y.size() * sizeof(complex));
                ba.nb = m_poly.dst.y.size();

                // Introduce system gain into numerator zeros
                cblas_zdscal(zpk_iir.nzeros + 1, zpk_iir.k, ba.b, 1);

                //PRAGMA_OMP_SIMD
                //for (i=0; i<nzeros+1; i++)
                //{
                //    bz[i] = k*bz[i];
                //}
            }
        }
    }


    return ierr;
}

void FilterDesigner::CheckZPK2TF64FVars(const SignalZPK_PrivateStruct& zpk_iir, STError_enum* ierr)
{
    // Some checks
    if (zpk_iir.k == 0.0)
    {
        STPrintWarning("System gain is zero");
    }
    if (zpk_iir.nzeros < 0)
    {
        STPrintError("Error invalid number of zeros %d");
        *ierr = ST_INVALID_INPUT;

    }
    if (*ierr != ST_SUCCESS)
    {
        if (zpk_iir.npoles < 0)
        {
            STPrintError("Error invalid number of poles %d");
            *ierr = ST_INVALID_INPUT;
        }
    }


}

STError_enum FilterDesigner::Process_ZPK2TF64F(const SignalZPK_PrivateStruct& zpk_iir, SignalBAf_Struct& ba)
{
    STError_enum ierr = ST_SUCCESS;
    SignalBAz_Struct baz;

    CheckZPK2TF64FVars(zpk_iir, &ierr);

    if (ierr == ST_SUCCESS)
    {
        // Compute transfer function from poles/zeros
        baz.nb = zpk_iir.nzeros + 1;
        baz.na = zpk_iir.npoles + 1;
        CallocMemory(&baz.b, baz.nb, &ierr);
        CallocMemory(&baz.a, baz.na, &ierr);
        ierr = ProcessPolynomialTransferFunctionFromZPK(zpk_iir, baz);
        if (ierr != ST_SUCCESS)
        {
            STPrintError("Error computing transfer function");
        }
    }

    if (ierr != ST_SUCCESS)
    {
        STPrintError("Error computing transfer function");
        ArrayZeros(ba.na, &ba.a, &ierr);
        ArrayZeros(ba.nb, &ba.b, &ierr);
        
    }
    else
    {
        // For now I'm going to assume the expanded polynomials are real
        PRAGMA_OMP_SIMD
        for (int i = 0; i < zpk_iir.npoles + 1; i++)
        {
            ba.a[i] = baz.a[i].r;
        }
        PRAGMA_OMP_SIMD
        for (int i = 0; i < zpk_iir.nzeros + 1; i++)
        {
            ba.b[i] = baz.b[i].r;
        }


    }
    // free memory
    FreeMemory(&baz.a);
    FreeMemory(&baz.b);
    /*if (ierr != ST_SUCCESS)
    {
        FreeMemory(&ba.b);
        FreeMemory(&ba.a);
        
        memset(&ba, 0, sizeof(SignalBA_Struct));
    }*/
    //return ba;
    return ierr;

}

void FilterDesigner::CheckZPK2TFVars(const SignalZPK_PrivateStruct& zpk_iir, STError_enum* ierr)
{
    // Some checks
    if (zpk_iir.k == 0.0)
    {
        STPrintWarning("System gain is zero");
    }
    if (zpk_iir.nzeros < 0)
    {
        STPrintError("Error invalid number of zeros");
        *ierr = ST_INVALID_INPUT;

    }
    if (*ierr == ST_SUCCESS)
    {
        if (zpk_iir.npoles < 0)
        {
            STPrintError("Error invalid number of poles");
            *ierr = ST_INVALID_INPUT;
        }
    }
}

FilterDesigner::SignalBAf_Struct FilterDesigner::ZPK2TF(const SignalZPK_PrivateStruct& zpk_iir, STError_enum* ierr)
{
    SignalBAf_Struct ba;
    
    // Initialize
    *ierr = ST_SUCCESS;

    CheckZPK2TFVars(zpk_iir, ierr);

    if (*ierr == ST_SUCCESS)
    {
        // Compute transfer function from poles/zeros
        ba.nb = zpk_iir.nzeros + 1;
        ba.na = zpk_iir.npoles + 1;
        CallocMemory(&ba.b, ba.nb, ierr);

        if (*ierr == ST_SUCCESS)
        {
            CallocMemory(&ba.a, ba.na, ierr);
            if(*ierr == ST_SUCCESS)
            {
                *ierr = Process_ZPK2TF64F(zpk_iir, ba);
                if (*ierr != ST_SUCCESS)
                {
                    STPrintError("Error computing transfer function");
                }
            }            
        }
    }

    if (*ierr != ST_SUCCESS)
    {
        FreeMemory(&ba.b);
        FreeMemory(&ba.a);
        memset(&ba, 0, sizeof(SignalBAf_Struct));
    }
    return ba;
}

void FilterDesigner::PrewarpFrequencies(PrewarpStruct& prewarp, IIRDesignProduceType produce_type, FilterParamStruct<double>& wn_param)
{
    if (produce_type == DIGITAL)
    {
        prewarp.fs = 2.0;
        prewarp.warped[0] = 2.0*prewarp.fs*tan(M_PI*wn_param.Wn[0] / prewarp.fs);
        prewarp.warped[1] = 2.0*prewarp.fs*tan(M_PI*wn_param.Wn[1] / prewarp.fs);
    }
    else // m_produce_type == ANALOG
    {
        prewarp.fs = 1.0;
        prewarp.warped[0] = wn_param.Wn[0];
        prewarp.warped[1] = wn_param.Wn[1];
        if (wn_param.Wn[1] > 1.0)
        {
            STPrintWarning("wn[1] > 1; result may be unstable");
        }
    }

}

void FilterDesigner::FillFilterParameters(
    const int n, const double W0, const double W1, const double r,
    const IIRDesignBandType btype/*,
    const IIRDesignFilterType ftype,
    const IIRDesignProduceType ptype*/)
{

    memset(&ba_iir, 0, sizeof(struct SignalBAf_Struct));
    memset(&zpk_iir, 0, sizeof(struct SignalZPK_PrivateStruct));

    m_params.n = n;
    m_params.Wn[0] = W0;
    m_params.Wn[1] = W1;
    m_params.r = r;
    m_band_type = btype;
    //m_filter_type = ftype;
    //m_produce_type = ptype;
}

STError_enum FilterDesigner::ValidateFilterType(const int n, const IIRDesignBandType btype,
                                                /*const IIRDesignFilterType ftype, 
                                                const IIRDesignProduceType ptype,*/
                                                const double W0, const double W1)
{
    STError_enum ierr = ST_SUCCESS;

    if (n < 1)
    {
        STPrintError("Error n is must be positive");
        ierr = ST_INVALID_INPUT;
    }

    //if (ierr == ST_SUCCESS)
    //{
    //    if ((ptype != FilterDesigner::DIGITAL) && (ptype != FilterDesigner::ANALOG) == true)
    //    {
    //        STPrintError("Filter produce type is not recognized");
    //        ierr = ST_INVALID_INPUT;
    //    }
    //}
    
    if(ierr == ST_SUCCESS)
    {
        if (btype != DESIGN_IIRBAND_LOWPASS  &&
            btype != DESIGN_IIRBAND_HIGHPASS &&
            btype != DESIGN_IIRBAND_BANDPASS &&
            btype != DESIGN_IIRBAND_BANDSTOP)
        {
            STPrintError("Filter band is not recognized");
            ierr = ST_INVALID_INPUT;
        }
    }
    
    if(ierr == ST_SUCCESS)
    {
        if((btype == DESIGN_IIRBAND_BANDSTOP || btype == DESIGN_IIRBAND_BANDPASS) == true)
        {
            if (W1 == -1)
            {
                STPrintError("cutoff is not valid");
                ierr = ST_INVALID_INPUT;
            }

            if(W0 > W1)
            {
                STPrintError("Error Wn is in improper order");
                ierr = ST_INVALID_INPUT;
            }

            if (W1 > 1.0)
            {
                STPrintError("Error Wn[1] > 1");
                ierr = ST_INVALID_INPUT;
            }
        }
    }

    //if(ierr == ST_SUCCESS)
    //{
    //    if (ftype != DESIGN_IIRTYPE_BUTTER &&
    //        ftype != DESIGN_IIRTYPE_BESSEL &&
    //        ftype != DESIGN_IIRTYPE_CHEBY1 &&
    //        ftype != DESIGN_IIRTYPE_CHEBY2)
    //    {
    //        STPrintError("Filter type is not recognized");
    //        ierr = ST_INVALID_INPUT;
    //    }
    //}

    if(ierr == ST_SUCCESS)
    {
        if (W0 < 0.0)
        {
            STPrintError("Error Wn[0] < 0");
            ierr = ST_INVALID_INPUT;
            
        }
        if (W0 > 1.0)
        {
            STPrintError("Error Wn[0] > 1");
            ierr = ST_INVALID_INPUT;
        }

    }
    
    return ierr;
}

//void FilterDesigner::SetResultToDestination(const SignalZPK_PrivateStruct& zpkout, SignalZPK_Struct* dst)
//{
//    dst->npoles = zpkout.npoles;
//    dst->p.resize(dst->npoles);
//    std::copy(zpkout.p, zpkout.p + zpkout.npoles, dst->p.begin());
//
//
//    dst->nzeros = zpkout.nzeros;
//    dst->z.resize(dst->nzeros);
//    std::copy(zpkout.z, zpkout.z + zpkout.nzeros, dst->z.begin());
//
//    dst->k = zpkout.k;
//    
//}

void FilterDesigner::FreeZPKStruct(SignalZPK_PrivateStruct& zpk)
{
    FreeMemory(&zpk.z);
    FreeMemory(&zpk.p);
    memset(&zpk, 0, sizeof(SignalZPK_PrivateStruct));
}

STError_enum FilterDesigner::TransformPrototypeToAppropriateBand(SignalZPK_PrivateStruct& zpk,
                                                                 const PrewarpStruct& prewarp, IIRDesignBandType btype)
{
    double w0;
    double bw;
    STError_enum ierr = ST_SUCCESS;
    SignalZPK_PrivateStruct zpkt{};
    SignalZPK_PrivateStruct zpkout{};

    // Transform to lowpass, bandpass, highpass, or bandstop
    if (btype == DESIGN_IIRBAND_LOWPASS)
    {
        w0 = prewarp.warped[0];
        zpkt = ZPKLp2Lp(zpk, w0, &ierr);
        if (ierr != ST_SUCCESS)
        {
            STPrintError("Error converting to lowpass");
        }
    }
    else if (btype == DESIGN_IIRBAND_HIGHPASS)
    {
        w0 = prewarp.warped[0];
        zpkt = ZPKLp2Hp(zpk, w0, &ierr);
        if (ierr != ST_SUCCESS)
        {
            STPrintError("Error converting to highpass");
        }
    }
    else if (btype == DESIGN_IIRBAND_BANDPASS)
    {
        bw = prewarp.warped[1] - prewarp.warped[0];
        w0 = sqrt(prewarp.warped[0] * prewarp.warped[1]);
        zpkt = ZPKLp2Bp(zpk, w0, bw, &ierr);
        if (ierr != ST_SUCCESS)
        {
            STPrintError("Error converting to bandpass");
        }
    }
    else if (btype == DESIGN_IIRBAND_BANDSTOP)
    {
        bw = prewarp.warped[1] - prewarp.warped[0];
        w0 = sqrt(prewarp.warped[0] * prewarp.warped[1]);
        zpkt = ZPKLp2Bs(zpk, w0, bw, &ierr);
        if (ierr != ST_SUCCESS)
        {
            STPrintError("Error converting to bandstop");
        }
    }
    else
    {
        STPrintError("Invalid band type");
    }



    if (ierr == ST_SUCCESS)
    {
        // Find the discrete equivalent if necessary
        if (m_produce_type == DIGITAL)
        {
            // Bilinear transform
            zpkout = ZPKBilinearTransform(zpkt, prewarp.fs, &ierr);
            if (ierr != ST_SUCCESS)
            {
                STPrintError("Error in bilinear transform");
            }
            // Otherwise just copy
        }
        else
        {
            zpkout.npoles = zpkt.npoles;
            zpkout.nzeros = zpkt.nzeros;
            CallocMemory(&zpkout.p, zpkout.npoles, &ierr);
            zpkout.z = nullptr;
            if (zpkt.nzeros > 0 && ierr == ST_SUCCESS)
            {
                CallocMemory(&zpkout.z, zpkout.nzeros, &ierr);
            }

            if (ierr == ST_SUCCESS)
            {
                zpkout.k = zpkt.k;
                for (int i = 0; i < zpkout.npoles; i++)
                {
                    zpkout.p[i] = zpkt.p[i];
                }
                for (int i = 0; i < zpkout.nzeros; i++)
                {
                    zpkout.z[i] = zpkt.z[i];
                }
            }   
        }
    }
    
    // Convert to transfer function
    ba_iir = ZPK2TF(zpkout, &ierr);


    if(ierr == ST_SUCCESS)
    {
        //SetResultToDestination(zpkout, static_cast<SignalZPK_Struct*>(m_result.dst));
        SetResultToDestination(ba_iir, m_result);
    }

    FreeMemory(&ba_iir.a);
    FreeMemory(&ba_iir.b);
    memset(&ba_iir, 0, sizeof(SignalBAf_Struct));

    
    FreeZPKStruct(zpkt);
    FreeZPKStruct(zpkout);

    return ierr;
}


void FilterDesigner::SetResultToDestination(const SignalBAf_Struct& ba_iir, DestinationStruct& result)
{
    //((SignalBA_Struct<double>*)(result.dst))->
    static_cast<SignalBA_Struct<double>*>(result.dst)->na = ba_iir.na;
    static_cast<SignalBA_Struct<double>*>(result.dst)->nb = ba_iir.nb;
    static_cast<SignalBA_Struct<double>*>(result.dst)->a.resize(ba_iir.na);
    static_cast<SignalBA_Struct<double>*>(result.dst)->b.resize(ba_iir.nb);
    memcpy(static_cast<SignalBA_Struct<double>*>(result.dst)->a.data(), ba_iir.a, sizeof(double)*ba_iir.na);
    memcpy(static_cast<SignalBA_Struct<double>*>(result.dst)->b.data(), ba_iir.b, sizeof(double)*ba_iir.nb);
}