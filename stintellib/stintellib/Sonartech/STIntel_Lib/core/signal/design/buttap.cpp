#include "buttap.h"



STError_enum Butter::SetParameters(
    const IIRDesignBandType btype,
    /*const IIRDesignFilterType ftype,
    const IIRDesignProduceType ptype,*/
    const int n, const double W0, const double W1)
{
    STError_enum ierr = ValidateFilterType(n, btype, /*ftype, ptype,*/ W0, W1);

    if(ierr == ST_SUCCESS)
    {
        const double r = -1;
        FillFilterParameters(n, W0, W1, r, btype/*, ftype, ptype*/);
        InitSetParameters();
    }

    return ierr;
}

STError_enum Butter::ValidateDesignParameters(SignalZPK_PrivateStruct& zpk_param, FilterParamStruct<double>& filter_param)
{
    complex* p = zpk_param.p;
    const int n = filter_param.n;
    const int npoles = zpk_param.npoles;

    STError_enum ierr = ST_SUCCESS;

    if (ST_SUCCESS == STReturnArrayTooSmallError(n, 1))
    {
        STReturnNullPointerError(p, &ierr);
    }

    if(ierr == ST_SUCCESS)
    {
        if (npoles != n)
        {
            if (npoles < n)
            {
                STPrintError(" Error insufficient space in p");
                ierr = ST_INVALID_INPUT;
            }
            else
            {
                STPrintWarning(" Warning not all poles will be initialized");
            }
        }
    }
    


    return ierr;
}

STError_enum Butter::FilterDesign(FilterParamStruct<double>& params, SignalZPK_PrivateStruct& zpk)
{
    int n = params.n;
    complex* p = zpk.p;
    double* k = &zpk.k;
    *k = 0.0;

    STError_enum ierr = ValidateDesignParameters(zpk, params);

    if (ierr == ST_SUCCESS)
    {
        const double pi_twoni = M_PI / (2.0 * static_cast<double>(n));
        for (int i = 0; i < n; i++)
        {
            const double xm = static_cast<double>(-n + 1 + 2 * i);
            std::complex<double> arg = std::complex<double>(0.0, pi_twoni * xm); //arg = 0.0 + 1j*pi*xm*twoni;
            arg = -std::exp(arg); // p = -numpy.exp(1j * pi * m / (2 * N))
            p[i].r = arg.real();
            p[i].i = arg.imag();

        }
        *k = 1.0; //k = real(prod(-p)) which is 1
    }


    return ierr;
}

STError_enum Butter::ProcessDetail()
{
    STError_enum ierr = ST_SUCCESS;

    SignalZPK_PrivateStruct zpk;
    //------------------------------------------------------------------------//
    memset(&zpk, 0, sizeof(SignalZPK_PrivateStruct));

    zpk.nzeros = 0;
    zpk.npoles = m_params.n;
    zpk.z = nullptr;

    /*****************************************/
    /*     Begin Analog Prototype Design     */
    /*****************************************/

    CallocMemory(&zpk.p, m_params.n, &ierr);

    if (ierr == ST_SUCCESS)
    {
        ierr = FilterDesign(m_params, zpk);
    }

    if (ierr != ST_SUCCESS)
    {
        STPrintError("Error designing filter prototype");
        FreeZPKStruct(zpk);
    }
    else
    {
        PrewarpStruct prewarp;
        // prewarp the frequencies
        PrewarpFrequencies(prewarp, m_produce_type, m_params);

        ierr = TransformPrototypeToAppropriateBand(zpk, prewarp, m_band_type);
        //static_cast<SignalZPK_Struct*>(m_result.dst)->p;

    }

    FreeZPKStruct(zpk);

    return ierr;
}
