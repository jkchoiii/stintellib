#pragma once
#include "filter_designer.h"


class Bessel : public FilterDesigner
{

private:
    

private:
    virtual STError_enum ValidateDesignParameters(SignalZPK_PrivateStruct& zpk_param, FilterParamStruct<double>& filter_param) override;


    
    STError_enum ProcessDetail() override;

    STError_enum SetParameters(const enum IIRDesignBandType btype,
        /*const enum IIRDesignFilterType ftype,
        const enum IIRDesignProduceType ptype,*/
        const int n, const double W0, const double W1 = -1);

public:

    Bessel()
    {
    }
    Bessel(IIRDesignProduceType produce_type)
    {
        m_produce_type = produce_type;
    }
    virtual ~Bessel()
    {
    }



    STError_enum SetParameters(void* param, INPUT_DATA_TYPE input_type) override
    {
        auto p = static_cast<BesselFilterParamStruct<void>*>(param);
        const STError_enum ierr = SetParameters(p->band_type, p->n, p->Wn[0], p->Wn[1]);

        return ierr;
    }

    //virtual STError_enum SignalDesign(int n, int npoles, complex* p, double* k) override;
    virtual STError_enum FilterDesign(FilterParamStruct<double>& params, SignalZPK_PrivateStruct& zpk) override;

    template<typename T>
    struct BesselFilterParamStruct
    { 
        int n; // 필터 차수
        double Wn[2]; // (0, 1) 사이의 정규화된 차단 주파수 -> Wn[0]은 low/highpass filter. Wn[0]과 Wn[1]은 bandpass, bandstop 필터에서 사용

        IIRDesignBandType band_type; // 해당 값에 따라 lowpass, highpass, bandpass, bandstop 필터 지정

        BesselFilterParamStruct()
        {
            n = -1;
            Wn[0] = -1;
            Wn[1] = -1;
            //lanalog = false;
            band_type = DESIGN_IIRBAND_INVALID;
        }
    };

    template<typename T>
    BesselFilterParamStruct<T> GetParameterStruct()
    {
        return BesselFilterParamStruct<T>();
    }

    virtual enum STError_enum SetDestination(void* dst) override
    {
        enum STError_enum ierr = ST_SUCCESS;

        m_result.dst = dst;
        InitSetDestination();

        /*if (m_state.param_state == true)
        {
        m_result.dst = dst;
        InitSetDestination();
        }
        else
        {
        ierr = ST_INVALID_INPUT;
        }*/

        return ierr;
    }



};


