#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include <complex>
#include <vector>

#include "../filter/filter.h"
#include "../../STIntelLib.h"
#include "../../polynomial/poly.h"



//class FilterDesigner : public STIntelLib
class FilterDesigner : public Filter
{
private:
    
    struct PolyRootStruct
    {
        std::unique_ptr<Poly> finder;
        
        Poly::PolynomialParameterStruct<complex> param;
        Poly::PolynomialDestinationStruct<complex> dst;
    };

    PolyRootStruct m_poly;


    struct SignalZPK_Struct
    {
        //complex *p;     /*!< Poles.  This is an array of dimension [npoles]. */
        //complex *z;     /*!< Zeros.  This is an array of dimension [nzeros]. */
        std::vector<complex> p;
        std::vector<complex> z;
        double k;    /*!< Gain. */
        int npoles;  /*!< Number of poles. */
        int nzeros;  /*!< Number of zeros. */
        SignalZPK_Struct()
        {
            p.clear();
            z.clear();
            //p = nullptr;
            //z = nullptr;
            k = 0;
            npoles = 0;
            nzeros = 0;
        }
    };


    struct SignalBAf_Struct
    {
        double *b; /*!< Numerator polynomial coefficients [nb] */
        double *a; /*!< Denominator polynomial coefficients [na] */
        int na;    /*!< Number of numerator coefficients */
        int nb;    /*!< Number of denominator coefficients */
        SignalBAf_Struct()
        {
            b = nullptr;
            a = nullptr;
            na = 0;
            nb = 0;
        }
    };


protected:
    struct PrewarpStruct
    {
        double fs;
        double warped[2];
        PrewarpStruct()
        {
            fs = 0.0;
            warped[0] = 0.0;
            warped[1] = 0.0;
        }
    };

    struct SignalBAz_Struct
    {
        complex* b; /*!< Numerator polynomial coefficients [nb] */
        complex* a; /*!< Denominator polynomial coefficients [na] */
        int na;    /*!< Number of numerator coefficients */
        int nb;    /*!< Number of denominator coefficients */
        SignalBAz_Struct()
        {
            b = nullptr;
            a = nullptr;
            na = 0;
            nb = 0;
        }
    };

public:

    enum IIRDesignProduceType
    {
        ANALOG = 0,
        DIGITAL = 1
    };

    enum IIRDesignBandType
    {
        DESIGN_IIRBAND_LOWPASS = 0,   /*!< Lowpass filter design. */
        DESIGN_IIRBAND_HIGHPASS = 1,  /*!< Highpass filter design. */
        DESIGN_IIRBAND_BANDPASS = 2,  /*!< Bandpass filter design. */
        DESIGN_IIRBAND_BANDSTOP = 3,  /*!< Bandstop filter design. */
        DESIGN_IIRBAND_INVALID = 4    /*!< Invalid. */
    };

    enum IIRDesignFilterType
    {
        DESIGN_IIRTYPE_BUTTER = 0, /*!< Butterworth filter design. */
        DESIGN_IIRTYPE_CHEBY1 = 1, /*!< Chebyshev I filter design. */
        DESIGN_IIRTYPE_CHEBY2 = 2, /*!< Chebyshev II filter design. */
        DESIGN_IIRTYPE_BESSEL = 3, /*!< Bessel filter design. */
        DESIGN_IIRTYPE_INVALID = 4 /*!< Invalid IIR design prototype. */
    };

public:
    template <typename T>
    struct FilterParamStruct
    {
        int n;
        double Wn[2];
        double r;
        //bool lanalog;

        IIRDesignBandType band_type;
        //const IIRDesignFilterType filter_type;
        //const IIRDesignProduceType produce_type;

        FilterParamStruct()
        {
            n = -1;
            Wn[0] = -1;
            Wn[1] = -1;
            r = -1;
            //lanalog = false;
            band_type = DESIGN_IIRBAND_INVALID;
        }
    };
    

private:
    virtual STError_enum ProcessDetail() = 0;

protected:

    struct SignalZPK_PrivateStruct
    {
        complex *p;     /*!< Poles.  This is an array of dimension [npoles]. */
        complex *z;     /*!< Zeros.  This is an array of dimension [nzeros]. */
        double k;    /*!< Gain. */
        int npoles;  /*!< Number of poles. */
        int nzeros;  /*!< Number of zeros. */
        SignalZPK_PrivateStruct()
        {
            p = nullptr;
            z = nullptr;
            k = 0;
            npoles = 0;
            nzeros = 0;
        }
    };


    SignalBAf_Struct ba_iir;
    SignalZPK_PrivateStruct zpk_iir;

    FilterParamStruct<double> m_params;
    DestinationStruct m_result;
    

    IIRDesignBandType m_band_type = DESIGN_IIRBAND_INVALID;
    IIRDesignFilterType m_filter_type = DESIGN_IIRTYPE_INVALID;

    IIRDesignProduceType m_produce_type = DIGITAL;

private:
    void ErrorCallingFilterTransform(SignalZPK_PrivateStruct& zpk_lp);
    void SetTransformSpace(SignalZPK_PrivateStruct& zpk, STError_enum* ierr);

    STError_enum Process_ZPKLp2Lp(const SignalZPK_PrivateStruct& zpk, double w0, SignalZPK_PrivateStruct& zpk_lp);
    STError_enum Process_ZPKLp2Hp(const SignalZPK_PrivateStruct& zpk, double w0, SignalZPK_PrivateStruct& zpk_hp);
    STError_enum Process_ZPKLp2Bp(const SignalZPK_PrivateStruct& zpk, double w0, double bw, SignalZPK_PrivateStruct& zpk_bp);
    STError_enum Process_ZPKLp2Bs(const SignalZPK_PrivateStruct& zpk, double w0, double bw, SignalZPK_PrivateStruct& zpk_bs);
    STError_enum Process_ZPKBilinearTranform(const SignalZPK_PrivateStruct& zpkt, SignalZPK_PrivateStruct& zpkz, const double fs);
    

    SignalZPK_PrivateStruct ZPKLp2Lp(SignalZPK_PrivateStruct& zpk, double w0, STError_enum* ierr);
    SignalZPK_PrivateStruct ZPKLp2Hp(SignalZPK_PrivateStruct& zpk, double w0, STError_enum* ierr);
    SignalZPK_PrivateStruct ZPKLp2Bp(SignalZPK_PrivateStruct& zpk, double w0, double bw, STError_enum* ierr);
    SignalZPK_PrivateStruct ZPKLp2Bs(SignalZPK_PrivateStruct& zpk, double w0, double bw, STError_enum* ierr);
    
    SignalZPK_PrivateStruct ZPKBilinearTransform(SignalZPK_PrivateStruct& zpkt, const double fs, STError_enum* ierr);
    void CheckBAzVarSize(const SignalZPK_PrivateStruct& zpk_iir, const SignalBAz_Struct& ba, STError_enum* ierr);
    STError_enum ProcessPolynomialTransferFunctionFromZPK(const SignalZPK_PrivateStruct& zpk_iir, SignalBAz_Struct& ba);
    void CheckZPK2TF64FVars(const SignalZPK_PrivateStruct& zpk_iir, STError_enum* ierr);
    STError_enum Process_ZPK2TF64F(const SignalZPK_PrivateStruct& zpk_iir, SignalBAf_Struct& ba);
    void CheckZPK2TFVars(const SignalZPK_PrivateStruct& zpk_iir, STError_enum* ierr);
    SignalBAf_Struct ZPK2TF(const SignalZPK_PrivateStruct& zpk_iir, STError_enum* ierr);


protected:
    void PrewarpFrequencies(PrewarpStruct& prewarp, IIRDesignProduceType produce_type, FilterParamStruct<double>& wn_param);
    void FillFilterParameters(const int i, const double w0, const double w1, const double r, const IIRDesignBandType btype/*, const IIRDesignFilterType ftype, const IIRDesignProduceType ptype*/);
    STError_enum ValidateFilterType(const int n, const IIRDesignBandType btype, /*const IIRDesignFilterType ftype, const IIRDesignProduceType ptype, */const double W0, const double W1);


    
    void FreeZPKStruct(SignalZPK_PrivateStruct& zpkt);

    void SetResultToDestination(const SignalBAf_Struct& ba_iir, DestinationStruct& result);
    STError_enum TransformPrototypeToAppropriateBand(SignalZPK_PrivateStruct& zpk, const PrewarpStruct& prewarp,
                                                     IIRDesignBandType btype);

    virtual STError_enum ValidateDesignParameters(SignalZPK_PrivateStruct& zpk_param, FilterParamStruct<double>& filter_param) = 0;
    virtual STError_enum FilterDesign(FilterParamStruct<double>& params, SignalZPK_PrivateStruct& zpk) = 0;

    

public:
    FilterDesigner()
    {

        m_poly.finder = std::make_unique<Poly>();
        m_poly.dst = m_poly.finder->GetDestinationStruct<complex>();
        m_poly.param = m_poly.finder->GetParameterStruct<complex>();
    }


    virtual ~FilterDesigner()
    {
    }


    template<typename T>
    SignalBA_Struct<T> GetDestinationStruct()
    {
        return SignalBA_Struct<T>();
    }

    virtual STError_enum SetParameters(void* param, INPUT_DATA_TYPE input_type) = 0;

   
};
